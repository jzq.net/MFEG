package cn.mfeg.utils;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;

@Component
public class SendMessageJob {
	//设置任务执行时间轮转，cronExpression表达式 (秒 分 时 日 月 周 年（可选）(间隔5秒执行 : 0/5 * * * * ?) (①每隔1小时人的表达式： 0 0 0/1 ? * *) (②每隔15分钟：0 * ? * *)(③每隔45分钟 0 0/45 * ？ * *))
	 /*  @Scheduled(cron="0/5 * * * * ?")
	    public void taskCycle(){  
		   if (true) {
			   String result = "";
			   String url = "http://gw.api.taobao.com/router/rest";//官网的URL, http请求就用这个链接
			   String appkey = "";
			   String secret = "";
			   TaobaoClient client = new DefaultTaobaoClient(url, appkey, secret);
			   AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
			   req.setExtend("123456");//公共回传参数，在“消息返回”中会透传回该参数；举例：用户可以传入自己下级的会员ID，在消息返回时，该会员ID会包含在内，用户可以根据该会员ID识别是哪位会员使用了你的应用
			   req.setSmsType("normal");//必填  短信类型，传入值请填写normal
			   req.setSmsFreeSignName("阿里大于");//必填  短信签名，传入的短信签名必须是在阿里大于“管理中心-短信签名管理”中的可用签名。
			   req.setSmsParamString("{\"code\":\"1234\",\"product\":\"alidayu\"}");//  短信模板变量，传参规则{"key":"value"}，key的名字须和申请模板中的变量名一致，多个变量之间以逗号隔开。示例：针对模板“验证码${code}，您正在进行${product}身份验证，打死不要告诉别人哦！”，传参时需传入{"code":"1234","product":"alidayu"}
			   req.setRecNum("15901099125");//必填  短信接收号码。支持单个或多个手机号码，传入号码为11位手机号码，不能加0或+86。群发短信需传入多个号码，以英文逗号分隔，一次调用最多传入200个号码。示例：18600000000,13911111111,13322222222
			   req.setSmsTemplateCode("SMS_585014");//必填  短信模板ID，传入的模板必须是在阿里大于“管理中心-短信模板管理”中的可用模板
			   AlibabaAliqinFcSmsNumSendResponse rsp;
			   try {
				   rsp = client.execute(req);
				   System.out.println(rsp.getBody());
				   result = rsp.getSubMsg();
			   } catch (ApiException e) {
				   e.printStackTrace();
			   }
			   System.out.println(result);
		   }
	    } */
	   
	   
}

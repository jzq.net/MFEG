package cn.mfeg.controllers;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.mfeg.models.ExpertUser;
import cn.mfeg.models.Project;
import cn.mfeg.services.CollegesUserService;
import cn.mfeg.services.ExpertUserService;
import cn.mfeg.services.ProjectExpertService;
import cn.mfeg.services.ProjectService;

@Controller
@RequestMapping(value = "/collegesUser")
public class CollegesUserController {
	
	@Autowired
	CollegesUserService collegesUserService;
	@Autowired
	ExpertUserService expertUserService;
	@Autowired
	ProjectService projectService;
	@Autowired
	ProjectExpertService projectExpertService;
	Logger log=Logger.getLogger(CollegesUserController.class);
	/**查询该高校项目信息
	 * @param code
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/projectManage.c")
	public void selectProjectInfoByCode(HttpServletRequest req) {
		HttpSession session = req.getSession();
		String code = (String) session.getAttribute("userName");
		String userRole = (String) session.getAttribute("userRole");
		List<Project> projects = collegesUserService.selectProjectInfoByCode(code,userRole);
		int projectTotal = projects.size();
		int underwayProjectTotal = 0;//进行中
		int finishProjectTotal = 0;//已结束
		for (Project project:projects) {
			if ("进行中".equals(project.getStatus())) {
				underwayProjectTotal=underwayProjectTotal+1;
			}else if ("已结束".equals(project.getStatus())) {
				finishProjectTotal = finishProjectTotal+1;
			}
		}
		session.setAttribute("projects", projects);
		session.setAttribute("projectTotal", projectTotal);
		session.setAttribute("underwayProjectTotal", underwayProjectTotal);
		session.setAttribute("finishProjectTotal", finishProjectTotal);
	}
	
	/**根据项目ID查询参与专家信息
	 * @param req
	 * @param projectId
	 */
	@RequestMapping(value = "/checkProjectInfo.c")
	public void checkProjectInfo(HttpServletRequest req,String projectId) {
		List<ExpertUser> expertUsers = expertUserService.selectInfoByProjectId(projectId);
		for (ExpertUser expertUser:expertUsers) {
			List<Project> projects = projectService.selectAppraisePnameByIdCard(expertUser.getIdCard());
			String achievement = "";
			for(Project project:projects){//拼装该专家业绩
				achievement += (project.getProjectname()+":"+project.getAppraise()+"<br>");
			}
			expertUser.setAchievement(achievement);
		}
		req.getSession().setAttribute("expertUsers", expertUsers);
	}
	
	/**查询专家详细信息
	 * @param req
	 * @param projectId
	 */
	@ResponseBody
	@RequestMapping(value = "/checkExpertDetialInfo.c")
	public void checkExpertDetialInfo(HttpServletRequest req,String idCard) {
		ExpertUser expertUser = expertUserService.selectInfoByIdCard(idCard);
		List<Project> projects = projectService.selectAppraisePnameByIdCard(idCard);
		req.getSession().setAttribute("expertUser", expertUser);
		req.getSession().setAttribute("projects", projects);
	}
	
	/**根据项目ID查询参与专家信息
	 * @param req
	 * @param projectId
	 */
	@ResponseBody
	@RequestMapping(value = "/checkNotPartakeProjectExpert.c")
	public void checkNotPartakeProjectExpert(HttpServletRequest req,String projectId) {
		List<ExpertUser> expertUsers = expertUserService.selectNotPartakeProjectExpertByProjectId(projectId);
		for (ExpertUser expertUser:expertUsers) {
			List<Project> projects = projectService.selectAppraisePnameByIdCard(expertUser.getIdCard());
			String achievement = "";
			for(Project project:projects){//拼装该专家业绩
				achievement += (project.getProjectname()+":"+project.getAppraise()+"<br>");
			}
			expertUser.setAchievement(achievement);
		}
		req.getSession().setAttribute("expertUsers", expertUsers);
	}
	
	/**根据项目ID查询已添加专家信息
	 * @param req
	 * @param projectId
	 */
	@ResponseBody
	@RequestMapping(value = "/checkAddedExpert.c")
	public void checkAddedExpertByProjectId(HttpServletRequest req,String projectId) {
		List<ExpertUser> expertUsers = expertUserService.selectAddedExpertByProjectId(projectId);
		req.getSession().setAttribute("expertUsers", expertUsers);
	}
	
	/**添加专家
	 * @param req
	 * @param projectId
	 */
	@ResponseBody
	@RequestMapping(value = "/addProjectExpert.c")
	public String addProjectExpert(HttpServletRequest req,String projectId,String expertId) {
		String expertIds[] = expertId.split("-");
		if (projectExpertService.insertProjectExpertByPidEid(projectId,expertIds)) {
			return "1";
		}else {
			return "0";
		}
	}
	
	/**删除专家
	 * @param req
	 * @param projectId
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteProjectExpert.c")
	public String deleteProjectExpert(HttpServletRequest req,String peId) {
		String peIds[] = peId.split("-");
		if (projectExpertService.deleteProjectExpertById(peIds)) {
			return "1";
		}else {
			return "0";
		}
	}
	
	
	
	
	
	/**上传专家评论
	 * @param req
	 * @param projectId
	 */
	/*@ResponseBody
	@RequestMapping(value = "/uploadExcel.c")
	public String upload(HttpServletRequest req, HttpServletResponse resp) {
		//req.setCharacterEncoding("UTF-8");
		try {
			String path = req.getServletContext().getRealPath("/downloadFiles");
			ServletFileUpload ss = new ServletFileUpload(new DiskFileItemFactory());// 使用系统默认的临时目录
			List<FileItem> list = ss.parseRequest(req);
			// 由于一个文件，和一个表单项目是一组
			for (FileItem item : list) {
				if (item.isFormField()) {// 普通的表表单项目 request.getparams()
					String value = item.getString(req.getCharacterEncoding());
					log.info(value);
				} else {
					String fileName = item.getName();
					fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
					File file = new File(path, fileName);
					item.write(file);//存放到磁盘中
					FileInputStream fis = new FileInputStream(file);
					Workbook workbook = new HSSFWorkbook(fis);
					*//** 获取sheet *//*
					Sheet sheet = workbook.getSheetAt(0);
					*//** 获取sheet的列数 *//*
					//int columnNum = sheet.getRow(0).getPhysicalNumberOfCells();
					*//** 获取文件有多少行 *//*
					int rowNum = sheet.getLastRowNum()+1;
					//获取第二行所有数据
					for(int i=1;i<rowNum;i++){
						String projectId = getCellStringValue(sheet.getRow(i),0);//项目ID
						String expertId = getCellStringValue(sheet.getRow(i),2);//专家ID
						String appraise = getCellStringValue(sheet.getRow(i),11);//评价
						String comment = getCellStringValue(sheet.getRow(i),12);//评语
						String expertName = getCellStringValue(sheet.getRow(i),4);//专家姓名
						//检验该专家是否存在
						ProjectExpert projectExpert = new ProjectExpert();
						projectExpert.setProjectid(projectId);
						projectExpert.setExpertid(expertId);
						if (!projectExpertService.checkExpertByProjectIdExpertId(projectExpert)) {//不存在
							workbook.close();
							return "专家"+expertName+"不存在！";
						}
						//将数据加入数据库
						projectExpert.setAppraise(appraise);
						projectExpert.setComment(comment);
					    if (!projectExpertService.insertAppraiseCommentByPidEid(projectExpert)) {//插入失败
						    workbook.close();
						    return "在导入专家"+expertName+"的信息时出现错误！";
					    }
					}
					item.delete();
					workbook.close();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "导入成功！";
	}*/
	
	/**获取单元格内容
	 * @param row
	 * @param i
	 * @return
	 */
	/*public final static String getCellStringValue(Row row, int i) {
		Cell cell = row.getCell(i);
		if (cell == null) {
			return "";
		}
		cell.setCellType(Cell.CELL_TYPE_STRING);
		if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
			return String.valueOf(cell.getBooleanCellValue());
		} else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			return String.valueOf(cell.getNumericCellValue());
		} else {
			return String.valueOf(cell.getStringCellValue());
		}
	}	*/
}

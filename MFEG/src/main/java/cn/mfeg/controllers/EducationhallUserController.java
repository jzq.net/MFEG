package cn.mfeg.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.mfeg.models.CollegesUser;
import cn.mfeg.models.DepartmentUser;
import cn.mfeg.models.ExpertUser;
import cn.mfeg.models.Project;
import cn.mfeg.models.User;
import cn.mfeg.services.CollegesUserService;
import cn.mfeg.services.DepartmentUserService;
import cn.mfeg.services.ExpertUserService;
import cn.mfeg.services.ProjectService;

@Controller
@RequestMapping(value = "/educationhallUser")
public class EducationhallUserController {
	@Autowired
	DepartmentUserService departmentUserService;
	@Autowired
	CollegesUserService collegesUserService;
	@Autowired
	ExpertUserService expertUserService;
	@Autowired
	ProjectService projectService;
	
	@ResponseBody
	@RequestMapping(value = "/userManage.c")
	public void  userManage(HttpServletRequest req,String userRole) {
		HttpSession session = req.getSession();
		if ("2".equals(userRole)) {//高校
			List<CollegesUser> collegesUsers = collegesUserService.selectAllCollegesUserInfo();
			session.setAttribute("collegesUsers", collegesUsers);
		}else if ("4".equals(userRole)) {//处室
			List<DepartmentUser> departmentUsers = departmentUserService.selectAllDepartmentUserInfo();
			session.setAttribute("departmentUsers", departmentUsers);
		}else if ("3".equals(userRole)) {//专家
			List<ExpertUser> expertUsers = expertUserService.selectAllExpertInfo();
			session.setAttribute("expertUsers", expertUsers);
		}
	}
	
	/**查询所有项目信息
	 * @param req
	 */
	@ResponseBody
	@RequestMapping(value = "/projectManage.c")
	public void  projectManage(HttpServletRequest req) {
		HttpSession session = req.getSession();
		List<Project> projects = projectService.selectAllProjectInfo();
		session.setAttribute("projects", projects);
	}
	
	/**根据项目ID查询参与专家信息
	 * @param req
	 * @param projectId
	 */
	@ResponseBody
	@RequestMapping(value = "/checkProjectInfo.c")
	public void checkProjectInfo(HttpServletRequest req,String projectId) {
		List<ExpertUser> expertUsers = expertUserService.selectInfoByProjectId(projectId);
		for (ExpertUser expertUser:expertUsers) {
			List<Project> projects = projectService.selectAppraisePnameByIdCard(expertUser.getIdCard());
			String achievement = "";
			for(Project project:projects){//拼装该专家业绩
				achievement += (project.getProjectname()+":"+project.getAppraise()+"<br>");
			}
			expertUser.setAchievement(achievement);
		}
		req.getSession().setAttribute("expertUsers", expertUsers);
	}
	
	/**根据项目ID查询参与专家信息
	 * @param req
	 * @param projectId
	 */
	@ResponseBody
	@RequestMapping(value = "/stopProject.c")
	public String stopProject(HttpServletRequest req,String projectId) {
		String projectIds[] = projectId.split("-");
		if (projectService.stopProjectById(projectIds)) {
			return "1";
		}else {
			return "0";
		}
	}
	
	/**添加用户
	 * @param req
	 * @param userRole
	 */
	@ResponseBody
	@RequestMapping(value = "/addUser.c")
	public String  addUser(User user,String userRole) {
		 
		if ("2".equals(userRole)) {//高校
			if (collegesUserService.validateUserName(user)) {
				if (collegesUserService.addUser(user)) {
					return "1";//成功
				}else {
					return "0";//失败
				}
			}else {
				return "3";//用户名重复
			}
		}else if ("4".equals(userRole)) {//处室
			if (departmentUserService.validateUserName(user)) {//检验用户名是否存在
				if (departmentUserService.addUser(user)) {
					return "1";//成功
				}else {
					return "0";//失败
				}
			}else {
				return "3";//用户名重复
			}
		}else if ("3".equals(userRole)) {//专家
			 
		}
		return "";
	}
	
	/**修改用户
	 * @param req
	 * @param userRole
	 */
	@ResponseBody
	@RequestMapping(value = "/alterUser.c")
	public String  alterUser(User user,String userRole) {
		
		if ("2".equals(userRole)) {//高校
			if (collegesUserService.alterUserById(user)) {
				return "1";//成功
			}else {
				return "0";//失败
			}
		}else if ("4".equals(userRole)) {//处室
			 if (departmentUserService.alterUserById(user)) {
				return "1";//成功
			}else {
				return "0";//失败
			}
		}else if ("3".equals(userRole)) {//专家
			 if (expertUserService.alterUserById(user)) {
					return "1";//成功
				}else {
					return "0";//失败
				}
		}
		return "0";
	}
	
	/**禁用用户
	 * @param req
	 * @param userRole
	 */
	@ResponseBody
	@RequestMapping(value = "/stopUser.c")
	public String  stopUser(String id,String userRole) {
		String ids[] = id.split("-");
		if ("2".equals(userRole)) {//高校
			if (collegesUserService.stopUser(ids)) {
				 return "1";
			}else {
				return "0";
			}
		}else if ("4".equals(userRole)) {//处室
			 if (departmentUserService.stopUser(ids)) {
				 return "1";
			}else {
				return "0";
			}
		}else if ("3".equals(userRole)) {//专家
			 if (expertUserService.stopUser(ids)) {
				 return "1";
			}else {
				return "0";
			}
		}
		return "0";
	}
	
	/**查询处室、高校项目数据
	 * @param req
	 */
	@ResponseBody
	@RequestMapping(value = "/selectAllDCProjectInfo.c")
	public void selectAllProjectInfoByOrganiserOrganiserRole(HttpServletRequest req) {
		 List<CollegesUser> collegesUsers = collegesUserService.selectAllCollegesUserInfo();
		 List<DepartmentUser> departmentUsers = departmentUserService.selectAllDepartmentUserInfo();
		 Map<String, Object> map = new HashMap<>();
		 for (CollegesUser collegesUser:collegesUsers) {
			 map.put("organiser", collegesUser.getCode());
			 map.put("organiserRole", "2");
			List<Project> projects = projectService.selectAllProjectInfoByOrganiserOrganiserRole(map);
			collegesUser.setProjects(projects);
		 }
		 for (DepartmentUser departmentUser:departmentUsers) {
			 map.put("organiser", departmentUser.getUserName());
			 map.put("organiserRole", "4");
			 List<Project> projects = projectService.selectAllProjectInfoByOrganiserOrganiserRole(map);
			 departmentUser.setProjects(projects);
		 }
		 map.put("collegesUsers", collegesUsers);
		 map.put("departmentUsers", departmentUsers);
		 req.getSession().setAttribute("infoMap", map);
	}
	
	/**查询专家项目数据
	 * @param req
	 */
	@ResponseBody
	@RequestMapping(value = "/selectAllExpertProjectInfo.c")
	public void selectAllProjectInfoByIdCard(HttpServletRequest req) {
		 List<ExpertUser> expertUsers = expertUserService.selectAllExpertInfo();
		 for(ExpertUser expertUser:expertUsers){
			 List<Project> projects = projectService.selectInfoByIdCard(expertUser.getIdCard());
			 expertUser.setProjects(projects);
		 }
		 req.getSession().setAttribute("expertUsers", expertUsers);
	}
	
	/**查询项目数据
	 * @param req
	 */
	@ResponseBody
	@RequestMapping(value = "/selectAllProjectInfo.c")
	public void selectAllProjectInfo(HttpServletRequest req) {
		List<Project> projects = projectService.selectAllProjectInfo();
		req.getSession().setAttribute("projects", projects);
	}
	
	/**查询参与项目的专家信息
	 * @param req
	 * @param projectId
	 */
	@ResponseBody
	@RequestMapping(value = "/selectAllExpertInfo.c")
	public void selectAllExpertInfoByProjectId(HttpServletRequest req,String projectId) {
		List<ExpertUser> expertUsers = expertUserService.selectInfoByProjectId(projectId);
		req.getSession().setAttribute("expertUsers", expertUsers);
	}
	
	/**查询参与项目的优秀专家信息
	 * @param req
	 * @param projectId
	 */
	@ResponseBody
	@RequestMapping(value = "/selectExcellentExpertInfo.c")
	public void selectExcellentExpertInfoByProjectId(HttpServletRequest req,String projectId) {
		List<ExpertUser> expertUsers = expertUserService.selectExcellentExpertInfoByProjectId(projectId);
		req.getSession().setAttribute("expertUsers", expertUsers);
	}
	
	/**查询所有专家参与的项目信息
	 * @param req
	 */
	@ResponseBody
	@RequestMapping(value = "/selectAllExpertData.c")
	public void selectAllExpertInfo(HttpServletRequest req) {
		List<ExpertUser> expertUsers = expertUserService.selectAllExpertInfo();
		for(ExpertUser expertUser:expertUsers){
			List<Project> allProjects = projectService.selectInfoByIdCard(expertUser.getIdCard());
			expertUser.setAllProjects(allProjects);
			List<Project> excellentProjects = projectService.selectExcellentProjectByIdCard(expertUser.getIdCard());
			expertUser.setExcellentProjects(excellentProjects);
		}
		req.getSession().setAttribute("expertUsers", expertUsers);
	}
	/**查询某专家参与的所有项目信息
	 * @param req
	 */
	@ResponseBody
	@RequestMapping(value = "/selectExpertAllProjectInfo.c")
	public void selectProjectInfoByIdCard(HttpServletRequest req,String idCard) {
		List<Project> allProjects = projectService.selectInfoByIdCard(idCard);
		req.getSession().setAttribute("allProjects", allProjects);
	}
	/**查询某专家参与的优秀项目信息
	 * @param req
	 */
	@ResponseBody
	@RequestMapping(value = "/selectExpertExcellentProjectInfo.c")
	public void selectExcellentProjectInfoByIdCard(HttpServletRequest req,String idCard) {
		List<Project> excellentProjects = projectService.selectExcellentProjectByIdCard(idCard);
		req.getSession().setAttribute("excellentProjects", excellentProjects);
	}
	
}

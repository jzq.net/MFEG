package cn.mfeg.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.mfeg.models.ExpertUser;
import cn.mfeg.services.UserService;
import cn.mfeg.utils.ObjectValidateUtil;

/**
 * 首页访问控制类
 * @version 
 * @author  jzq
 */
@Controller
public class IndexController {
	@Autowired
	UserService userService;
	
	/**
	 * 页面跳转通用方法
	 * @param page
	 * @author 
	 * @return
	 */
	@RequestMapping(value = "/{page}.c")
	public String showPage(@PathVariable String page) {
		return page;
	}

	/**
	 * 退出 
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/loginOut.c")
	public String logout(HttpServletRequest req) {
		req.getSession().invalidate();
		return "redirect:/";
	}

	/**
	 * 登录信息验证
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/index.c")
	public String index(HttpServletRequest req) {
		HttpSession session = req.getSession();
		String userRole=req.getParameter("userRole");
		req.setAttribute("userRole", userRole);
		String loginType=req.getParameter("loginType");
		if ("2".equals(loginType)) {//首次登录
			String userIdCard=req.getParameter("userIdCard");
			String userPhoneNum=req.getParameter("userPhoneNum");
			ExpertUser expertUser = userService.selectFirstLoginMobileByIdCard(userIdCard);
			String firstLoginFlag = expertUser.getFirstLogin();
			if (!"2".equals(firstLoginFlag)) {
				String mobile = expertUser.getMobile();
				if (!ObjectValidateUtil.objIsNull(mobile)&&mobile.equals(userPhoneNum)) {
					userService.updateFirstLogin(userIdCard);
					session.setAttribute("userName",userIdCard );
					session.setAttribute("userRole",userRole );
					return "common/main";
				}else if (mobile==null||mobile.equals("")) {
					req.setAttribute("firstLoginError", "登录失败，用户名不存在！");
					return "index";
				}else{
					req.setAttribute("firstLoginError", "登录失败，预留手机号与用户名不匹配！");
					return "index";
				}
			}else {
				req.setAttribute("firstLoginError", "登录失败，系统检查您不是第一次登录！");
				return "index";
			}
		}else {//正常登录
			String userName=req.getParameter("userName");
			String userPwd=req.getParameter("userPwd");
			Boolean isValid = userService.validateUserInfo(userRole, userName, userPwd, req);
			if (isValid) {
				if (userService.validateUserStatus(userRole, userName, req)) {//检验用户名是否已被禁用
					session.setAttribute("userName",userName );
					session.setAttribute("userRole",userRole );
					return "common/main";
				}else {
					req.setAttribute("loginError", "登录失败，该用户名已被禁用！");
					return "index";
				}
			}else {
				if (req.getAttribute("loginError")==null||req.getAttribute("loginError").equals("")) {
					req.setAttribute("loginError", "登录失败，请检查用户名、密码和类型是否正确！");
				}
				return "index";
			}
		}
		
	}
	
}

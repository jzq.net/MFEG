package cn.mfeg.controllers;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.mfeg.models.Project;
import cn.mfeg.services.ProjectService;

@Controller
@RequestMapping(value = "/project")
public class ProjectController {
	
	@Autowired
	ProjectService projectService;
	
	@RequestMapping(value = "/projectPartakeInfo.c")
	public void projectPartakeInfo(HttpServletRequest req) {
		HttpSession session = req.getSession();
		String idCard = (String) session.getAttribute("userName");
		List<Project> projects = projectService.selectInfoByIdCard(idCard);
		session.setAttribute("projects", projects);
	}
	@ResponseBody
	@RequestMapping(value = "/createProject.c")
	public String createProject(HttpServletRequest req,Project project) {
		HttpSession session = req.getSession();
		String organiser = (String) session.getAttribute("userName");
		project.setOrganiser(organiser);
		String userRole = (String) session.getAttribute("userRole");
		if ("2".equals(userRole)) {//高校
			project.setOrganiserrole("2");
		}else if ("4".equals(userRole)) {//处室
			project.setOrganiserrole("4");
		} 
		if (projectService.creatProject(project)) {
			return "1";
		}else {
			return "0";
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateProjectById.c")
	public String updateProjectById(HttpServletRequest req,Project project) {
		if (projectService.updateProjectById(project)) {
			return "1";
		}else {
			return "0";
		}
	}
	@ResponseBody
	@RequestMapping(value = "/deleteProjectById.c")
	public String deleteProjectById(String id) {
		String ids[] = id.split("-");
		if (projectService.deleteProjectById(ids)) {
			return "1";
		}else {
			return "0";
		}
	}
}

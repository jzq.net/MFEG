package cn.mfeg.controllers;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.mfeg.models.CollegesUser;
import cn.mfeg.models.DepartmentUser;
import cn.mfeg.models.ExpertUser;
import cn.mfeg.models.Project;
import cn.mfeg.services.CollegesUserService;
import cn.mfeg.services.DepartmentUserService;
import cn.mfeg.services.ExpertUserService;
import cn.mfeg.services.ProjectService;

/**更换密码
 * @author jzq
 *
 */
@Controller
@RequestMapping(value = "/accountManage")
public class AccountManageController {
	@Autowired
	ExpertUserService expertUserService;
	@Autowired
	ProjectService projectService;
	@Autowired
	CollegesUserService collegesUserService;
	@Autowired
	DepartmentUserService departmentUserService;
	
	@ResponseBody
	@RequestMapping(value = "/pwdManage/alterPwd.c")
	public String alterPwd(HttpServletRequest req) {
		HttpSession session = req.getSession();
		String userName = (String) session.getAttribute("userName");
		String userRole = (String) session.getAttribute("userRole");
		String oldPwd = req.getParameter("oldPwd");
		String newPwd = req.getParameter("newPwd");
		if ("2".equals(userRole)) {//高校
			if (collegesUserService.validatePwd(userName, oldPwd)) {//原密码正确
				if (collegesUserService.updatePwd(userName, newPwd)) {//改密成功
					return "1";
				}else {
					return "0";
				}
			}else {
				return "2";
			}
		}else if ("3".equals(userRole)) {//专家
			if (expertUserService.validatePwd(userName, oldPwd)) {//原密码正确
				if (expertUserService.updatePwd(userName, newPwd)) {//改密成功
					return "1";
				}else {
					return "0";
				}
			}else {
				return "2";
			}
		}else {//处室
			if (departmentUserService.validatePwd(userName, oldPwd)) {//原密码正确
				if (departmentUserService.updatePwd(userName, newPwd)) {//改密成功
					return "1";
				}else {
					return "0";
				}
			}else {
				return "2";
			}
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/pwdManage/forgetPwd.c")
	public String forgetPwd(HttpServletRequest req) {
		HttpSession session = req.getSession();
		String userRole = (String) session.getAttribute("userRole");
		if ("2".equals(userRole)) {//高校
			
		}else if ("3".equals(userRole)) {//专家
			
		}else {//处室
			
		}
		return "";
	}
	
	@ResponseBody
	@RequestMapping(value = "/pwdManage/alterPhoneNum.c")
	public String alterPhoneNum(HttpServletRequest req) {
		HttpSession session = req.getSession();
		String userRole = (String) session.getAttribute("userRole");
		if ("2".equals(userRole)) {//高校
			
		}else if ("3".equals(userRole)) {//专家
			
		}else {//处室
			
		}
		return "";
	}
	
	@RequestMapping(value = "/personalInfo.c")
	public void personalInfo(HttpServletRequest req) {
		HttpSession session = req.getSession();
		String userName = (String) session.getAttribute("userName");
		String userRole = (String) session.getAttribute("userRole");
		if ("2".equals(userRole)) {//高校
			 CollegesUser collegesUser = collegesUserService.selectInfoByIdCard(userName);
			 session.setAttribute("collegesUser", collegesUser);
		}else if ("3".equals(userRole)) {//专家
			ExpertUser expertUser= expertUserService.selectInfoByIdCard(userName);
			List<Project> projects = projectService.selectAppraisePnameByIdCard(userName);
			session.setAttribute("expertUser", expertUser);
			session.setAttribute("projects", projects);
		}else {//处室
			 DepartmentUser departmentUser = departmentUserService.selectInfoByUserName(userName);
			 session.setAttribute("departmentUser", departmentUser);
		}
		
	}
	
	@ResponseBody
	@RequestMapping(value = "/alterDescription.c")
	public String alterDescription(HttpServletRequest req) {
		HttpSession session = req.getSession();
		String idCard = (String) session.getAttribute("userName");
		String description = req.getParameter("description");
		if (expertUserService.alterDescription(idCard, description)) {
			return "1";
		}else {
			return "0";
		}
	}
}

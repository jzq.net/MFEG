package cn.mfeg.filters;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import cn.mfeg.services.RoleMenuPermissionService;
import cn.mfeg.utils.ObjectValidateUtil;

/**权限过滤器
 * @author jzq
 *
 */
public class AuthFilter implements Filter{
	private List<String> ehList;
	private List<String> uList;
	private List<String> eList;
	private List<String> oList;
	private static Map<String , List<String>> map = new ConcurrentHashMap<String, List<String>>();
	private Logger log = Logger.getLogger(AuthFilter.class);
	private ApplicationContext applicationContext;
	@Override
	public void init(FilterConfig config) throws ServletException {
		try {
			applicationContext = new ClassPathXmlApplicationContext("spring-servlet.xml");
			RoleMenuPermissionService roleMenuPermissionService = (RoleMenuPermissionService) applicationContext.getBean("roleMenuPermissionService");
			ehList = roleMenuPermissionService.selectUrlByroleType("1");
			uList = roleMenuPermissionService.selectUrlByroleType("2");
			eList = roleMenuPermissionService.selectUrlByroleType("3");
			oList = roleMenuPermissionService.selectUrlByroleType("4");
			//将用户权限信息放入容器,方便提取对比,减少性能消耗
			map.put("1", ehList);
			map.put("2", uList);
			map.put("3", eList);
			map.put("4", oList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		String userRole = (String) session.getAttribute("userRole");
		String uri = req.getRequestURI().replace(req.getContextPath(), "");//截取即将访问的url
		
		if (uri.contains("?")) {//get方式
			uri = uri.substring(0,uri .indexOf("?"));
		}
		if (uri.contains(";jsessionid=")) {
			uri = uri.substring(0,uri .indexOf(";jsessionid="));
		}
		log.info(uri);
		if ("/index.c".equals(uri)||"/loginOut.c".equals(uri)) {//登录、退出不需要拦截
			chain.doFilter(req, response);
		}else {
			if (ObjectValidateUtil.objIsNull(userRole)) {
				log.info("您还没有登录！请先去登录...");
				resp.sendRedirect(req.getContextPath()+"/error/noLogin.jsp");
				return;
			}
			List<String> urls = map.get(userRole);
			if (!ObjectValidateUtil.objIsNull(urls)&&urls.contains(uri)) {
				chain.doFilter(req, response);
			}else {
				log.info("您还没有访问权限..."+uri);
				resp.sendRedirect(req.getContextPath()+"/error/noPermission.jsp");
				return;
			}
		}
		
	}

	@Override
	public void destroy() {
	}
}
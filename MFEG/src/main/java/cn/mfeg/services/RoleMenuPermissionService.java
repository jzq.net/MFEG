package cn.mfeg.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.mfeg.daos.RoleMenuPermissionDao;

@Service(value="roleMenuPermissionService")
public class RoleMenuPermissionService {
	@Autowired
	RoleMenuPermissionDao roleMenuPermissionDao;
	public List<String> selectUrlByroleType(String userRole) {
		List<String> urls = roleMenuPermissionDao.selectUrlByroleType(userRole);
		return urls;
	}
}

package cn.mfeg.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.mfeg.daos.ProjectExpertDao;
import cn.mfeg.models.ProjectExpert;
import cn.mfeg.utils.ObjectValidateUtil;

@Service
public class ProjectExpertService {
	@Autowired
	ProjectExpertDao projectExpertDao;
	
	/**添加专家
	 * @param projectExpert
	 * @return
	 */
	public boolean insertProjectExpertByPidEid(String projectId,String[] expertIds) {
		ProjectExpert projectExpert = new ProjectExpert();
		projectExpert.setProjectid(projectId);
		int result = 0;
		for(String expertId:expertIds){
			projectExpert.setExpertid(expertId);
			result = projectExpertDao.insertProjectExpertByPidEid(projectExpert);
		}
		if (result==1) {
			return true;
		}else {
			return false;
		}
	}
	
	/**删除专家
	 * @param projectExpert
	 * @return
	 */
	public boolean deleteProjectExpertById(String[] peIds) {
		int result = 0;
		for(String peId:peIds){
			result = projectExpertDao.deleteProjectExpertById(peId);
		}
		if (result==1) {
			return true;
		}else {
			return false;
		}
	}
	
	/**检验专家是否存在
	 * @param projectExpert
	 * @return
	 */
	public boolean checkExpertByProjectIdExpertId(ProjectExpert projectExpert) {
		String result = projectExpertDao.checkExpertByProjectIdExpertId(projectExpert);
		if (ObjectValidateUtil.objIsNull(result)) {
			return false;
		}else {
			return true;
		}
	}
	/**插入评价、评语
	 * @param projectExpert
	 * @return
	 */
	public boolean insertAppraiseCommentByPidEid(ProjectExpert projectExpert) {
		int result = projectExpertDao.insertAppraiseCommentByPidEid(projectExpert);
		if (result==1) {
			return true;
		}else {
			return false;
		}
	}
	
}

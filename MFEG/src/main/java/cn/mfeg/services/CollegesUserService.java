package cn.mfeg.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.mfeg.daos.CollegesUserDao;
import cn.mfeg.daos.ProjectDao;
import cn.mfeg.models.CollegesUser;
import cn.mfeg.models.Project;
import cn.mfeg.models.User;
import cn.mfeg.utils.Md5Util;
import cn.mfeg.utils.ObjectValidateUtil;

@Service
public class CollegesUserService {
	
	@Autowired
	CollegesUserDao collegesUserDao;
	@Autowired
	ProjectDao projectDao;
	
	/**检验原密码是否正确
	 * @param idCard
	 * @param paramPwd
	 * @return
	 */
	public boolean validatePwd(String code,String paramPwd){
		CollegesUser collegesUser = collegesUserDao.selectSaltPwdByUserName(code);
		String nativePwd = collegesUser.getPassword();
		String  salt = collegesUser.getSalt();
		paramPwd = Md5Util.md5(paramPwd, salt);
		if (paramPwd.equals(nativePwd)) {
			return true;
		}else {
			return false;
		}
	}
	 
	/**更新密码
	 * @param code
	 * @param newPwd
	 * @return
	 */
	public boolean updatePwd(String code,String newPwd) {
			CollegesUser collegesUser = collegesUserDao.selectSaltPwdByUserName(code);
			newPwd = Md5Util.md5(newPwd, collegesUser.getSalt());//对密码加密
			collegesUser.setCode(code);
			collegesUser.setPassword(newPwd);
			int result = collegesUserDao.updatePwd(collegesUser);
			if (result==1) {
				return true;
			}else {
				return false;
			}
		}
	 
	/**添加用户
	 * @param user
	 * @return
	 */
	public boolean addUser(User user) {
		String salt = Md5Util.getSalt();
		user.setSalt(salt);
		user.setPassword(Md5Util.md5(user.getPassword(), salt));
		int result = collegesUserDao.addUser(user);
		if (result==1) {
			return true;
		}else {
			return false;
		}
	}
	/**禁用用户
	 * @param code
	 * @param newPwd
	 * @return
	 */
	public boolean stopUser(String[] ids) {
		int result = 0;
		for (String id:ids) {
			result = collegesUserDao.stopUser(id);
		}
		if (result==1) {
			return true;
		}else {
			return false;
		}
	}
	
	/**修改用户
	 * @param code
	 * @param newPwd
	 * @return
	 */
	public boolean alterUserById(User user) {
		String salt = selectInfoById(user.getId()).getSalt();
		user.setPassword(Md5Util.md5(user.getPassword(), salt));
		int result = collegesUserDao.alterUserById(user);
		if (result==1) {
			return true;
		}else {
			return false;
		}
	}
	
	/**根据id查询用户信息
	 * @param userName
	 * @return
	 */
	public CollegesUser selectInfoById(int id) {
		return collegesUserDao.selectInfoById(id);
	}
	
	/**验证高校代码是否已存在
	 * @param user
	 * @return
	 */
	public boolean validateUserName(User user){
		CollegesUser collegesUser = collegesUserDao.selectInfoByCode(user.getCode());
		if (ObjectValidateUtil.objIsNull(collegesUser)) {
			return true;//不存在
		}else {
			return false;
		}
	}	
	/**查询高校个人信息
	 * @param code
	 * @return
	 */
	public CollegesUser selectInfoByIdCard(String code){
		return collegesUserDao.selectInfoByCode(code);
	}
	
	/**查询该高校项目信息
	 * @param code
	 * @return
	 */
	public List<Project> selectProjectInfoByCode(String code,String userRole) {
		Project project = new Project();
		project.setOrganiser(code);
		project.setOrganiserrole(userRole);
		List<Project> projects = projectDao.selectProjectInfoByCode(project);
		return projects;
	}
	/**查询所有高校信息
	 * @param code
	 * @return
	 */
	public List<CollegesUser> selectAllCollegesUserInfo() { 
		List<CollegesUser> collegesUsers = collegesUserDao.selectAllCollegesUserInfo();
		return collegesUsers;
	}
	
	public String selectStatusByCode(String code) {
		return collegesUserDao.selectStatusByCode(code);
	}
	
}

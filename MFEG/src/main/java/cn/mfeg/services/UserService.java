package cn.mfeg.services;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.mfeg.daos.CollegesUserDao;
import cn.mfeg.daos.DepartmentUserDao;
import cn.mfeg.daos.EducationhallUserDao;
import cn.mfeg.daos.ExpertUserDao;
import cn.mfeg.models.CollegesUser;
import cn.mfeg.models.DepartmentUser;
import cn.mfeg.models.EducationhallUser;
import cn.mfeg.models.ExpertUser;
import cn.mfeg.utils.Md5Util;
import cn.mfeg.utils.ObjectValidateUtil;

/**
 * @author jzq
 *
 */
@Service
public class UserService {
	@Autowired
	ExpertUserDao expertUserDao;
	@Autowired
	EducationhallUserDao educationhallUserDao;
	@Autowired
	CollegesUserDao collegesUserDao;
	@Autowired
	DepartmentUserDao departmentUserDao;
	
	/**验证专家用户首次登录手机号与身份证是否匹配
	 * @param idCard
	 * @return
	 */
	public ExpertUser selectFirstLoginMobileByIdCard(String idCard) {
		return expertUserDao.selectFirstLoginMobileByIdCard(idCard);
	}
	
	/**将专家登录状态置为非首次登录
	 * @param idCard
	 * @return
	 */
	@Transactional
	public int updateFirstLogin(String idCard) {
		return expertUserDao.updateFirstLogin(idCard);
	}
	
	/**验证用户登录信息是否正确
	 * @param userRole
	 * @param userName
	 * @param userPwd
	 * @param req
	 * @return
	 */
	public Boolean validateUserInfo(String userRole,String userName,String userPwd,HttpServletRequest req) {
		String salt = "";
		String pwd = "";
		if ("1".equals(userRole)) {//教育厅
			EducationhallUser educationhallUser = educationhallUserDao.selectSaltPwdByIdCard(userName);
			if (ObjectValidateUtil.objIsNull(educationhallUser)) {
				req.setAttribute("loginError", "登录失败，用户名不存在！");
				return false;
			}else {
				salt = educationhallUser.getSalt();
				pwd = educationhallUser.getPassword();
			}
		}else if ("2".equals(userRole)) {//高校
			CollegesUser collegesUser = collegesUserDao.selectSaltPwdByUserName(userName);
			if (ObjectValidateUtil.objIsNull(collegesUser)) {
				req.setAttribute("loginError", "登录失败，用户名不存在！");
				return false;
			}else {
				salt = collegesUser.getSalt();
				pwd = collegesUser.getPassword();
			}
			
		}else if ("3".equals(userRole)) {//专家  注意检查是不是首次登录
			ExpertUser expertUser = expertUserDao.selectFirstLoginSaltPwdByIdCard(userName);
			if (ObjectValidateUtil.objIsNull(expertUser)) {
				req.setAttribute("loginError", "登录失败，用户名不存在！");
				return false;
			}else {
				if (!"2".equals(expertUser.getFirstLogin())) {//首次登录
					req.setAttribute("loginError", "系统检测到您是新用户，请使用首次登录方式登录！");
					return false;
				}else{
					salt = expertUser.getSalt();
					pwd = expertUser.getPassword();
				}
			}
		}else {//处室
			DepartmentUser departmentUser = departmentUserDao.selectSaltPwdByUserName(userName);
			if (ObjectValidateUtil.objIsNull(departmentUser)) {
				req.setAttribute("loginError", "登录失败，用户名不存在！");
				return false;
			}else {
				salt = departmentUser.getSalt();
				pwd = departmentUser.getPassword();
			}
		}
		
		String md5Pwd = Md5Util.md5(userPwd, salt);
		if (md5Pwd.equals(pwd)) {
			return true;
		}else {
			return false;
		}
	}
	/**验证用户状态是否正常
	 * @param userRole
	 * @param userName
	 * @param userPwd
	 * @param req
	 * @return
	 */
	public Boolean validateUserStatus(String userRole,String userName,HttpServletRequest req) {
		String status;
		if ("1".equals(userRole)) {//教育厅
			status = educationhallUserDao.selectStatusByUserName(userName);
			
		}else if ("2".equals(userRole)) {//高校
			 status = collegesUserDao.selectStatusByCode(userName);
			 
		}else if ("3".equals(userRole)) {//专家  注意检查是不是首次登录
			status = expertUserDao.selectStatusByIdCard(userName);
			
		}else {//处室
			status = departmentUserDao.selectStatusByUserName(userName);
			 
		}
		if ("2".equals(status)) {
			return false;
		}else {
			return true;
		}
	}
	
}

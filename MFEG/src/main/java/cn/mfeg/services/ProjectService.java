package cn.mfeg.services;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.mfeg.daos.ProjectDao;
import cn.mfeg.models.Project;

@Service
public class ProjectService {

	@Autowired
	ProjectDao projectDao;
	
	public List<Project> selectInfoByIdCard(String idCard) {
		List<Project> projects = projectDao.selectInfoByIdCard(idCard);
		return projects;
	}
	
	public List<Project> selectExcellentProjectByIdCard(String idCard) {
		List<Project> projects = projectDao.selectExcellentProjectByIdCard(idCard);
		return projects;
	}
	
	public List <Project> selectAppraisePnameByIdCard(String idCard){
		List<Project> projects = projectDao.selectAppraisePnameByIdCard(idCard);
		return projects;
	}
	
	/**创建项目
	 * @param project
	 * @return
	 */
	public boolean creatProject(Project project) {
		int result = projectDao.creatProject(project);
		if (result==0) {
			return false;
		}else {
			return true;
		}
	}
	
	/**修改项目
	 * @param project
	 * @return
	 */
	public boolean updateProjectById(Project project) {
		int result = projectDao.updateProjectById(project);
		if (result==0) {
			return false;
		}else {
			return true;
		}
	}
	/**删除项目
	 * @param project
	 * @return
	 */
	public boolean deleteProjectById(String[] ids) {
		int result = 0;
		for(int i=0;i<ids.length;i++){
			result = projectDao.deleteProjectById(ids[i]);
		}
		if (result==0) {
			return false;
		}else {
			return true;
		}
	}
	
	/**查询所有项目信息
	 * @return
	 */
	public List <Project> selectAllProjectInfo(){
		List<Project> projects = projectDao.selectAllProjectInfo();
		return projects;
	}
	/**查询某高校、处室下的所有项目信息
	 * @return
	 */
	public List <Project> selectAllProjectInfoByOrganiserOrganiserRole(Map<String, Object> map){
		List<Project> projects = projectDao.selectAllProjectInfoByOrganiserOrganiserRole(map);
		return projects;
	}
	/**暂停项目
	 * @param project
	 * @return
	 */
	public boolean stopProjectById(String[] ids) {
		int result = 0;
		for(String id:ids){
			result = projectDao.stopProjectById(id);
		}
		if (result==0) {
			return false;
		}else {
			return true;
		}
	}
}

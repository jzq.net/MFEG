package cn.mfeg.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.mfeg.daos.ExpertUserDao;
import cn.mfeg.models.ExpertUser;
import cn.mfeg.models.User;
import cn.mfeg.utils.Md5Util;

/**
 * @author jzq
 *
 */
@Service
public class ExpertUserService {
	@Autowired
	ExpertUserDao expertUserDao;

	/**检验原密码是否正确
	 * @param idCard
	 * @param paramPwd
	 * @return
	 */
	public boolean validatePwd(String idCard,String paramPwd){
		ExpertUser expertUser = expertUserDao.selectPwdSaltByIdCard(idCard);
		String nativePwd = expertUser.getPassword();
		String  salt = expertUser.getSalt();
		paramPwd = Md5Util.md5(paramPwd, salt);
		if (paramPwd.equals(nativePwd)) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean updatePwd(String idCard,String newPwd) {
		ExpertUser expertUser = expertUserDao.selectPwdSaltByIdCard(idCard);
		newPwd = Md5Util.md5(newPwd, expertUser.getSalt());//对密码加密
		expertUser.setIdCard(idCard);
		expertUser.setPassword(newPwd);
		int result = expertUserDao.updatePwd(expertUser);
		if (result==1) {
			return true;
		}else {
			return false;
		}
	}
	
	public ExpertUser selectInfoByIdCard(String idCard){
		return expertUserDao.selectInfoByIdCard(idCard);
	}
	
	public boolean alterDescription(String idCard,String description) {
		ExpertUser expertUser = new ExpertUser();
		expertUser.setIdCard(idCard);
		expertUser.setDescription(description);
		int result = expertUserDao.alterDescription(expertUser);
		if (result==1) {
			return true;
		}else {
			return false;
		}
	}
	
	/**根据项目ID查询参与专家信息
	 * @param projectId
	 * @return
	 */
	public List<ExpertUser> selectInfoByProjectId(String projectId) {
		return expertUserDao.selectInfoByProjectId(projectId);
	}
	/**根据项目ID查询参与的优秀专家信息
	 * @param projectId
	 * @return
	 */
	public List<ExpertUser> selectExcellentExpertInfoByProjectId(String projectId) {
		return expertUserDao.selectExcellentExpertInfoByProjectId(projectId);
	}
	
	/**根据项目ID查询没有参加该项目的专家
	 * @param projectId
	 * @return
	 */
	public List<ExpertUser> selectNotPartakeProjectExpertByProjectId(String projectId) {
		return expertUserDao.selectNotPartakeProjectExpertByProjectId(projectId);
	}
	
	/**根据项目ID查询已添加专家
	 * @param projectId
	 * @return
	 */
	public List<ExpertUser> selectAddedExpertByProjectId(String projectId) {
		return expertUserDao.selectAddedExpertByProjectId(projectId);
	}
	
	/*查询所有专家的信息
	 * @return
	 */
	public List<ExpertUser> selectAllExpertInfo() {
		return expertUserDao.selectAllExpertInfo();
	}
	
	/**禁用用户
	 * @param code
	 * @param newPwd
	 * @return
	 */
	public boolean stopUser(String[] ids) {
		int result = 0;
		for (String id:ids) {
			result = expertUserDao.stopUser(id);
		}
		if (result==1) {
			return true;
		}else {
			return false;
		}
	}
	
	/**修改用户
	 * @param code
	 * @param newPwd
	 * @return
	 */
	public boolean alterUserById(User user) {
		String salt = selectInfoById(user.getId()).getSalt();
		user.setPassword(Md5Util.md5(user.getPassword(), salt));
		int result = expertUserDao.alterUserById(user);
		if (result==1) {
			return true;
		}else {
			return false;
		}
	}
	/**根据id查询用户信息
	 * @param userName
	 * @return
	 */
	public ExpertUser selectInfoById(int id) {
		return expertUserDao.selectInfoById(id);
	}
}

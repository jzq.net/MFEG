package cn.mfeg.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.mfeg.daos.EducationhallUserDao;

@Service
public class EducationhallUserService {
	@Autowired
	EducationhallUserDao educationhallUserDao;
	
	public String selectStatusByUserName(String userName) {
		return educationhallUserDao.selectStatusByUserName(userName);
	}
}

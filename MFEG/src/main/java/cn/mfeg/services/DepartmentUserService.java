package cn.mfeg.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.mfeg.daos.DepartmentUserDao;
import cn.mfeg.models.DepartmentUser;
import cn.mfeg.models.User;
import cn.mfeg.utils.Md5Util;
import cn.mfeg.utils.ObjectValidateUtil;

@Service
public class DepartmentUserService {
	@Autowired
	DepartmentUserDao departmentUserDao;
	
	/**检验原密码是否正确
	 * @param idCard
	 * @param paramPwd
	 * @return
	 */
	public boolean validatePwd(String userName,String paramPwd){
		DepartmentUser departmentUser = departmentUserDao.selectSaltPwdByUserName(userName);
		String nativePwd = departmentUser.getPassword();
		String  salt = departmentUser.getSalt();
		paramPwd = Md5Util.md5(paramPwd, salt);
		if (paramPwd.equals(nativePwd)) {
			return true;
		}else {
			return false;
		}
	}
	/**检验用户名存不存在
	 * @param idCard
	 * @param paramPwd
	 * @return
	 */
	public boolean validateUserName(User user){
		DepartmentUser departmentUser = departmentUserDao.selectInfoByUserName(user.getUserName());
		if (ObjectValidateUtil.objIsNull(departmentUser)) {
			return true;//不存在
		}else {
			return false;
		}
	}
	 
	/**更新密码
	 * @param code
	 * @param newPwd
	 * @return
	 */
	public boolean updatePwd(String username,String newPwd) {
			DepartmentUser departmentUser = departmentUserDao.selectSaltPwdByUserName(username);
			newPwd = Md5Util.md5(newPwd, departmentUser.getSalt());//对密码加密
			departmentUser.setUserName(username);
			departmentUser.setPassword(newPwd);
			int result = departmentUserDao.updatePwd(departmentUser);
			if (result==1) {
				return true;
			}else {
				return false;
			}
		}
	/**添加用户
	 * @param code
	 * @param newPwd
	 * @return
	 */
	public boolean addUser(User user) {
		String salt = Md5Util.getSalt();
		user.setSalt(salt);
		user.setPassword(Md5Util.md5(user.getPassword(), salt));
		int result = departmentUserDao.addUser(user);
		if (result==1) {
			return true;
		}else {
			return false;
		}
	}
	/**修改用户
	 * @param code
	 * @param newPwd
	 * @return
	 */
	public boolean alterUserById(User user) {
		String salt = selectInfoById(user.getId()).getSalt();
		user.setPassword(Md5Util.md5(user.getPassword(), salt));
		int result = departmentUserDao.alterUserById(user);
		if (result==1) {
			return true;
		}else {
			return false;
		}
	}
	/**禁用用户
	 * @param code
	 * @param newPwd
	 * @return
	 */
	public boolean stopUser(String[] ids) {
		int result = 0;
		for (String id:ids) {
			result = departmentUserDao.stopUser(id);
		}
		if (result==1) {
			return true;
		}else {
			return false;
		}
	}
	
	/**根据用户名查询用户信息
	 * @param userName
	 * @return
	 */
	public DepartmentUser selectInfoByUserName(String userName) {
		return departmentUserDao.selectInfoByUserName(userName);
	}
	/**根据id查询用户信息
	 * @param userName
	 * @return
	 */
	public DepartmentUser selectInfoById(int id) {
		return departmentUserDao.selectInfoById(id);
	}
	
	/**查询全部处室用户信息
	 * @return
	 */
	public List<DepartmentUser> selectAllDepartmentUserInfo() {
		return departmentUserDao.selectAllDepartmentUserInfo();
	}
	
}

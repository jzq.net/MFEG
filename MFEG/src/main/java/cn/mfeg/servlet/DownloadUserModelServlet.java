package cn.mfeg.servlet;

import java.io.IOException;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import cn.mfeg.utils.ExportExcelUtil;

public class DownloadUserModelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String userRole = req.getParameter("userRole");
		//创建工作空间
		HSSFWorkbook book = new HSSFWorkbook();
		// 创建一个工作表
		HSSFSheet sheet = book.createSheet("添加用户模板");
		// 创建第一行
		HSSFRow row = sheet.createRow(0);
		HSSFCell cel = null;
		Random random = new Random();
		int val = random.nextInt(999999999);
		// 创建一个单元格 设置值
		if ("2".equals(userRole)) {//高校
			cel = row.createCell(0);
			cel.setCellValue("高校代码");
			cel = row.createCell(1);
			cel.setCellValue("高校名称");
			cel = row.createCell(2);
			cel.setCellValue("用户名");
			cel = row.createCell(3);
			cel.setCellValue("联系人");
			cel = row.createCell(4);
			cel.setCellValue("电话");
			cel = row.createCell(5);
			cel.setCellValue("状态");
			cel = row.createCell(6);
			cel.setCellValue("密码");
			
			row = sheet.createRow(1);
			cel = row.createCell(0);
			cel.setCellValue("高校代码"+val);
			cel = row.createCell(1);
			cel.setCellValue("高校名称"+val);
			cel = row.createCell(2);
			cel.setCellValue("用户"+val);
			cel = row.createCell(3);
			cel.setCellValue("Tom");
			cel = row.createCell(4);
			cel.setCellValue("1590000000");
			cel = row.createCell(5);
			cel.setCellValue("1");
			cel = row.createCell(6);
			cel.setCellValue("mima1234");
		}else if ("3".equals(userRole)) {//专家
			
		}else if ("4".equals(userRole)) {//处室
			cel = row.createCell(0);
			cel.setCellValue("处室名称");
			cel = row.createCell(1);
			cel.setCellValue("用户名");
			cel = row.createCell(2);
			cel.setCellValue("联系人");
			cel = row.createCell(3);
			cel.setCellValue("电话");
			cel = row.createCell(4);
			cel.setCellValue("状态");
			cel = row.createCell(5);
			cel.setCellValue("密码");
			
			row = sheet.createRow(1);
			cel = row.createCell(0);
			cel.setCellValue("处室"+val);
			cel = row.createCell(1);
			cel.setCellValue("用户"+val);
			cel = row.createCell(2);
			cel.setCellValue("Tom");
			cel = row.createCell(3);
			cel.setCellValue("1590000000");
			cel = row.createCell(4);
			cel.setCellValue("1");
			cel = row.createCell(5);
			cel.setCellValue("mima1234");
		}
		ExportExcelUtil.exportExcel(req, resp, book);
	}
}

package cn.mfeg.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import cn.mfeg.models.ExpertUser;
import cn.mfeg.models.Project;
import cn.mfeg.services.ExpertUserService;
import cn.mfeg.services.ProjectService;
import cn.mfeg.utils.ExportExcelUtil;

public class ExportExpertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	ApplicationContext applicationContext;
	ExpertUserService expertUserService;
	ProjectService projectService;
	
	public void init() throws ServletException {
		applicationContext = new ClassPathXmlApplicationContext("spring-servlet.xml");
		expertUserService = (ExpertUserService) applicationContext.getBean(ExpertUserService.class);
		projectService = (ProjectService) applicationContext.getBean(ProjectService.class);
	}
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String projectId = req.getParameter("projectId");
		List<ExpertUser> expertUsers = expertUserService.selectInfoByProjectId(projectId);
		for (ExpertUser expertUser:expertUsers) {
			List<Project> projects = projectService.selectAppraisePnameByIdCard(expertUser.getIdCard());
			String achievement = "";
			for(Project project:projects){
				achievement += (project.getProjectname()+":"+project.getAppraise()+"<br>");
			}
			expertUser.setAchievement(achievement);
		}
		//创建工作空间
		HSSFWorkbook book = new HSSFWorkbook();
		// 创建一个工作表
		HSSFSheet sheet = book.createSheet("专家信息");
		// 创建第一行
		HSSFRow row = sheet.createRow(0);
		// 创建一个单元格 设置值
		HSSFCell cel = null;
		cel = row.createCell(0);
		cel.setCellValue("项目ID");
		cel = row.createCell(1);
		cel.setCellValue("项目名称");
		cel = row.createCell(2);
		cel.setCellValue("专家ID");
		cel = row.createCell(3);
		cel.setCellValue("高校代码");
		cel = row.createCell(4);
		cel.setCellValue("姓名");
		cel = row.createCell(5);
		cel.setCellValue("性别");
		cel = row.createCell(6);
		cel.setCellValue("职务");
		cel = row.createCell(7);
		cel.setCellValue("行业");
		cel = row.createCell(8);
		cel.setCellValue("领域");
		cel = row.createCell(9);
		cel.setCellValue("来源");
		cel = row.createCell(10);
		cel.setCellValue("类型");
		cel = row.createCell(11);
		cel.setCellValue("评价");
		cel = row.createCell(12);
		cel.setCellValue("评语");
		 
		int rowIndex = 1;
		for (ExpertUser eu:expertUsers) {
			row = sheet.createRow(rowIndex++);
			cel = row.createCell(0);
			cel.setCellValue(eu.getProjectId());
			cel = row.createCell(1);
			cel.setCellValue(eu.getProjectName());
			cel = row.createCell(2);
			cel.setCellValue(eu.getId());
			cel = row.createCell(3);
			cel.setCellValue(eu.getCollegesCode());
			cel = row.createCell(4);
			cel.setCellValue(eu.getName());
			cel = row.createCell(5);
			cel.setCellValue(eu.getSex());
			cel = row.createCell(6);
			cel.setCellValue(eu.getPosition());
			cel = row.createCell(7);
			cel.setCellValue(eu.getIndustryName());
			cel = row.createCell(8);
			cel.setCellValue(eu.getResearchArea());
			cel = row.createCell(9);
			cel.setCellValue(eu.getOriginName());
			cel = row.createCell(10);
			cel.setCellValue(eu.getDeclareTypeName());
			cel = row.createCell(11);
			cel.setCellValue(eu.getAppraise());
			cel = row.createCell(12);
			cel.setCellValue(eu.getComment());
		}
		ExportExcelUtil.exportExcel(req, resp, book);
	}
}

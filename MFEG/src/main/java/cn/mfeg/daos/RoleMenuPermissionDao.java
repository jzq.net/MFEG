package cn.mfeg.daos;

import java.util.List;

public interface RoleMenuPermissionDao {
    List<String> selectUrlByroleType(String userRole);
}
package cn.mfeg.daos.impls;

import java.util.List;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import cn.mfeg.daos.DepartmentUserDao;
import cn.mfeg.models.DepartmentUser;
import cn.mfeg.models.User;

@Repository
public class DepartmentUserDaoImpl extends SqlSessionDaoSupport implements DepartmentUserDao {

	@Override
	public DepartmentUser selectSaltPwdByUserName(String userName) {
		DepartmentUser departmentUser = getSqlSession().getMapper(DepartmentUserDao.class).selectSaltPwdByUserName(userName);
		return departmentUser;
	}

	@Override
	public int updatePwd(DepartmentUser departmentUser) {
		int result = getSqlSession().getMapper(DepartmentUserDao.class).updatePwd(departmentUser);
		return result;
	}

	@Override
	public DepartmentUser selectInfoByUserName(String userName) {
		DepartmentUser departmentUser = getSqlSession().getMapper(DepartmentUserDao.class).selectInfoByUserName(userName);
		return departmentUser;
	}

	@Override
	public List<DepartmentUser> selectAllDepartmentUserInfo() {
		List<DepartmentUser> departmentUsers = getSqlSession().getMapper(DepartmentUserDao.class).selectAllDepartmentUserInfo();
		return departmentUsers;
	}

	@Override
	public int addUser(User user) {
		int result = getSqlSession().getMapper(DepartmentUserDao.class).addUser(user);
		return result;
	}

	@Override
	public int stopUser(String id) {
		int result = getSqlSession().getMapper(DepartmentUserDao.class).stopUser(id);
		return result;
	}

	@Override
	public int alterUserById(User user) {
		int result = getSqlSession().getMapper(DepartmentUserDao.class).alterUserById(user);
		return result;
	}

	@Override
	public DepartmentUser selectInfoById(int id) {
		DepartmentUser departmentUser = getSqlSession().getMapper(DepartmentUserDao.class).selectInfoById(id);
		return departmentUser;
	}

	@Override
	public String selectStatusByUserName(String userName) {
		return getSqlSession().getMapper(DepartmentUserDao.class).selectStatusByUserName(userName);
	}

}

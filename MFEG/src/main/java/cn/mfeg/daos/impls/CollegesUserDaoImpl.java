package cn.mfeg.daos.impls;

import java.util.List;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import cn.mfeg.daos.CollegesUserDao;
import cn.mfeg.models.CollegesUser;
import cn.mfeg.models.User;

@Repository
public class CollegesUserDaoImpl extends SqlSessionDaoSupport implements CollegesUserDao {

	@Override
	public CollegesUser selectSaltPwdByUserName(String code) {
		CollegesUser collegesUser = getSqlSession().getMapper(CollegesUserDao.class).selectSaltPwdByUserName(code);
		return collegesUser;
	}

	@Override
	public int updatePwd(CollegesUser collegesUser) {
		int result = getSqlSession().getMapper(CollegesUserDao.class).updatePwd(collegesUser);
		return result;
	}

	@Override
	public CollegesUser selectInfoByCode(String code) {
		CollegesUser collegesUser = getSqlSession().getMapper(CollegesUserDao.class).selectInfoByCode(code);
		return collegesUser;
	}

	@Override
	public List<CollegesUser> selectAllCollegesUserInfo() {
		List<CollegesUser> collegesUsers = getSqlSession().getMapper(CollegesUserDao.class).selectAllCollegesUserInfo();
		return collegesUsers;
	}

	@Override
	public int addUser(User user) {
		int result = getSqlSession().getMapper(CollegesUserDao.class).addUser(user);
		return result;
	}

	@Override
	public int stopUser(String id) {
		int result = getSqlSession().getMapper(CollegesUserDao.class).stopUser(id);
		return result;
	}

	@Override
	public int alterUserById(User user) {
		int result = getSqlSession().getMapper(CollegesUserDao.class).alterUserById(user);
		return result;
	}

	@Override
	public CollegesUser selectInfoById(int id) {
		CollegesUser collegesUser = getSqlSession().getMapper(CollegesUserDao.class).selectInfoById(id);
		return collegesUser;
	}

	@Override
	public String selectStatusByCode(String code) {
		String status = getSqlSession().getMapper(CollegesUserDao.class).selectStatusByCode(code);
		return status;
	}

}

package cn.mfeg.daos.impls;

import java.util.List;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import cn.mfeg.daos.ExpertUserDao;
import cn.mfeg.models.ExpertUser;
import cn.mfeg.models.User;

@Repository
public class ExpertUserDaoImpl extends SqlSessionDaoSupport implements ExpertUserDao {

	@Override
	public ExpertUser selectFirstLoginMobileByIdCard(String idCard) {
		ExpertUser expertUser = getSqlSession().getMapper(ExpertUserDao.class).selectFirstLoginMobileByIdCard(idCard);
		return expertUser;
	}

	@Override
	public ExpertUser selectFirstLoginSaltPwdByIdCard(String idCard) {
		ExpertUser expertUser = getSqlSession().getMapper(ExpertUserDao.class).selectFirstLoginSaltPwdByIdCard(idCard);
		return expertUser;
	}

	@Override
	public int updateFirstLogin(String idCard) {
		int result = getSqlSession().getMapper(ExpertUserDao.class).updateFirstLogin(idCard);
		return result;
	}

	@Override
	public ExpertUser selectPwdSaltByIdCard(String idCard) {
		ExpertUser expertUser = getSqlSession().getMapper(ExpertUserDao.class).selectPwdSaltByIdCard(idCard);
		return expertUser;
	}

	@Override
	public int updatePwd(ExpertUser expertUser) {
		int result = getSqlSession().getMapper(ExpertUserDao.class).updatePwd(expertUser);
		return result;
	}

	@Override
	public ExpertUser selectInfoByIdCard(String idCard) {
		ExpertUser expertUser = getSqlSession().getMapper(ExpertUserDao.class).selectInfoByIdCard(idCard);
		return expertUser;
	}

	@Override
	public int alterDescription(ExpertUser expertUser) {
		int result = getSqlSession().getMapper(ExpertUserDao.class).alterDescription(expertUser);
		return result;
	}

	@Override
	public List<ExpertUser> selectInfoByProjectId(String projectId) {
		List<ExpertUser> expertUsers = getSqlSession().getMapper(ExpertUserDao.class).selectInfoByProjectId(projectId);
		return expertUsers;
	}

	@Override
	public List<ExpertUser> selectNotPartakeProjectExpertByProjectId(String projectId) {
		List<ExpertUser> expertUsers = getSqlSession().getMapper(ExpertUserDao.class).selectNotPartakeProjectExpertByProjectId(projectId);
		return expertUsers;
	}

	@Override
	public List<ExpertUser> selectAddedExpertByProjectId(String projectId) {
		List<ExpertUser> expertUsers = getSqlSession().getMapper(ExpertUserDao.class).selectAddedExpertByProjectId(projectId);
		return expertUsers;
	}

	@Override
	public List<ExpertUser> selectAllExpertInfo() {
		List<ExpertUser> expertUsers = getSqlSession().getMapper(ExpertUserDao.class).selectAllExpertInfo();
		return expertUsers;
	}

	@Override
	public int stopUser(String id) {
		int result = getSqlSession().getMapper(ExpertUserDao.class).stopUser(id);
		return result;
	}

	@Override
	public int alterUserById(User user) {
		int result = getSqlSession().getMapper(ExpertUserDao.class).alterUserById(user);
		return result;
	}

	@Override
	public ExpertUser selectInfoById(int id) {
		ExpertUser expertUser = getSqlSession().getMapper(ExpertUserDao.class).selectInfoById(id);
		return expertUser;
	}

	@Override
	public List<ExpertUser> selectExcellentExpertInfoByProjectId(String projectId) {
		List<ExpertUser> expertUsers = getSqlSession().getMapper(ExpertUserDao.class).selectExcellentExpertInfoByProjectId(projectId);
		return expertUsers;
	}

	@Override
	public String selectStatusByIdCard(String idCard) {
		return getSqlSession().getMapper(ExpertUserDao.class).selectStatusByIdCard(idCard);
	}

}

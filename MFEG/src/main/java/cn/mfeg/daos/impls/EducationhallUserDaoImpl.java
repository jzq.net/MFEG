package cn.mfeg.daos.impls;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

import cn.mfeg.daos.EducationhallUserDao;
import cn.mfeg.models.EducationhallUser;

@Repository
public class EducationhallUserDaoImpl extends SqlSessionDaoSupport implements EducationhallUserDao{

	@Override
	public EducationhallUser selectSaltPwdByIdCard(String idCard) {
		EducationhallUser educationhallUser = getSqlSession().getMapper(EducationhallUserDao.class).selectSaltPwdByIdCard(idCard);
		return educationhallUser;
	}

	@Override
	public String selectStatusByUserName(String userName) {
		return getSqlSession().getMapper(EducationhallUserDao.class).selectStatusByUserName(userName);
	}

}

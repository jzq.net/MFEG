package cn.mfeg.daos.impls;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

import cn.mfeg.daos.ProjectExpertDao;
import cn.mfeg.models.ProjectExpert;

@Repository
public class ProjectExpertDaoImpl extends SqlSessionDaoSupport implements ProjectExpertDao {

	@Override
	public int insertProjectExpertByPidEid(ProjectExpert projectExpert) {
		int result = getSqlSession().getMapper(ProjectExpertDao.class).insertProjectExpertByPidEid(projectExpert);
		return result;
	}

	@Override
	public int deleteProjectExpertById(String id) {
		int result = getSqlSession().getMapper(ProjectExpertDao.class).deleteProjectExpertById(id);
		return result;
	}

	@Override
	public String checkExpertByProjectIdExpertId(ProjectExpert projectExpert) {
		String id = getSqlSession().getMapper(ProjectExpertDao.class).checkExpertByProjectIdExpertId(projectExpert);
		return id;
	}

	@Override
	public int insertAppraiseCommentByPidEid(ProjectExpert projectExpert) {
		int result = getSqlSession().getMapper(ProjectExpertDao.class).insertAppraiseCommentByPidEid(projectExpert);
		return result;
	}

}

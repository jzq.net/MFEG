package cn.mfeg.daos.impls;

import java.util.List;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import cn.mfeg.daos.RoleMenuPermissionDao;

@Repository
public class RoleMenuPermissionDaoImpl extends SqlSessionDaoSupport implements RoleMenuPermissionDao {

	@Override
	public List<String> selectUrlByroleType(String userRole) {
		List<String> urls = getSqlSession().getMapper(RoleMenuPermissionDao.class).selectUrlByroleType(userRole);
		return urls;
	}

}

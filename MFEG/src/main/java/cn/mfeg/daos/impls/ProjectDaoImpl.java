package cn.mfeg.daos.impls;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

import cn.mfeg.daos.ProjectDao;
import cn.mfeg.models.Project;

@Repository
public class ProjectDaoImpl extends SqlSessionDaoSupport implements ProjectDao {

	@Override
	public List<Project> selectInfoByIdCard(String idCard) {
		List<Project> projects = getSqlSession().getMapper(ProjectDao.class).selectInfoByIdCard(idCard);
		return projects;
	}

	@Override
	public List<Project> selectAppraisePnameByIdCard(String idCard) {
		List<Project> projects = getSqlSession().getMapper(ProjectDao.class).selectAppraisePnameByIdCard(idCard);
		return projects;
	}

	@Override
	public List<Project> selectProjectInfoByCode(Project project) {
		List<Project> projects = getSqlSession().getMapper(ProjectDao.class).selectProjectInfoByCode(project);
		return projects;
	}

	@Override
	public int creatProject(Project project) {
		int result = getSqlSession().getMapper(ProjectDao.class).creatProject(project);
		return result;
	}

	@Override
	public int updateProjectById(Project project) {
		int result = getSqlSession().getMapper(ProjectDao.class).updateProjectById(project);
		return result;
	}

	@Override
	public int deleteProjectById(String id) {
		int result = getSqlSession().getMapper(ProjectDao.class).deleteProjectById(id);
		return result;
	}

	@Override
	public List<Project> selectAllProjectInfo() {
		List<Project> projects = getSqlSession().getMapper(ProjectDao.class).selectAllProjectInfo();
		return projects;
	}

	@Override
	public int stopProjectById(String id) {
		int result = getSqlSession().getMapper(ProjectDao.class).stopProjectById(id);
		return result;
	}

	@Override
	public List<Project> selectAllProjectInfoByOrganiserOrganiserRole(Map<String, Object> map) {
		List<Project> projects = getSqlSession().getMapper(ProjectDao.class).selectAllProjectInfoByOrganiserOrganiserRole(map);
		return projects;
	}

	@Override
	public List<Project> selectExcellentProjectByIdCard(String idCard) {
		List<Project> projects = getSqlSession().getMapper(ProjectDao.class).selectExcellentProjectByIdCard(idCard);
		return projects;
	}

}

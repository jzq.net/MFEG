package cn.mfeg.daos;

import cn.mfeg.models.EducationhallUser;

public interface EducationhallUserDao {
	
	EducationhallUser selectSaltPwdByIdCard(String idCard);
	String selectStatusByUserName(String userName);
}
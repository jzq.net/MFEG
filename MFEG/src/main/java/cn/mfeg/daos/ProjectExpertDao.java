package cn.mfeg.daos;

import cn.mfeg.models.ProjectExpert;

public interface ProjectExpertDao {
	int insertProjectExpertByPidEid(ProjectExpert projectExpert);
	int deleteProjectExpertById(String id);
	String checkExpertByProjectIdExpertId(ProjectExpert projectExpert);
	int insertAppraiseCommentByPidEid(ProjectExpert projectExpert);
}
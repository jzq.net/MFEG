package cn.mfeg.daos;

import java.util.List;

import cn.mfeg.models.ExpertUser;
import cn.mfeg.models.User;

public interface ExpertUserDao {
	ExpertUser selectFirstLoginMobileByIdCard(String idCard);
	ExpertUser selectFirstLoginSaltPwdByIdCard(String idCard);
	int updateFirstLogin(String idCard);
	ExpertUser selectPwdSaltByIdCard(String idCard);
	int updatePwd(ExpertUser expertUser);
	ExpertUser selectInfoByIdCard(String idCard);
	int alterDescription(ExpertUser expertUser);
	List<ExpertUser> selectInfoByProjectId(String projectId);
	List<ExpertUser> selectNotPartakeProjectExpertByProjectId(String projectId);
	List<ExpertUser> selectAddedExpertByProjectId(String projectId);
	List<ExpertUser> selectAllExpertInfo();
	int stopUser(String id);
	int alterUserById(User user);
	ExpertUser selectInfoById(int id);
	List<ExpertUser> selectExcellentExpertInfoByProjectId(String projectId);
	String selectStatusByIdCard(String idCard);
}
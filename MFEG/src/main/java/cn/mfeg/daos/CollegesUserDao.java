package cn.mfeg.daos;

import java.util.List;

import cn.mfeg.models.CollegesUser;
import cn.mfeg.models.User;

public interface CollegesUserDao {
	CollegesUser selectSaltPwdByUserName(String code);
	int updatePwd(CollegesUser collegesUser);
	CollegesUser selectInfoByCode(String code);
	CollegesUser selectInfoById(int id);
	List<CollegesUser> selectAllCollegesUserInfo();
	int addUser(User user);
	int stopUser(String id);
	int alterUserById(User user);
	String selectStatusByCode(String code);
}
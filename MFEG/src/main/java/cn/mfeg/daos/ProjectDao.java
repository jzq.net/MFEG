package cn.mfeg.daos;

import java.util.List;
import java.util.Map;

import cn.mfeg.models.Project;

public interface ProjectDao {
	List <Project> selectInfoByIdCard(String idCard);
	List <Project> selectAppraisePnameByIdCard(String idCard);
	List <Project> selectProjectInfoByCode(Project project);
	int creatProject(Project project);
	int updateProjectById(Project project);
	int deleteProjectById(String id);
	List <Project> selectAllProjectInfo();
	int stopProjectById(String id);
	List <Project> selectAllProjectInfoByOrganiserOrganiserRole(Map<String, Object> map);
	List <Project> selectExcellentProjectByIdCard(String idCard);
}
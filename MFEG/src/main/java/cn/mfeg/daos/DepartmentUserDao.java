package cn.mfeg.daos;

import java.util.List;

import cn.mfeg.models.DepartmentUser;
import cn.mfeg.models.User;

public interface DepartmentUserDao {
	DepartmentUser selectSaltPwdByUserName(String userName);
	int updatePwd(DepartmentUser departmentUser);
	DepartmentUser selectInfoByUserName(String userName);
	DepartmentUser selectInfoById(int id);
	List <DepartmentUser> selectAllDepartmentUserInfo();
	int addUser(User user);
	int stopUser(String id);
	int alterUserById(User user);
	String selectStatusByUserName(String username);
}
package cn.mfeg.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ExpertUser implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

    private Integer collegesId;

    private String username;

    private String name;

    private String sex;

    private String position;

    private Integer jobTitleId;

    private Integer educationId;

    private Integer originId;

    private String originOther;

    private String workUnit;

    private Integer rigionId;

    private Integer industryId;

    private String researchArea;

    private String telephone;

    private String mobile;

    private String email;

    private String idCard;

    private Integer declareTypeId;

    private Integer auditStatus;

    private String password;

    private Integer logintime;

    private String salt;

    private String bankCardId;

    private String bankname;

    private String bank;

    private String status;

    private Date createTime;

    private Date updateTime;
    
    private String description;

    private String auditOpinion;
    
    private String firstLogin;
    
    private List<Project> projects;
    
    private String jobTitleName;//职称
    private String educationName;//学历
    private String originName;//来源
    private String rigionName;//城市
    private String industryName;//行业
    private String declareTypeName;//申报类型
    private String appraise;//评价
    private String comment;//评语
    private String collegesCode;//高校代码
    private String collegesName;//高校名称
    private String achievement;//业绩
    private String projectName;
    private String projectId;
    private String peStatus;//是否参加
    private String peId;//projectExpert主键
    List<Project> allProjects;
    List<Project> excellentProjects;
    
    public List<Project> getAllProjects() {
		return allProjects;
	}

	public void setAllProjects(List<Project> allProjects) {
		this.allProjects = allProjects;
	}

	public List<Project> getExcellentProjects() {
		return excellentProjects;
	}

	public void setExcellentProjects(List<Project> excellentProjects) {
		this.excellentProjects = excellentProjects;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public String getCollegesName() {
		return collegesName;
	}

	public void setCollegesName(String collegesName) {
		this.collegesName = collegesName;
	}

	public String getPeId() {
		return peId;
	}

	public void setPeId(String peId) {
		this.peId = peId;
	}

	public String getPeStatus() {
		return peStatus;
	}

	public void setPeStatus(String peStatus) {
		this.peStatus = peStatus;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getAchievement() {
		return achievement;
	}

	public void setAchievement(String achievement) {
		this.achievement = achievement;
	}

	public String getCollegesCode() {
		return collegesCode;
	}

	public void setCollegesCode(String collegesCode) {
		this.collegesCode = collegesCode;
	}

	public String getAppraise() {
		return appraise;
	}

	public void setAppraise(String appraise) {
		this.appraise = appraise;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getJobTitleName() {
		return jobTitleName;
	}

	public void setJobTitleName(String jobTitleName) {
		this.jobTitleName = jobTitleName;
	}

	public String getEducationName() {
		return educationName;
	}

	public void setEducationName(String educationName) {
		this.educationName = educationName;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getRigionName() {
		return rigionName;
	}

	public void setRigionName(String rigionName) {
		this.rigionName = rigionName;
	}

	public String getIndustryName() {
		return industryName;
	}

	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

	public String getDeclareTypeName() {
		return declareTypeName;
	}

	public void setDeclareTypeName(String declareTypeName) {
		this.declareTypeName = declareTypeName;
	}

	public String getFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(String firstLogin) {
		this.firstLogin = firstLogin;
	}

	public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuditOpinion() {
        return auditOpinion;
    }

    public void setAuditOpinion(String auditOpinion) {
        this.auditOpinion = auditOpinion;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCollegesId() {
        return collegesId;
    }

    public void setCollegesId(Integer collegesId) {
        this.collegesId = collegesId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getJobTitleId() {
        return jobTitleId;
    }

    public void setJobTitleId(Integer jobTitleId) {
        this.jobTitleId = jobTitleId;
    }

    public Integer getEducationId() {
        return educationId;
    }

    public void setEducationId(Integer educationId) {
        this.educationId = educationId;
    }

    public Integer getOriginId() {
        return originId;
    }

    public void setOriginId(Integer originId) {
        this.originId = originId;
    }

    public String getOriginOther() {
        return originOther;
    }

    public void setOriginOther(String originOther) {
        this.originOther = originOther;
    }

    public String getWorkUnit() {
        return workUnit;
    }

    public void setWorkUnit(String workUnit) {
        this.workUnit = workUnit;
    }

    public Integer getRigionId() {
        return rigionId;
    }

    public void setRigionId(Integer rigionId) {
        this.rigionId = rigionId;
    }

    public Integer getIndustryId() {
        return industryId;
    }

    public void setIndustryId(Integer industryId) {
        this.industryId = industryId;
    }

    public String getResearchArea() {
        return researchArea;
    }

    public void setResearchArea(String researchArea) {
        this.researchArea = researchArea;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public Integer getDeclareTypeId() {
        return declareTypeId;
    }

    public void setDeclareTypeId(Integer declareTypeId) {
        this.declareTypeId = declareTypeId;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getLogintime() {
        return logintime;
    }

    public void setLogintime(Integer logintime) {
        this.logintime = logintime;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getBankCardId() {
        return bankCardId;
    }

    public void setBankCardId(String bankCardId) {
        this.bankCardId = bankCardId;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

	@Override
	public String toString() {
		return "ExpertUser [id=" + id + ", collegesId=" + collegesId + ", username=" + username + ", name=" + name
				+ ", sex=" + sex + ", position=" + position + ", jobTitleId=" + jobTitleId + ", educationId="
				+ educationId + ", originId=" + originId + ", originOther=" + originOther + ", workUnit=" + workUnit
				+ ", rigionId=" + rigionId + ", industryId=" + industryId + ", researchArea=" + researchArea
				+ ", telephone=" + telephone + ", mobile=" + mobile + ", email=" + email + ", idCard=" + idCard
				+ ", declareTypeId=" + declareTypeId + ", auditStatus=" + auditStatus + ", password=" + password
				+ ", logintime=" + logintime + ", salt=" + salt + ", bankCardId=" + bankCardId + ", bankname="
				+ bankname + ", bank=" + bank + ", status=" + status + ", createTime=" + createTime + ", updateTime="
				+ updateTime + ", description=" + description + ", auditOpinion=" + auditOpinion + ", firstLogin="
				+ firstLogin + ", jobTitleName=" + jobTitleName + ", educationName=" + educationName + ", originName="
				+ originName + ", rigionName=" + rigionName + ", industryName=" + industryName + ", declareTypeName="
				+ declareTypeName + ", appraise=" + appraise + ", comment=" + comment + ", collegesCode=" + collegesCode
				+ ", achievement=" + achievement + ", projectName=" + projectName + "]";
	}
    
}
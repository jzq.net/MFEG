package cn.mfeg.models;

import java.io.Serializable;
import java.sql.Date;

public class Project implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

    private String projectcode;

    private String projectname;

    private String organiser;

    private String organiserrole;

    private String status;

    private String hostunit;

    private String phonenum;

    private String website;

    private Date starttime;
    
    private Date endtime;

    private Integer experttotal;

    private Date createTime;

    private Date updateTime;
    
    private String appraise;
    
    private String comment;
    
    private String startTime;
    
    private String endTime;
    
    private Integer disabledTime;
    
	public Integer getDisabledTime() {
		return disabledTime;
	}

	public void setDisabledTime(Integer disabledTime) {
		this.disabledTime = disabledTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getAppraise() {
		return appraise;
	}

	public void setAppraise(String appraise) {
		this.appraise = appraise;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProjectcode() {
        return projectcode;
    }

    public void setProjectcode(String projectcode) {
        this.projectcode = projectcode;
    }

    public String getProjectname() {
        return projectname;
    }

    public void setProjectname(String projectname) {
        this.projectname = projectname;
    }

    public String getOrganiser() {
        return organiser;
    }

    public void setOrganiser(String organiser) {
        this.organiser = organiser;
    }

    public String getOrganiserrole() {
        return organiserrole;
    }

    public void setOrganiserrole(String organiserrole) {
        this.organiserrole = organiserrole;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHostunit() {
        return hostunit;
    }

    public void setHostunit(String hostunit) {
        this.hostunit = hostunit;
    }

    public String getPhonenum() {
        return phonenum;
    }

    public void setPhonenum(String phonenum) {
        this.phonenum = phonenum;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(java.sql.Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public Integer getExperttotal() {
        return experttotal;
    }

    public void setExperttotal(Integer experttotal) {
        this.experttotal = experttotal;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

	@Override
	public String toString() {
		return "Project [id=" + id + ", projectcode=" + projectcode + ", projectname=" + projectname + ", organiser="
				+ organiser + ", organiserrole=" + organiserrole + ", status=" + status + ", hostunit=" + hostunit
				+ ", phonenum=" + phonenum + ", website=" + website + ", starttime=" + starttime + ", endtime="
				+ endtime + ", experttotal=" + experttotal + ", createTime=" + createTime + ", updateTime=" + updateTime
				+ ", appraise=" + appraise + ", comment=" + comment + ", startTime=" + startTime + ", endTime="
				+ endTime + ", disabledTime=" + disabledTime + "]";
	}
    
}
var win = window;
var nav = win.navigator;
var doc = win.document;
var ieAX = win.ActiveXObject;
var ieMode = doc.documentMode;
var ieVer = _getIeVersion() || ieMode || 0;
var isIe = ieAX || ieMode;
/**
 * 检测 external 是否包含该字段
 * @param reg 正则
 * @param type 检测类型，0为键，1为值
 * @returns {boolean}
 * @private
 */
function _testExternal(reg, type) {
    var external = win.external || {};

    for (var i in external) {
        if (reg.test(type ? external[i] : i)) {
            return true;
        }
    }

    return false;
}
function _getChromiumType() {

    if (!!ieVer||isIe) {
        return 'ie'+ieVer;
    }

    if(navigator.userAgent.indexOf('Firefox')!=-1){
        return 'firefox';
    }
    if(navigator.userAgent.indexOf("Opera") > -1){
        return "opera"
    }

    // 搜狗浏览器
    if (_testExternal(/^sogou/i, 0)) {
        return 'sogou';
    }

    // 猎豹浏览器
    if (_testExternal(/^liebao/i, 0)) {
        return 'liebao';
    }


    if(navigator.userAgent.toLowerCase().indexOf('chrome') > -1 ) {
        var desc = navigator.mimeTypes['application/x-shockwave-flash'].description.toLowerCase();
        if (desc.indexOf('adobe') > -1) {
            var _track = 'track' in document.createElement('track');
            var webstoreKeysLength = win.chrome && win.chrome.webstore ? Object.keys(win.chrome.webstore).length : 0;
            if (_track) {
                // 360极速浏览器
                // 360安全浏览器
                return webstoreKeysLength > 1 ? '360ee' : '360se';
            }
        }else{
            return 'chrome';
        }
    }

    if (navigator.userAgent.indexOf("Safari") > -1) {
        return "safari";
    }
}


// 获得ie浏览器版本

function _getIeVersion() {
    var v = 3,
        p = document.createElement('p'),
        all = p.getElementsByTagName('i');

    while (
        p.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
            all[0]);

    return v > 4 ? v : 0;
}
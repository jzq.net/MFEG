function getLastPageIndex(){
     var totalCount=$.trim($("#totalSpan").html())==""?0:$("#totalSpan").html();
     var pageSize=$.trim($("#pageSizeSel").val())==""?10:$("#pageSizeSel").val();
     return Math.ceil(parseInt(totalCount)/parseInt(pageSize))
}
function getPageIndex() {
	var pageIndex=$("#pageIndex").val();
	if(pageIndex!=""){
		return parseInt(pageIndex);
	}else{
		return 1;
	}   
}
function setPageIndex(pi) {
    $("#pageIndex").val(pi);
}
function setPageSize(pi) {
    $("#pageSize").val(pi);
}
function removeDuplicateElements(elements) {
    if(elements.length < 2) {
        return;
    }
    for(var i = 1; i <= elements.length - 1; i++) {
        $(elements[i]).remove();
    }
}
function removeDuplicatePageInfo() {
    $("form").each(function (i, obj) {
        var hdnPIList = $(obj).find("input[name='pageIndex']");
        var hdnPSList = $(obj).find("input[name='pageSize']");
        removeDuplicateElements(hdnPIList);
        removeDuplicateElements(hdnPSList);
    });
}
function pageIndexChange(obj){
	  removeDuplicatePageInfo();
      setPageIndex($(obj).val());
      $(obj).closest("form").submit();
}
function pageSizeChange(obj){
      setPageSize($(obj).val());
      $(obj).closest("form").submit();
}
function firstPageClick(obj){
	 setPageIndex(1);
     $(obj).closest("form").submit();
}
function previousPageClick(obj){
	  var pi = getPageIndex() - 1;
      if(pi < 1) {
         pi = 1;
      }
      setPageIndex(pi);
      $(obj).closest("form").submit();
}
function nextPageClick(obj){
	 var pi = getPageIndex() + 1;
	 if(pi >getLastPageIndex()) {
        pi = getLastPageIndex();
     }
     setPageIndex(pi);
     $(obj).closest("form").submit();
}
function lastPageClick(obj){
	 setPageIndex(getLastPageIndex());
     $(obj).closest("form").submit();
}
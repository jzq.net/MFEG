$(function(){
	$('.form_datetime').datepicker({format: 'yyyy-mm-dd'});
	 $('#myModal').modal('hide');
	 $("#school_id").select2(commonSelect2Ajax("/student/school"));
	 $("#saveBtn").click(function(){
		   var validResult=$("#saveForm").valid();
		   if(validResult){
			     if(selfFormValid()){
			    	 if(mailExitValid($("#mail").val())){
			    		 commonAlert("该邮箱账号已被注册过,请更换")
			    		 return false;
			    	 }else{
			    		 $("#saveForm").attr("action","/student/doRegister");
				    	 $("#saveForm").submit();
			    	 }
			     }
		   }
	 });
	 $("input").keyup(function(){
		  $(this).parents(".form-group").find(".errorself").css("display","none");
	 });
	 $("select").change(function(){
		 $(this).parents(".form-group").find(".errorself").css("display","none");
	 });
	 $("input:checkbox").change(function(){
		 $(this).parents(".col-sm-offset-2").find(".errorself").css("display","none");
	 });
	 $("#sendMailBtn").click(function(){
		 sendValidCodeMail($(this),$("#mail").val(),"register");
	 });
});
function selfFormValid(){
	var password=$.trim($("#password").val());
	if(checkStrong(password)<3){
		$("#passwordError").css("display","block");
		return false;
	}
	var confirmPassword=$.trim($("#confirm_password").val());
	if(password!=confirmPassword){
		$("#confirm_passwordError").css("display","block");
		return false;
	}
	var group=$.trim($("#school_group_id").val());
	if(group==0){
		$("#school_group_idError").css("display","block");
		return false;
	}
	var school=$.trim($("#school_id").val());
	if(school==""){
		$("#school_idError").css("display","block");
		return false;
	}
	
	var mobileRegx=/^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
	var mobile=$.trim($("#telephone").val());
	if(!mobileRegx.test(mobile)){
		$("#telephoneError").css("display","block");
		return false;
	}
    var validCode=$.trim($("#validcode").val());
    if(validCode==""){
    	$("#validcodeError").html("请输入验证码");
    	$("#validcodeError").css("display","inline-block");
    	return false;
    }
    if(!$("#agree").is(':checked')){
    	$("#agreeError").css("display","block");
    	return false;
    }
    var validRes=validCodeCheck("register",validCode,$.trim($("#mail").val()),"validcodeError");
    if(!validRes){
    	$("#validcodeError").css("display","inline-block");
    	return false;
    }
    
    return true;
}
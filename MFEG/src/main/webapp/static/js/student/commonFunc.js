/**
 * 发送验证码邮件
 * @param btnObj
 * @param mail
 * @param type
 */
function sendValidCodeMail(obj,mail,type){
	 if($.trim(mail)!=""){
		  obj.attr("disabled","disabled");
		  $.ajax({
			   type:"POST",
				url:"/student/sendMail",
				async:false,
				data:{
					mailAccount:mail,
					type:type
				},
				success:function(res){
					if(res.success){
						var countdown=60;
						obj.attr("disabled","disabled");
						sl=setInterval(function(){
							if(countdown==0){
								clearInterval(sl);
								obj.removeAttr("disabled");
								obj.html("给邮箱发送验证码");
								countdown=60;
							}else{
							    countdown--;
								obj.html(countdown+"秒后可重发");
							}
						}, 1000); 
					}else{
						commonAlert(res.message);
						obj.removeAttr("disabled");
					}
				},
				error:function(){
					commonAlert("网络连接错误");
					obj.removeAttr("disabled");
				}
		  });
	 }else{
		 var mailReg=/^[a-z\d]+(\.[a-z\d]+)*@([\da-z](-[\da-z])?)+(\.{1,2}[a-z]+)+$/;
		 if(!mailReg.test(mail)){
			 commonAlert("邮箱账号不合法");
		 }
	 }
}

/**
 * 验证码验证
 * @param validCode
 */
function validCodeCheck(type,validCode,mailAccount,tipLableId){
	if($.trim(validCode)==""){
		return false;
	}
	var result=false;
    $.ajax({
    	type:"POST",
    	url:"/student/codeValid",
    	async:false,
    	data:{
    		mailAccount:mailAccount,
    		code:validCode,
    		type:type
    	},
    	success:function(res){
    		if(res.success){
    			result=true;
    		}else{
    			if($("#"+tipLableId).length>0){
    				$("#"+tipLableId).html(res.message);
    			}
    		}
    	}
    });
    return result;
}

/**
 * 邮箱是否存在验证
 * @param mail
 */
function mailExitValid(mail){
	if($.trim(mail)==""){
		return false;
	}
	var result=false;
	$.ajax({
		type:"POST",
		async:false,
		url:"/student/mailExit",
		data:{
			mail:mail
		},
		success:function(res){
			if(res.success){
				result=true;
			}
		}
	});
	return result;
}
/**
 * 通用提示框
 * @param msg
 */
function commonAlert(msg){
	 bootbox.alert({  
         buttons: {  
            ok: {  
                 label: '确定',  
                 className: 'btn-default'  
             }  
         },  
         message: msg,
         title:'提示'
     });  
}
/* 
返回密码的强度级别 
*/  
function checkStrong(sPW) {  
	if (sPW.length <= 4)  
	    return 0; //密码太短    
	Modes = 0;  
	for (i = 0; i < sPW.length; i++) {  
	    //测试每一个字符的类别并统计一共有多少种模式.    
	    Modes |= CharMode(sPW.charCodeAt(i));  
	}  
	return bitTotal(Modes);  
} 
function CharMode(iN) {  
    if (iN >= 48 && iN <= 57) //数字    
        return 1;  
    if (iN >= 65 && iN <= 90) //大写字母    
        return 2;  
    if (iN >= 97 && iN <= 122) //小写    
        return 4;  
    else  
        return 8; //特殊字符    
}  
/* 
    统计字符类型 
*/  
function bitTotal(num) {  
    modes = 0;  
    for (i = 0; i < 4; i++) {  
        if (num & 1) modes++;  
        num >>>= 1;  
    }  
    return modes;  
}
function commonSelect2Ajax(url){
	return {
		  allowClear: true,
		  placeholder: "请选择",
  	      tokenSeparators: [','],
	      ajax: {
	          url: url,
	          dataType: 'json',
	          delay: 250,
	          data: function (params) {
	            return {
	              q: params.term, // search term
	              page: params.page
	            };
	          },
	          processResults: function (data, params) {
	            params.page = params.page || 1;
	            var result=[];
	            if(data){
	            	for(var i=0;i<data.length;i++){
	            		result.push({id:data[i].id,name:data[i].name,code:data[i].code});
	            	}
	            }
	            return {
	              results: result,
	              pagination: {
	                more: (params.page * 30) < data.total_count
	              }
	            };
	          },
	          cache: true
	        },
	        escapeMarkup: function (markup) { return markup; },
	        minimumInputLength: 1,
	        templateResult: formatRepo,
	        templateSelection: formatRepoSelection
	}
}
function formatRepo (repo) {
	 if (repo.loading) return repo.text;
	 if(repo.id&&repo.name){
		 var markup = "<option value="+repo.id+">"+repo.code+"&nbsp;&nbsp;"+repo.name+"</option>";
		 return markup;
	 }
 }
function formatRepoSelection (repo) {
	if(repo.selected) return repo.text;
   return repo.name;
 }
function pAjax(url,type,pId){
	return {
		  allowClear: true,
		  placeholder: "请选择",
  	      tokenSeparators: [','],
	      ajax: {
	          url: url,
	          dataType: 'json',
	          delay: 250,
	          data: function (params) {
	            return {
	              q: params.term, // search term
	              page: params.page,
	              type:type,
	              pId:pId
	            };
	          },
	          processResults: function (data, params) {
	            params.page = params.page || 1;
	            var result=[];
	            if(data){
	            	if(type==1){
	            		for(var i=0;i<data.length;i++){
		            		result.push({id:data[i].code,code:data[i].code,name:data[i].name});
		            	}
	            	}else{
	            		for(var i=0;i<data.length;i++){
		            		result.push({id:data[i].name,code:data[i].code,name:data[i].name});
		            	}
	            	}
	            
	            }
	            return {
	              results: result,
	              pagination: {
	                more: (params.page * 30) < data.total_count
	              }
	            };
	          },
	          cache: true
	        },
	        escapeMarkup: function (markup) { return markup; },
	        minimumInputLength: 1,
	        templateResult: pFormatRepo,
	        templateSelection: pFormatRepoSelection
	}
}
function pFormatRepo (repo) {
	 if (repo.loading) return repo.text;
	 if(repo.id){
		 var markup = "<option value="+repo.id+">"+repo.code+"&nbsp;&nbsp"+repo.name+"</option>";
		 return markup;
	 }
 }
function pFormatRepoSelection (repo) {
	if(repo.selected) return repo.text;
   return repo.id;
 }
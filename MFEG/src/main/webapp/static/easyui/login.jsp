<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE>
<html>
<head>
	<meta charset="UTF-8">
	<title>Basic PropertyGrid - jQuery EasyUI Demo</title>
	<link href="${pageContext.request.contextPath }/static/easyui/themes/default/easyui.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath }/static/easyui/themes/icon.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath }/static/easyui/demo.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/jquery/jquery-1.12.4.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/easyui/jquery.easyui.min.js"></script>
</head>
<body>
	<h2>Basic PropertyGrid</h2>
	<p>Click on row to change each property value.</p>
	<div style="margin:20px 0;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="showGroup()">ShowGroup</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="hideGroup()">HideGroup</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="showHeader()">ShowHeader</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="hideHeader()">HideHeader</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="getChanges()">GetChanges</a>
	</div>
	<table id="pg" class="easyui-propertygrid"   data-options="
				url:'<c:url value="/static/easyui/propertygrid_data1.json"></c:url>',
				method:'get',
				showGroup:true,
				scrollbarSize:0,
				columns: mycolumns
			">
	</table>
	<script type="text/javascript">
	var mycolumns = [[
	          		{field:'name',title:'学校名',width:100,sortable:true},
	         		{field:'value',title:'共有项目',width:100,resizable:false},
	         		{field:'value1',title:'进行中项目',width:100,resizable:false},
	         		{field:'value2',title:'操作',width:100,resizable:false}
	              ]];
		function showGroup(){
			$('#pg').propertygrid({
				showGroup:true
			});
		}
		function hideGroup(){
			$('#pg').propertygrid({
				showGroup:false
			});
		}
		function showHeader(){
			$('#pg').propertygrid({
				showHeader:true
			});
		}
		function hideHeader(){
			$('#pg').propertygrid({
				showHeader:false
			});
		}
		function getChanges(){
			var s = '';
			var rows = $('#pg').propertygrid('getChanges');
			for(var i=0; i<rows.length; i++){
				s += rows[i].name + ':' + rows[i].value + ',';
			}
			alert(s)
		}
	</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript" src='<c:url value="/static/jquery/jquery-1.12.4.js"></c:url>'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/page.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/login/js/main.js"></script>
	<link href="${pageContext.request.contextPath }/static/css/tableSort.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/datatables/css/jquery.dataTables.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src='<c:url value="/static/bootstrap/js/bootstrap.js"></c:url>'></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/zzsc.css'/>">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/metismenu.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/demo.css">
	<script src="${pageContext.request.contextPath }/static/js/metismenu.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/fileinput/css/fileinput.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/locales/zh.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/style.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/list.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/shell.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/plugins/bootstrap/js/bootbox.min.js"></script>
	<script src="${pageContext.request.contextPath }/static/js/student/commonFunc.js"></script>
	<link rel="stylesheet" href='<c:url value="/static/bootstrap/css/bootstrap.css"></c:url>' />
</head>
<body>
<div>
      <div class="panel-default" style="margin-bottom:0; overflow-x:auto; border: 0px;">
            <div class="panel-heading">
                <div style="display: inline-block;"><h3 class="panel-title" style="font-weight: normal;">个人信息</h3></div>
            </div>
	        <div class="panel-body" style=" ">
	         <form action="" class="form-horizontal col-sm-10 col-sm-push-1" method="post">
				   <table class="table table-bordered">
				   <thead>
				   </thead>
				   <tbody>
				     <tr>
				       <td>姓名</td>
				       <td>${expertUser.name} </td>
				       <td>性别</td>
				       <td>${expertUser.sex} </td>
				       <td>职务</td>
				       <td>${expertUser.position} </td>
				       <td>职称</td>
				       <td>${expertUser.jobTitleName} </td>
				       <td>学历</td>
				       <td>${expertUser.educationName} </td>
				     </tr>
				     <tr>
				       <td>来源</td>
				       <td colspan="9">${expertUser.originName} </td>
				     </tr>
				     <tr>
				       <td>工作单位</td>
				       <td colspan="4">${expertUser.workUnit} </td>
				       <td>所在城市</td>
				       <td colspan="4">${expertUser.rigionName} </td>
				     </tr>
				     <tr>
				       <td>所属行业</td>
				       <td colspan="4">${expertUser.industryName} </td>
				       <td>研究领域</td>
				       <td colspan="4">${expertUser.researchArea} </td>
				     </tr>
				     <tr>
				       <td>办公电话</td>
				       <td colspan="4">${expertUser.telephone} </td>
				       <td>电子邮箱</td>
				       <td colspan="4">${expertUser.email} </td>
				     </tr>
				     <tr>
				       <td>手机号码</td>
				       <td colspan="4">${expertUser.mobile} </td>
				       <td>身份证号</td>
				       <td colspan="4">${expertUser.idCard} </td>
				     </tr>
				     <tr>
				       <td>申报类型</td>
				       <td colspan="9">${expertUser.declareTypeName} </td>
				     </tr>
				     <tr>
				        <td>个人介绍</td>
				       <td colspan="9"> <textarea id="descriptionId" style="border:0px; resize : none;" class="form-control">${expertUser.description}</textarea> </td>
				     </tr>
				     <tr>
				       <td>工作业绩</td>
				       <td colspan="9">
					       <c:forEach var="p" items="${projects}">
					       		${p.projectname}:${p.appraise}<br>
					       </c:forEach>
				       </td>
				     </tr>
				   </tbody>
				 </table>
				 <div class="form-group" style="text-align: center;">
					<button type="button" class="btn btn-primary " onclick="javascript:alterDescription()">保存</button>
					<button type="button" class="btn btn-default " data-dismiss="modal">撤销</button>
				 </div>
			</form>
		 </div>
     </div>
</div>
</body>
<script type="text/javascript">
function alterDescription(){
	var description = $("#descriptionId").val();
	var old = "${expertUser.description}";
	if(description!=old){
		$.ajax({
			url : "<c:url value='/accountManage/alterDescription.c'></c:url>",
			type : "post",
			data : {
				description : description 
			},
			success : function(data) {
				 if (data==1) {
					alert("更新成功！")
				}else if (data==0) {
					alert("更新失败！");
					$('#myFrame',parent.document).prop("src",'<c:url value="/expert/personalInfo.html"></c:url>')  
				}
			},
			error : function(data) {
				alert("更新失败！");
				$('#myFrame',parent.document).prop("src",'<c:url value="/expert/personalInfo.html"></c:url>')  
			}
		});
	}else {
		alert("内容无变更,无需保存！");
	}
	
}
</script>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript" src='<c:url value="/static/jquery/jquery-1.12.4.js"></c:url>'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/page.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/login/js/main.js"></script>
	<link href="${pageContext.request.contextPath }/static/css/tableSort.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/datatables/css/jquery.dataTables.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src='<c:url value="/static/bootstrap/js/bootstrap.js"></c:url>'></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/zzsc.css'/>">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/metismenu.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/demo.css">
	<script src="${pageContext.request.contextPath }/static/js/metismenu.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/fileinput/css/fileinput.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/locales/zh.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/style.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/list.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/shell.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/plugins/bootstrap/js/bootbox.min.js"></script>
	<script src="${pageContext.request.contextPath }/static/js/student/commonFunc.js"></script>
	<link rel="stylesheet" href='<c:url value="/static/bootstrap/css/bootstrap.css"></c:url>' />
</head>
<body>
<div>
      <div class="panel-default" style="margin-bottom:0;overflow-x:auto;">
            <div class="panel-heading" style="height: 45px;">
                <div style="display: inline-block;padding-left: 0px;font-size: 16px;"> 密码管理 </div>
                <div style="display: inline-block;float: right;" class="btn-group">
	                <button type="button" class="btn btn-default" onclick="pwdManage()">修改密码</button>
	                <button type="button" class="btn btn-default" onclick="forgetPwd()()">忘记密码</button>
	                <button type="button" class="btn btn-default" onclick="altertPhoneNum()()">换绑手机</button>
				</div> 
            </div>
	        <div class="panel-body" style="padding-bottom:50px;">
	           <form action="<c:url value='/accountManage/pwdManage/alterPwd.c'></c:url>" style="display: block;" id="altertPwdId" name="pwdForm" class="form-horizontal col-sm-6 col-sm-push-3" method="post">
						<div class="form-group">
							<label class="col-sm-3 control-label">原密码：</label>
							<div class="col-sm-6">
								<input type="password" class="form-control" placeholder="密码" name="oldPwd" id="oldPwd"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">新密码：</label>
							<div class="col-sm-6">
								<input type="password" class="form-control" placeholder="密码" name="newPwd" id="newPwd"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">确认密码：</label>
							<div class="col-sm-6">
								<input type="password" class="form-control" placeholder="确认密码" name="confirmPwd" id="confirmPwd"/>
							</div>
						</div>
						<div class="form-group" style="text-align: center;">
							<button type="button" class="btn btn-primary " id="alterPwdBtn">提交</button>
							<button type="button" class="btn btn-default " data-dismiss="modal">取消</button>
						</div>
					</form>
	           <form style="display: none;" id="forgetPwdId" name="pwdForm" class="form-horizontal col-sm-6 col-sm-push-3" method="post">
						<div class="form-group">
							<label class="col-sm-3 control-label">新密码：</label>
							<div class="col-sm-6">
								<input type="password" class="form-control" placeholder="新密码" name="pwd"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">确认密码：</label>
							<div class="col-sm-6">
								<input type="password" class="form-control" placeholder="确认密码" name="pwd2"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">手机号：</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="手机号" name="pwd"/>
							</div>
							<button class="btn" type="button" style="font-size: 8px;">获取验证码</button> 
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">验证码：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="验证码" name="pwd"/>
							</div>
						</div>
						<div class="form-group" style="text-align: center;">
							<button type="button" class="btn btn-primary ">提交</button>
							<button type="button" class="btn btn-default " data-dismiss="modal">取消</button>
						</div>
			   </form>
	           <form style="display: none;" id="altertPhoneNum" name="pwdForm" class="form-horizontal col-sm-6 col-sm-push-3" method="post">
						<div class="form-group">
							<label class="col-sm-3 control-label">已绑定手机号：</label>
							<div class="col-sm-6">
								<label class="control-label col-sm-6">159*****125</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">原手机号：</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="手机号" name="pwd"/>
							</div>
							<button class="btn" type="button" style="font-size: 8px;">获取验证码</button> 
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">验证码：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="验证码" name="pwd"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">新手机号：</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="手机号" name="pwd"/>
							</div>
							<button class="btn" type="button" style="font-size: 8px;">获取验证码</button> 
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">验证码：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="验证码" name="pwd"/>
							</div>
						</div>
						<div class="form-group" style="text-align: center;">
							<button type="button" class="btn btn-primary ">提交</button>
							<button type="button" class="btn btn-default " data-dismiss="modal">取消</button>
						</div>
				</form>
		    </div>
     </div>
</div>
</body>
<script>
	$(function(){
		$("#alterPwdBtn").click(function(){
			var oldPwd = $("#oldPwd").val();
			var newPwd = $("#newPwd").val();
			var confirmPwd = $("#confirmPwd").val();
			if(newPwd!=null&&newPwd!="undefined"&&newPwd!=""){
				if (newPwd==confirmPwd) {
					$.ajax({
						url : "<c:url value='/accountManage/pwdManage/alterPwd.c'></c:url>",
						type : "post",
						data : {
							oldPwd : oldPwd,
							newPwd : newPwd
						},
						success : function(data) {
							if (data==1) {
								alert("修改成功！");
							}else if (data==0) {
								alert("修改失败！");
							}else if(data==2){
								alert("密码不正确！");
							}
						},
						error : function(data) {
							alert("修改失败！");
						}
					});
				}else{
					alert("两次密码不相同！")
				}
			}else{
				alert("新密码不能为空！");
				return;
			}
		});
	})
	
	function pwdManage(){
		$("form[name='pwdForm']").css("display","none");
		$("#altertPwdId").css("display","block");
	}
	function forgetPwd(){
		$("form[name='pwdForm']").css("display","none");
		$("#forgetPwdId").css("display","block");
	}
	function altertPhoneNum(){
		$("form[name='pwdForm']").css("display","none");
		$("#altertPhoneNum").css("display","block");
	}
</script>
</html>
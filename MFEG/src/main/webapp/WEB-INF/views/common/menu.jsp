<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript" src='<c:url value="/static/jquery/jquery-1.12.4.js"></c:url>'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/page.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/login/js/main.js"></script>
	<link href="${pageContext.request.contextPath }/static/css/tableSort.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/datatables/css/jquery.dataTables.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src='<c:url value="/static/bootstrap/js/bootstrap.js"></c:url>'></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/zzsc.css'/>">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/metismenu.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/demo.css">
	<script src="${pageContext.request.contextPath }/static/js/metismenu.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/fileinput/css/fileinput.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/locales/zh.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/style.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/list.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/shell.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/plugins/bootstrap/js/bootbox.min.js"></script>
	<script src="${pageContext.request.contextPath }/static/js/student/commonFunc.js"></script>
	<link rel="stylesheet" href='<c:url value="/static/bootstrap/css/bootstrap.css"></c:url>' />
</head>
<body>
<div class="left-menu">
	<div class="navbar navbar-default" role="navigation">
      <nav class="sidebar-nav">
          <ul id="menu" style="background-color: #F5F5F5">
          <!--教育厅用户  -->
             <c:if test='${param["userRole"]==1}'>
              <li>
                  <a href="#">用户管理 <span class="fa arrow" style="font-weight: bold;"></span>
                  </a>
                  <ul>
                      <li>
                          <a href="javascript:officeUserManage()">
                            	 处室用户管理
                          </a>
                      </li>
                      <li>
                          <a href="javascript:universityUserManage()">
                             	高校用户管理
                          </a>
                      </li>
                      <li>
                          <a href="javascript:expertUserManage()">
                            	  专家用户管理
                          </a>
                      </li>
                  </ul>
              </li>
              <li>
                  <a href="javascript:projectManage()">项目管理</a>
              </li>
              <li>
                  <a href="#">数据查询 <span class="fa arrow"  style="font-weight: bold;"></span></a>
                  <ul>
                      <li>
                          <a href="#">报表查询<span class="fa arrow"  style="font-weight: bold;"></span></a>
                          <ul>
                              <li><a href="javascript:uReportFormsData()">处室/高校用户</a></li>
                              <li><a href="javascript:eReportFormsData()">专家</a></li>
                          </ul>
                      </li>
                      <li><a href="javascript:projectData()">项目数据</a></li>
                      <li><a href="javascript:expertData()">专家数据</a></li>
                  </ul>
              </li>
 			 </c:if>
			 <!--处室、高校用户  -->
		     <c:if test='${param["userRole"]==2||param["userRole"]==4}'>
			      <li>
			          <a href="">账户设置 <span class="fa arrow" style="font-weight: bold;"></span></a>
			          <ul>
			              <li>
			              		<!-- 同专家用户 -->
			                  <a href="javascript:pwdManage()">
			                    	密码管理
			                  </a>
			              </li>
			              <li>
			                  <a href="javascript:universityPersonalInfo()">
			                     	个人信息
			                  </a>
			              </li>
			          </ul>
			      </li>
			      <li>
			          <a href="javascript:universityProjectManage()">项目管理</a>
			      </li>
			 </c:if>	 	 
			 <!--专家用户  -->
		     <c:if test='${param["userRole"]==3}'>
			      <li>
			          <a href="">账户设置 <span class="fa arrow" style="font-weight: bold;"></span>
			          </a>
			          <ul>
			              <li>
			                  <a href="javascript:pwdManage()">
			                    	密码管理
			                  </a>
			              </li>
			              <li>
			                  <a href="javascript:personalInfo()">
			                     	个人信息
			                  </a>
			              </li>
			          </ul>
			      </li>
			      <li>
			          <a href="javascript:projectPartakeInfo()">项目参与详情</a>
			      </li>
			</c:if>	 
		  </ul>
 	   </nav>
     </div>   
</div>   
</body>       
<script type="text/javascript">
$(function() {
    $('#menu').metisMenu({
    	 toggle: true ,
    });
});
</script>
</html>

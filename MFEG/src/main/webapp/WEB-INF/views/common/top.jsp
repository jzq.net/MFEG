<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!--网站公共头部-->
<div class="topbar" style="height: 50px;">
	<nav class="navbar navbar-default" role="navigation" style="background-color: #1995dc;  border-color: #1995dc;">
		<div class="container" >
			<div class="navbar-header">
				<a class="navbar-brand" href="javascript:homePageShow()" style="color:#ffffff">导师库</a>
			</div>
			<div class="collapse navbar-collapse" id="example-navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="#" class=""style="color:#ffffff">联系我们</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href='<c:url value="/loginOut.c"></c:url>' class="n1" style="font-size:13px;color:#ffffff">退出</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a class="n1" style="font-size:13px;color:#ffffff"><marquee scrollAmount=2 width=150>${sessionScope.userName}，欢迎您~</marquee></a></li>
				</ul>
			</div>
		</div>
	</nav>
</div>

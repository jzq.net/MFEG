<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<!DOCTYPE html>
<html>
<head>
	<title>
		<c:if test='${param["userRole"]==1}'>教育厅</c:if>
		<c:if test='${param["userRole"]==2}'>高校</c:if>
		<c:if test='${param["userRole"]==4}'>处室</c:if>
		<c:if test='${param["userRole"]==3}'>专家</c:if>
	</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript" src='<c:url value="/static/jquery/jquery-1.12.4.js"></c:url>'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/page.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/login/js/main.js"></script>
	<link href="${pageContext.request.contextPath }/static/css/tableSort.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/datatables/css/jquery.dataTables.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src='<c:url value="/static/bootstrap/js/bootstrap.js"></c:url>'></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/zzsc.css'/>">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/metismenu.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/demo.css">
	<script src="${pageContext.request.contextPath }/static/js/metismenu.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/fileinput/css/fileinput.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/locales/zh.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/style.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/list.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/shell.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/plugins/bootstrap/js/bootbox.min.js"></script>
	<script src="${pageContext.request.contextPath }/static/js/student/commonFunc.js"></script>
	<link rel="stylesheet" href='<c:url value="/static/bootstrap/css/bootstrap.css"></c:url>' />
</head>
<body>
	<!-- 引入头部 -->
	<%@ include file="/WEB-INF/views/common/top.jsp" %>
	<div class="hx-main clearfix">
		<!-- 左侧菜单 -->
		<%@ include file="/WEB-INF/views/common/menu.jsp" %>
		<!--内容部分-->
		<div class="content" id="contentDivId" style="display: block;">
			<div class="main newslist" style="min-height: 500px;">
	      		<div class="panel panel-default" style="margin-bottom:0;margin-right: 21px; overflow-x:auto;">
			        <iframe id="myFrame" style="margin: 0px;padding: 0px;border: 0px;overflow: auto;"  width="100%" src='<c:url value="/common/homePage.html"></c:url>'>
			        </iframe>
	     		</div>
			</div>
		</div>
	</div>
	<!-- 引入底部 -->
	<div style="text-align: center;">
		<%@ include file="/WEB-INF/views/common/footer.jsp" %>
	</div>
</body>
<script type="text/javascript">
    /*用户首次登录 */
	<c:if test='${param["loginType"]==2}'>
		pwdManage();
	</c:if>
	
	$('#myFrame').css("height",$(window).height()-160);
	$(window).resize(function(){  
    	$('#myFrame').css("height",$(window).height()-160);
    })  
    
    function personalInfo(){
		$.ajax({
			url : "<c:url value='/accountManage/personalInfo.c'></c:url>",
			type : "post",
			async: false
		});
		$('#myFrame').prop("src",'<c:url value="/expert/personalInfo.html"></c:url>')  
	}
	
	function pwdManage(){
		$("form[name='pwdForm']").css("display","none");
		$("#altertPwdId").css("display","block");
		$('#myFrame').prop("src",'<c:url value="/expert/pwdManage.html"></c:url>')  
	}
	
	function projectPartakeInfo(){
		$.ajax({
			url : "<c:url value='/project/projectPartakeInfo.c'></c:url>",
			type : "post",
			async: false
		});
		$('#myFrame').prop("src",'<c:url value="/expert/projectPartakeInfo.html"></c:url>') 
	} 
	function universityPersonalInfo(){
		$.ajax({
			url : "<c:url value='/accountManage/personalInfo.c'></c:url>",
			type : "post",
			async: false
		});
		$('#myFrame').prop("src",'<c:url value="/university/universityPersonalInfo.html"></c:url>') 
	}
	function universityProjectManage(){
		$.ajax({
			url : "<c:url value='/collegesUser/projectManage.c'></c:url>",
			type : "post",
			async: false
		});
		$('#myFrame').prop("src",'<c:url value="/university/universityProjectManage.html"></c:url>') 
	}
	function officeUserManage(){
		$.ajax({
			url : "<c:url value='/educationhallUser/userManage.c'></c:url>",
			type : "post",
			data:{
				userRole:'4'
			},
			async: false
		});
		$('#myFrame').prop("src",'<c:url value="/educationHall/officeUserManage.html"></c:url>') 
	}
	function universityUserManage(){
		$.ajax({
			url : "<c:url value='/educationhallUser/userManage.c'></c:url>",
			type : "post",
			data:{
				userRole:'2'
			},
			async: false
		});
		$('#myFrame').prop("src",'<c:url value="/educationHall/universityUserManage.html"></c:url>') 
	}
	function expertUserManage(){
		$.ajax({
			url : "<c:url value='/educationhallUser/userManage.c'></c:url>",
			type : "post",
			data:{
				userRole:'3'
			},
			async: false
		});
		$('#myFrame').prop("src",'<c:url value="/educationHall/expertUserManage.html"></c:url>') 
	}
	function projectManage(){
		//项目管理
		$.ajax({
			url : "<c:url value='/educationhallUser/projectManage.c'></c:url>",
			type : "post",
			async: false
		});
		$('#myFrame').prop("src",'<c:url value="/educationHall/projectManage.html"></c:url>') 
	}
	function uReportFormsData(){
		$.ajax({
			url : "<c:url value='/educationhallUser/selectAllDCProjectInfo.c'></c:url>",
			type : "post",
			async: false
		});
		$('#myFrame').prop("src",'<c:url value="/educationHall/uReportFormsData.html"></c:url>') 
	}
	function eReportFormsData(){
		$.ajax({
			url : "<c:url value='/educationhallUser/selectAllExpertProjectInfo.c'></c:url>",
			type : "post",
			async: false
		});
		$('#myFrame').prop("src",'<c:url value="/educationHall/eReportFormsData.html"></c:url>') 
	}
	function projectData(){
		$.ajax({
			url : "<c:url value='/educationhallUser/selectAllProjectInfo.c'></c:url>",
			type : "post",
			async: false
		});
		$('#myFrame').prop("src",'<c:url value="/educationHall/projectData.html"></c:url>') 
	}
	function expertData(){
		$.ajax({
			url : "<c:url value='/educationhallUser/selectAllExpertData.c'></c:url>",
			type : "post",
			async: false
		});
		$('#myFrame').prop("src",'<c:url value="/educationHall/expertData.html"></c:url>') 
	}
	
	
	
	
</script>
</html>
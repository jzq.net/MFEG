<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript" src='<c:url value="/static/jquery/jquery-1.12.4.js"></c:url>'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/page.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/login/js/main.js"></script>
	<link href="${pageContext.request.contextPath }/static/css/tableSort.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/datatables/css/jquery.dataTables.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src='<c:url value="/static/bootstrap/js/bootstrap.js"></c:url>'></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/zzsc.css'/>">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/metismenu.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/demo.css">
	<script src="${pageContext.request.contextPath }/static/js/metismenu.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/fileinput/css/fileinput.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/locales/zh.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/style.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/list.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/shell.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/plugins/bootstrap/js/bootbox.min.js"></script>
	<script src="${pageContext.request.contextPath }/static/js/student/commonFunc.js"></script>
	<link rel="stylesheet" href='<c:url value="/static/bootstrap/css/bootstrap.css"></c:url>' />
</head>
<body>
<div >
      <div class=" panel-default" style="margin-bottom:0; overflow-x:auto;">
            <div class="panel-heading" style="height: 45px;">
                <div style="display: inline-block;padding-left: 0px;font-size: 16px;"><a href="javascript:universityManageExpert()"><span class="glyphicon glyphicon-share-alt" style="color: rgb(255, 140, 60);">返回</span></a></div>
                 <div style="display: inline-block;">
				  ( 预计需要XXX人，当前已有XX人可确认参加)
				</div> 
                <div style="display: inline-block; float: right;"class="btn-group">
	                <button type="button" class="btn btn-default" onclick="universityManageExpert()">去增加专家</button>
	                <button type="button" class="btn btn-default" id="deleteExpertBtnId">删除专家</button>
	                <button type="button" class="btn btn-default" onclick="sendMessage()">发送确认短信</button>
				</div> 
            </div>
	        <div class="panel-body" style="padding-bottom:50px;">
	            <form role="form" action="" method="post">
	                <%@ include file="/WEB-INF/views/university/lookAddedExpert_table.jsp" %>
	      	    </form>
		    </div>
     </div>
</div>
<div class="modal" id="sendMessage_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">短信内容</h4>
			</div>
			<div class="modal-body">
					<form name="f1" class="form-horizontal" method="post">
						<div class="form-group" style="text-align: center;">
						 				<br>
							<label>确定参加请回复数字1</label><br>
							<label>拒绝参加请回复数字2</label>
						</div>
					</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary">确定</button>
			</div>
		</div> 
	</div> 
</div> 
</body>
<script type="text/javascript">
	function sendMessage(){
		$("#sendMessage_modal").modal("toggle");
	}
	function universityManageExpert(){
		var projectId = "${param.projectId}";
		var projectName = "${param.projectName}";
		$.ajax({
			url : "<c:url value='/collegesUser/checkNotPartakeProjectExpert.c'></c:url>",
			type : "post",
			data:{
				projectId:projectId
			},
			async: false
		});
		$('#myFrame', parent.document).prop("src","<c:url value='/university/manageExpert.html?projectId="+projectId+"&projectName="+projectName+"'></c:url>") ;
	}
	
	$(function(){
		$("#deleteExpertBtnId").click(function(){
			var docEle = $(".checkchild:checked");
			if (docEle.size()==0) {
				alert("请选择您要删除的专家！");
				return;
			}else{
				var peIds = new Array();
				for(var i=0;i<docEle.size();i++){
					peIds.push(docEle[i].value);
				}
				var peId = peIds.join("-");
				$.ajax({
					url : "<c:url value='/collegesUser/deleteProjectExpert.c'></c:url>",
					type : "post",
					data : {
						peId:peId
					},
					success : function(data) {
						if (data==1) {
							alert("删除成功！");
							refurbish('${param.projectId}','${param.projectName}');
						}else {
							alert("删除失败！");
						}
					},
					error : function(data) {
						alert("删除失败！");
					}
				});
			}
		})
		var refurbish = function(projectId,projectName){
			$.ajax({
				url : "<c:url value='/collegesUser/checkAddedExpert.c'></c:url>",
				type : "post",
				data:{
					projectId:projectId
				},
				async: false
			});
			$('#myFrame', parent.document).prop("src","<c:url value='/university/lookAddedExpert.html?projectId="+projectId+"&projectName="+projectName+"'></c:url>") ;
		} 
		
		
		
	})
</script>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<table id="uemTable" class="display cell-border" style="width: 100%;">
		<thead>
			<tr>
				<td style="width: 10px;">
					 <input type="checkbox" id="uemCheckboxParent" class="n"/>
				</td>
				<td>
					序列 
				</td>
				<td>
					项目日期
				</td>
				<td>
					项目名称
				</td>
				<td>
					项目状态
				</td>
				<td>
					需要专家数量
				</td>
				<td>
					已确认专家数量
				</td>
				<td>
					未回复专家数量
				</td>
				<td>
					操作
				</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="p" items="${projects}">
				<tr>
					<td>
						<input type="checkbox"  class="checkchild" value="${p.id}"/>
						<input type="hidden" name="startTime" value="${p.starttime}"/>
						<input type="hidden" name="endTime" value="${p.endtime}"/>
						<input type="hidden" name="hostunit" value="${p.hostunit}"/>
						<input type="hidden" name="phonenum" value="${p.phonenum}"/>
						<input type="hidden" name="website" value="${p.website}"/>
						<input type="hidden" name="disabledTime" value="${p.disabledTime}"/>
					</td>
					<td></td>
					<td>
						${p.starttime}~${p.endtime}
					</td>
					<td name="projectname">
						${p.projectname}
					</td>
					<td name="status">
						${p.status}
					</td>
					<td name="experttotal">
						${p.experttotal}
					</td>
					<td>
						 XXX
					</td>
					<td>
						 XXX
					</td>
					<td>
						<c:if test="${p.status!='已结束'}">
							<a href="javascript:universityManageExpert('${p.id}','${p.projectname}')">管理专家</a>
						</c:if>
						<c:if test="${p.status=='已结束'}">
							<a href='javascript:universityLookExpert("${p.id}")'>查看专家</a>|<a href='javascript:assessExpert("${p.id}")'>评价专家</a>|<a href="javascript:universityManageExpert('${p.id}','${p.projectname}')">管理专家</a>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
<script type="text/javascript">
	$(function() {
		//设置数据源
		$("#uemTable").DataTable({
			"rowCallback": function( row, data, index ) {
			  },
			"aaSorting" : [[0, "asc"]], //默认的排序方式，第1列，升序排列  
			columns: [{
				"data": "null",
				"bSortable":false,
				'sClass': "text-center" 
			},{
				"data": "index",
				'sClass': "text-center",
				width : "30px",  
				render : function(data, type, row, meta) {  
                    // 显示行号  
                    var startIndex = meta.settings._iDisplayStart;  
                    return startIndex + meta.row + 1;  
               	}  
			},{
				"data": "projectDate"
			}, {
				"data": "projectName"
			}, {
				"data": "projectStatus"
			}, {
				"data": "expertTotal",
				'sClass': "text-center",
			}, {
				"data": "confirmedExpertTotal"
			}, {
				"data": "notConfirmedExpertTotal"
			}, {
				"data": "operate",
				'sClass': "text-center"
			}],
			 "paging": true,  
			 "sPaginationType": "full_numbers",//用于指定分页器风格
			 "oLanguage": {    // 语言设置  
		            "sLengthMenu": "每页显示 _MENU_ 条记录",  
		            "sZeroRecords": "抱歉， 没有找到",  
		            "sInfo": "从 _START_ 到 _END_  共 _TOTAL_条数据",  
		            "sInfoEmpty": "没有数据",  
		            "sInfoFiltered": "(从 _MAX_ 条数据中检索)",  
		            "sZeroRecords": "没有检索到数据",  
		            "sSearch": "搜索:",  
		            "oPaginate": {  
		              "sFirst": "首页",  
		              "sPrevious": "上一页",  
		              "sNext": "下一页",  
		              "sLast": "尾页"  
		            }  
		          },  
		});
	});
</script> 
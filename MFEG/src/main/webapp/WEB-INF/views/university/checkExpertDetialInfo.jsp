<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 模态对话框 -->
<div class="modal" id="detailedInfo_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">详细信息</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
				   <table class="table table-bordered">
				   <thead>
				   </thead>
				   <tbody id="uedi">
				     <tr>
				       <td>姓名</td>
				       <td name="name"> </td>
				       <td>性别</td>
				       <td  name="sex"> </td>
				       <td>职务</td>
				       <td  name="position"> </td>
				       <td>职称</td>
				       <td  name="jobTitleName"> </td>
				       <td>学历</td>
				       <td  name="educationName"> </td>
				     </tr>
				     <tr>
				       <td>专家来源</td>
				       <td colspan="9"  name="originName"> </td>
				     </tr>
				     <tr>
				       <td>工作单位</td>
				       <td colspan="4"  name="workUnit"> </td>
				       <td>所在城市</td>
				       <td colspan="4"  name="rigionName"> </td>
				     </tr>
				     <tr>
				       <td>所属行业</td>
				       <td colspan="4"  name="industryName"> </td>
				       <td>研究领域</td>
				       <td colspan="4"  name="researchArea"> </td>
				     </tr>
				     <tr>
				       <td>申报类型</td>
				       <td colspan="9"  name="declareTypeName"> </td>
				     </tr>
				     <tr>
				        <td>个人介绍</td>
				       <td colspan="9"> <textarea id="descriptionId" style="border:0px; resize : none;" class="form-control"   name="description"> </textarea> </td>
				     </tr>
				     <tr>
				       <td>工作业绩</td>
				       <td colspan="9"  name="achievement"> </td>
				     </tr>
				   </tbody>
				 </table>
			</form>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
		  </div>
		</div> 
	</div> 
</div> 
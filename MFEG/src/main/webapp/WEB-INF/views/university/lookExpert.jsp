<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript" src='<c:url value="/static/jquery/jquery-1.12.4.js"></c:url>'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/page.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/login/js/main.js"></script>
	<link href="${pageContext.request.contextPath }/static/css/tableSort.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/datatables/css/jquery.dataTables.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src='<c:url value="/static/bootstrap/js/bootstrap.js"></c:url>'></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/zzsc.css'/>">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/metismenu.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/demo.css">
	<script src="${pageContext.request.contextPath }/static/js/metismenu.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/fileinput/css/fileinput.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/locales/zh.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/style.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/list.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/shell.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/plugins/bootstrap/js/bootbox.min.js"></script>
	<script src="${pageContext.request.contextPath }/static/js/student/commonFunc.js"></script>
	<link rel="stylesheet" href='<c:url value="/static/bootstrap/css/bootstrap.css"></c:url>' />
</head>
<body>
<div >
      <div class=" panel-default" style="margin-bottom:0; overflow-x:auto;">
            <div class="panel-heading" style="height: 45px;">
                <div style="display: inline-block;padding-left: 0px;font-size: 16px;"><a href="javascript:universityProjectManage()"><span class="glyphicon glyphicon-share-alt" style="color: rgb(255, 140, 60);">返回</span></a></div>
                <div style="display: inline-block; ">
				  (项目名称:${expertUsers[0].projectName})
				</div> 
                <div style="display: inline-block;float: right;">
                   <button type="button" class="btn btn-default" onclick="exportExpertInfo2Excel()">导出</button>
                   <form method="post" id="eeieid" style="display: none;" action="<c:url value='/exportExpert.c'></c:url>">
                   	<input name="projectId" value="${expertUsers[0].projectId}">
                   </form>
				</div> 
            </div>
	        <div class="panel-body" style="padding-bottom:50px;">
	            <form role="form" action="" method="post">
	                <%@ include file="/WEB-INF/views/university/eInfo_table.jsp" %>
	      	    </form>
		    </div>
     </div>
</div>
<%@ include file="/WEB-INF/views/university/checkExpertDetialInfo.jsp" %>
</body>
<script type="text/javascript">
	function checkdetailedInfo(idCard,obj){
		var tr = $(obj).parent().parent();
		$("#uedi td[name='name']").text(tr.find("td[name='name']").text());
		$("#uedi td[name='sex']").text(tr.find("td[name='sex']").text());
		$("#uedi td[name='position']").text(tr.find("td[name='position']").text());
		$("#uedi td[name='jobTitleName']").text(tr.find("input[name='jobTitleName']").val());
		$("#uedi td[name='educationName']").text(tr.find("input[name='educationName']").val());
		$("#uedi td[name='originName']").text(tr.find("td[name='originName']").text());
		$("#uedi td[name='workUnit']").text(tr.find("input[name='workUnit']").val());
		$("#uedi td[name='rigionName']").text(tr.find("input[name='rigionName']").val());
		$("#uedi td[name='industryName']").text(tr.find("td[name='industryName']").text());
		$("#uedi td[name='researchArea']").text(tr.find("td[name='researchArea']").text());
		$("#uedi td[name='declareTypeName']").text(tr.find("td[name='declareTypeName']").text());
		$("#uedi textarea[name='description']").text(tr.find("input[name='description']").val());
		$("#uedi td[name='achievement']").html(tr.find("input[name='achievement']").val());
		$("#detailedInfo_modal").modal("toggle");
	}
	function universityProjectManage(){
		$.ajax({
			url : "<c:url value='/collegesUser/projectManage.c'></c:url>",
			type : "post",
			async: false
		});
		$('#myFrame', parent.document).prop("src",'<c:url value="/university/universityProjectManage.html"></c:url>') ;
	}
	
	function exportExpertInfo2Excel(){
		$("#eeieid").submit();
	}
</script>
</html>
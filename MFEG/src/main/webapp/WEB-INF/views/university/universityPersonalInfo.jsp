<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript" src='<c:url value="/static/jquery/jquery-1.12.4.js"></c:url>'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/page.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/login/js/main.js"></script>
	<link href="${pageContext.request.contextPath }/static/css/tableSort.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/datatables/css/jquery.dataTables.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src='<c:url value="/static/bootstrap/js/bootstrap.js"></c:url>'></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/zzsc.css'/>">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/metismenu.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/demo.css">
	<script src="${pageContext.request.contextPath }/static/js/metismenu.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/fileinput/css/fileinput.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/locales/zh.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/style.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/list.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/shell.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/plugins/bootstrap/js/bootbox.min.js"></script>
	<script src="${pageContext.request.contextPath }/static/js/student/commonFunc.js"></script>
	<link rel="stylesheet" href='<c:url value="/static/bootstrap/css/bootstrap.css"></c:url>' />
</head>
<body>
<div>
      <div class="panel-default" style="margin-bottom:0; overflow-x:auto;">
            <div class="panel-heading">
                <div style="display: inline-block;"><h3 class="panel-title" style="font-weight: normal;">个人信息</h3></div>
            </div>
	        <div class="panel-body" style="padding-bottom:50px;">
	         <form action="" class="form-horizontal col-sm-6 col-sm-push-3" method="post">
	         <!--处室用户  -->
    				<c:if test='${userRole==4}'>
				   <table class="table table-bordered" style="text-align: center;">
				   <thead>
				   </thead>
				   <tbody>
				     <tr>
				       <td class="col-sm-4">处室名称</td>
				       <td>${departmentUser.userName} </td>
				     </tr>
				     <tr>
				       <td>联系人姓名</td>
				       <td>${departmentUser.linkman} </td>
				     </tr>
				     <tr>
				       <td>手机号</td>
				       <td>${departmentUser.phoneNum} </td>
				     </tr>
				     <tr>
				       <td>邮箱</td>
				       <td>${departmentUser.email} </td>
				     </tr>
				   </tbody>
				 </table>
			   </c:if>	
	         <!--高校用户  -->
    				<c:if test='${userRole==2}'>
				   <table class="table table-bordered" style="text-align: center;">
				   <thead>
				   </thead>
				   <tbody>
				     <tr>
				       <td class="col-sm-4">高校代码</td>
				       <td>${collegesUser.code} </td>
				     </tr>
				     <tr>
				       <td>高校名称</td>
				       <td>${collegesUser.name} </td>
				     </tr>
				     <tr>
				       <td>联系人姓名</td>
				       <td>${collegesUser.linkman} </td>
				     </tr>
				     <tr>
				       <td>手机号</td>
				       <td>${collegesUser.phoneNum} </td>
				     </tr>
				     <tr>
				       <td>邮箱</td>
				       <td>${collegesUser.email} </td>
				     </tr>
				   </tbody>
				 </table>
			   </c:if>	
			</form>
		 </div>
     </div>
</div>
</body> 
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript" src='<c:url value="/static/jquery/jquery-1.12.4.js"></c:url>'></script>
	<script type="text/javascript" src='<c:url value="/static/js/ajaxfileupload.js"></c:url>'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/page.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/login/js/main.js"></script>
	<link href="${pageContext.request.contextPath }/static/css/tableSort.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/datatables/css/jquery.dataTables.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src='<c:url value="/static/bootstrap/js/bootstrap.js"></c:url>'></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/zzsc.css'/>">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/metismenu.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/demo.css">
	<script src="${pageContext.request.contextPath }/static/js/metismenu.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/fileinput/css/fileinput.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/locales/zh.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/style.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/list.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/shell.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/plugins/bootstrap/js/bootbox.min.js"></script>
	<script src="${pageContext.request.contextPath }/static/js/student/commonFunc.js"></script>
	<link rel="stylesheet" href='<c:url value="/static/bootstrap/css/bootstrap.css"></c:url>' />
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/datetimepicker/css/bootstrap-datetimepicker.min.css'/>">
	<script type="text/javascript" src="<c:url value='/static/datetimepicker/js/bootstrap-datetimepicker.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/static/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js'/>"></script>
</head>
<body>
<div >
      <div class="panel panel-default" style="margin-bottom:0; overflow-x:auto;">
            <div class="panel-heading" style="height: 45px;">
                <div style="display: inline-block;padding-left: 0px;font-size: 16px;"> 项目管理 </div>
                <div style="display: inline-block; ">
				  (当前共${projectTotal}项项目，正在进行${underwayProjectTotal}项，已结束${finishProjectTotal}项)
				</div> 
                <div style="display: inline-block; float: right;"class="btn-group">
	                <button type="button" class="btn btn-default" onclick="universityMakeProject()">创建项目</button>
	                <button type="button" class="btn btn-default" onclick="universityAltertProject()">修改项目</button>
	                <button type="button" class="btn btn-default" id="deleteProjectBtn">删除</button>
				</div> 
            </div>
	        <div class="panel-body" style="padding-bottom:50px;">
	            <form id="listForm" role="form" action="" method="post">
	                <%@ include file="/WEB-INF/views/university/upm_table.jsp" %>
	      	    </form>
		    </div>
     </div>
</div>
<div class="modal" id="u_makeProjectModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">创建项目</h4>
			</div>
			<div class="modal-body">
					<form name="f1" class="form-horizontal" method="post" id="createProjectForm" action='<c:url value=""></c:url>'>
						<div class="form-group">
							<label class="col-sm-5 control-label">项目名称：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="项目名称"  name="projectname"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">主办单位：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="主办单位" name="hostunit"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">联系电话：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="联系电话" name="phonenum"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">项目官网：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="项目官网" name="website"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">开始时间：</label>
							<div class="col-sm-6">
								<input type="text" id="dt" name="startTime" readonly class="form-control form_date dt">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">结束时间：</label>
							<div class="col-sm-6">
								<input type="text" id="dt" name="endTime" readonly class="form-control form_date dt">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">预计需要专家数量：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="预计需要专家数量" name="experttotal"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">多长时间(分钟)未回复短信视为不参加：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="时间(分钟)" name="disabledTime"/>
							</div>
						</div>
					</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary" id="createProjectFormBtn">确定</button>
			</div>
		</div> 
	</div> 
</div> 
<div class="modal" id="u_altertProjectModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">修改项目</h4>
			</div>
			<div class="modal-body">
					<form name="f1" class="form-horizontal" method="post" id="alterProjectForm">
						<input type="hidden" name="id"/>
						<div class="form-group">
							<label class="col-sm-5 control-label">项目名称：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="项目名称"  name="projectname"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">主办单位：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="主办单位" name="hostunit"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">联系电话：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="联系电话" name="phonenum"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">项目官网：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="项目官网" name="website"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">开始时间：</label>
							<div class="col-sm-6">
								<input type="text" id="dt" name="startTime" readonly class="form-control form_date dt">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">结束时间：</label>
							<div class="col-sm-6">
								<input type="text" id="dt" name="endTime" readonly class="form-control form_date dt">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">预计需要专家数量：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="预计需要专家数量" name="experttotal"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-5 control-label">多长时间(分钟)未回复短信视为不参加：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="时间(分钟)" name="disabledTime"/>
							</div>
						</div>
					</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary" id="alterProjectFormBtn">确定</button>
			</div>
		</div> 
	</div> 
</div> 
<div class="modal" id="alterProgectStatusModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">修改项目状态</h4>
			</div>
			<div class="modal-body">
				<form name="f1" role="form-horizontal" method="post">
				   <div class="form-group">
						<div class="radio col-sm-push-5">
							<label>
								<input type="radio" name="projectStatus" value="" >
								已结束 
							</label>
						</div>
					</div>
					<div class="form-group">
						<div class="radio col-sm-push-5">
							<label>
								<input type="radio" name="projectStatus" value="" >
								未评价
							</label>
						</div>
					</div>
					<div class="form-group">
						<div class="radio col-sm-push-5">
							<label>
								<input type="radio" name="projectStatus" value="" >
								已评价
							</label>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary">确定</button>
			</div>
		</div> 
	</div> 
</div> 
<div class="modal" id="assessExpertModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">评价专家</h4>
			</div>
			<div class="modal-body">
				<form id="dmFormId" action="<c:url value='/exportExpert.c'></c:url>" name="f1" role="form-horizontal" method="post" style="text-align: center;">
				   <div class="form-group">
						 <button id="downMould" style="width: 200px; margin: 0 auto;" class="btn btn-primary btn-block" type="button">下载模板</button>
                   		 <input type="hidden" name="projectId" value="">
				   </div>
				   <div class="form-group">
						 <button id="uploadAssess" style="width: 200px; margin: 0 auto;" class="btn btn-primary btn-block" type="button">导入评价数据</button>
				   </div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
			</div>
		</div> 
	</div> 
</div> 
<div class="modal" id="uploadAssessModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">导入评价数据</h4>
			</div>
			<div class="modal-body">
				<form id="uploadFormId" class="form-horizontal" role="form" action="<c:url value='/collegesUser/uploadExcel.c'></c:url>" method="post" enctype="multipart/form-data">	 
					<div class="form-group">
						<label class="control-label col-md-3">上传Excel文件： </label>
						<div class="col-md-8">
							<input id="input-44" name="file" type="file" multiple class="file-loading" data-allowed-file-extensions='["xls"]'>
						</div>
					</div>
						<label class="control-label col-md-9" style="color: red;font-size: 13px;font-weight: normal;">(文件以.xls结尾)文档格式参考<a href="" style="color: red;font-weight: normal;font-style: italic ;">模板</a> </label>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button id="assessExpertBtn" type="button" class="btn btn-primary">确定</button>
			</div>
		</div> 
	</div> 
</div> 
</body>
<script type="text/javascript">
	function universityMakeProject(){
		$("#u_makeProjectModal").modal("toggle");
	}
	function universityAltertProject(){
		//获取选中的项目
		var docEle = $(".checkchild:checked");
		if (docEle.size()==0) {
			alert("请选择您要修改的项目！");
			return;
		}else if (docEle.size()>1) {
			alert("只能选择一项！");
			return;
		} 
		var status = docEle.parent().parent().children("td[name='status']").text();
		if (status=="已结束") {
			alert("对不起,已结束的项目不能修改！");
			return;
		}
		$("#alterProjectForm input[name='id']").val(docEle.val());
		$("#alterProjectForm input[name='startTime']").val(docEle.parent().children("input[name='startTime']").val());
		$("#alterProjectForm input[name='endTime']").val(docEle.parent().children("input[name='endTime']").val());
		$("#alterProjectForm input[name='hostunit']").val(docEle.parent().children("input[name='hostunit']").val());
		$("#alterProjectForm input[name='phonenum']").val(docEle.parent().children("input[name='phonenum']").val());
		$("#alterProjectForm input[name='website']").val(docEle.parent().children("input[name='website']").val());
		$("#alterProjectForm input[name='disabledTime']").val(docEle.parent().children("input[name='disabledTime']").val());
		$("#alterProjectForm input[name='projectname']").val(docEle.parent().parent().children("td[name='projectname']").text());
		$("#alterProjectForm input[name='experttotal']").val(docEle.parent().parent().children("td[name='experttotal']").text());
		$("#u_altertProjectModal").modal("toggle");
		
	}
	
	function alterProgectStatus(){
		$("#alterProgectStatusModal").modal("toggle");
	}
	//评论专家
	function assessExpert(projectId){
		$("#dmFormId").find("input[name='projectId']").val(projectId);
		$("#assessExpertModal").modal("toggle");
	}
	$(function(){
		$("#downMould").click(function(){
			$("#dmFormId").submit();
			$("#assessExpertModal").modal("hide");
		})
		$("#uploadAssess").click(function(){
			$("#assessExpertModal").modal("hide");
			$("#uploadAssessModal").modal("toggle");
		})
		$("#input-44").fileinput({
			showCaption : true,
			language : 'zh',
			maxFileCount : 1
		});
	})
	
	function universityLookExpert(id){
		//拿到该项目ID去查询专家
		var projectId = id;
		$.ajax({
			url : "<c:url value='/collegesUser/checkProjectInfo.c'></c:url>",
			type : "post",
			data:{
				projectId:projectId
			},
			async: false
		});
		$('#myFrame', parent.document).prop("src",'<c:url value="/university/lookExpert.html"></c:url>') ;
	}
	
	//管理专家
	function universityManageExpert(id,projectName){
		var projectId = id;
		$.ajax({
			url : "<c:url value='/collegesUser/checkNotPartakeProjectExpert.c'></c:url>",
			type : "post",
			data:{
				projectId:projectId
			},
			async: false
		});
		$('#myFrame', parent.document).prop("src","<c:url value='/university/manageExpert.html?projectId="+projectId+"&projectName="+projectName+"'></c:url>") ;
	} 
	
	$(function() {
		$(".dt").datetimepicker({
			language : 'zh-CN',
			format : 'yyyy-mm-dd hh:ii',
			todayBtn : true,
			autoclose : true,
			todayHighlight : true,
			startView : 'month',
			minView : 'hour'
		}); 
		
		$("#createProjectFormBtn").click(function(){
			var projectname = $("#createProjectForm").find("input[name='projectname']").val();
			var hostunit = $("#createProjectForm").find("input[name='hostunit']").val();
			var phonenum = $("#createProjectForm").find("input[name='phonenum']").val();
			var website = $("#createProjectForm").find("input[name='website']").val();
			var startTime = $("#createProjectForm").find("input[name='startTime']").val();
			var endTime = $("#createProjectForm").find("input[name='endTime']").val();
			var experttotal = $("#createProjectForm").find("input[name='experttotal']").val();
			var disabledTime = $("#createProjectForm").find("input[name='disabledTime']").val();
			
			if(projectname=='undefined'||projectname==''||projectname==null){
				alert("请输入项目名称!");
				return;
			}
			if(hostunit=='undefined'||hostunit==''||hostunit==null){
				alert("请输入主办单位!");
				return;
			}
			if(phonenum=='undefined'||phonenum==''||phonenum==null){
				alert("请输入联系电话!");
				return;
			}
			if(!validate(phonenum)){
				alert("请输入合法的手机号!");
				return;
			}
			if(website=='undefined'||website==''||website==null){
				alert("请输入项目官网!");
				return;
			}
			if(startTime=='undefined'||startTime==''||startTime==null){
				alert("请输入开始时间!");
				return;
			}
			if(endTime=='undefined'||endTime==''||endTime==null){
				alert("请输入结束时间!");
				return;
			}
			if(startTime>=endTime){
				alert("结束时间必须大于开始时间!");
				return;
			}
			if(experttotal=='undefined'||experttotal==''||experttotal==null){
				alert("请输入预计需要专家数量!");
				return;
			}
			if(!validateNum(experttotal)){
				alert("专家数量必须为正整数!");
				return;
			}
			if(disabledTime=='undefined'||disabledTime==''||disabledTime==null){
				alert("请输入未回复短信视为不参加的最长时间!");
				return;
			}
			if(!validateNum(disabledTime)){
				alert("未回复短信视为不参加的最长时间必须为正整数!");
				return;
			}
			$.ajax({
				url : "<c:url value='/project/createProject.c'></c:url>",
				data:$("#createProjectForm").serialize(),
				success : function(data) {
					if (data==1) {
						alert("创建成功！");
						refurbish();
					}else {
						alert("创建失败！");
					}
				},
				error : function(data) {
					alert("创建失败！");
				}
			});
		})
		$("#alterProjectFormBtn").click(function(){
			var projectname = $("#alterProjectForm").find("input[name='projectname']").val();
			var hostunit = $("#alterProjectForm").find("input[name='hostunit']").val();
			var phonenum = $("#alterProjectForm").find("input[name='phonenum']").val();
			var website = $("#alterProjectForm").find("input[name='website']").val();
			var startTime = $("#alterProjectForm").find("input[name='startTime']").val();
			var endTime = $("#alterProjectForm").find("input[name='endTime']").val();
			var experttotal = $("#alterProjectForm").find("input[name='experttotal']").val();
			var disabledTime = $("#alterProjectForm").find("input[name='disabledTime']").val();
			
			if(projectname=='undefined'||projectname==''||projectname==null){
				alert("请输入项目名称!");
				return;
			}
			if(hostunit=='undefined'||hostunit==''||hostunit==null){
				alert("请输入主办单位!");
				return;
			}
			if(phonenum=='undefined'||phonenum==''||phonenum==null){
				alert("请输入联系电话!");
				return;
			}
			if(!validate(phonenum)){
				alert("请输入合法的手机号!");
				return;
			}
			if(website=='undefined'||website==''||website==null){
				alert("请输入项目官网!");
				return;
			}
			if(startTime=='undefined'||startTime==''||startTime==null){
				alert("请输入开始时间!");
				return;
			}
			if(endTime=='undefined'||endTime==''||endTime==null){
				alert("请输入结束时间!");
				return;
			}
			if(startTime>=endTime){
				alert("结束时间必须大于开始时间!");
				return;
			}
			if(experttotal=='undefined'||experttotal==''||experttotal==null){
				alert("请输入预计需要专家数量!");
				return;
			}
			if(!validateNum(experttotal)){
				alert("专家数量必须为正整数!");
				return;
			}
			if(disabledTime=='undefined'||disabledTime==''||disabledTime==null){
				alert("请输入未回复短信视为不参加的最长时间!");
				return;
			}
			if(!validateNum(disabledTime)){
				alert("未回复短信视为不参加的最长时间必须为正整数!");
				return;
			}
			$.ajax({
				url : "<c:url value='/project/updateProjectById.c'></c:url>",
				data:$("#alterProjectForm").serialize(),
				success : function(data) {
					if (data==1) {
						alert("修改成功！");
						refurbish();
					}else {
						alert("修改失败！");
					}
				},
				error : function(data) {
					alert("修改失败！");
				}
			});
		})
		
		$("#deleteProjectBtn").click(function(){
			var docEle = $(".checkchild:checked");
			if (docEle.size()==0) {
				alert("请选择您要删除的项目！");
				return;
			}else{
				if(confirm("确定要删除这"+docEle.size()+"项吗？")){
					var ids = new Array();
					for(var i=0;i<docEle.size();i++){
						ids.push(docEle[i].value);
					}
					var id = ids.join("-");
					$.ajax({
						url : "<c:url value='/project/deleteProjectById.c'></c:url>",
						type : "post",
						data : {
							id:id
						},
						success : function(data) {
							if (data==1) {
								alert("删除成功！");
								refurbish();
							}else {
								alert("删除失败！");
							}
						},
						error : function(data) {
							alert("删除失败！");
						}
					});
				} 
			}
		})
		
		var validate = function(phoneNum){
			var reg = /^1[3|4|5|7|8][0-9]{9}$/; //验证规则
			var flag = reg.test(phoneNum); //true
			return flag;
		}
		var validateNum = function(num){
			 var r = /^\+?[1-9][0-9]*$/;//正整数
			 var flag=r.test(num);
			 return flag;
		}
		
		var refurbish = function(){
			$.ajax({
				url : "<c:url value='/collegesUser/projectManage.c'></c:url>",
				type : "post",
				async: false
			});
			$('#myFrame',parent.document).prop("src",'<c:url value="/university/universityProjectManage.html"></c:url>') 
		}
		
		$("#assessExpertBtn").click(function(){
			var fv = $("#input-44").val();
			if(fv==undefined||fv==null||fv==""){
				alert("请选择文件！")
				return;
			}
			 $.ajaxFileUpload({
                url: "<c:url value='/collegesUser/uploadExcel.c'></c:url>",
                secureuri: false,
                cache: false,
                type: "post",
                dataType : 'text/html',//contentType参数是一定要有的，否则浏览器总是提示将返回的JSON结果另存为文件，不会交给ajaxfileupload处理
                fileElementId: "input-44",
                success: function (data) {
        			$("#uploadAssessModal").modal("hide");
        			alert(data);
                },
                error: function (data) {
                    alert("请求错误!");
                } 
            });
		})
	});
</script>
</html>
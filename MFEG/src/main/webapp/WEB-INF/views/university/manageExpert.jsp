<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript" src='<c:url value="/static/jquery/jquery-1.12.4.js"></c:url>'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/page.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/login/js/main.js"></script>
	<link href="${pageContext.request.contextPath }/static/css/tableSort.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/datatables/css/jquery.dataTables.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src='<c:url value="/static/bootstrap/js/bootstrap.js"></c:url>'></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/zzsc.css'/>">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/metismenu.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/demo.css">
	<script src="${pageContext.request.contextPath }/static/js/metismenu.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/fileinput/css/fileinput.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/locales/zh.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/style.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/list.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/shell.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/plugins/bootstrap/js/bootbox.min.js"></script>
	<script src="${pageContext.request.contextPath }/static/js/student/commonFunc.js"></script>
	<link rel="stylesheet" href='<c:url value="/static/bootstrap/css/bootstrap.css"></c:url>' />
</head>
<body>
<div >
      <div class=" panel-default" style="margin-bottom:0; overflow-x:auto;">
            <div class="panel-heading" style="height: 45px;">
                <div style="display: inline-block;padding-left: 0px;font-size: 16px;"><a href="javascript:universityProjectManage()"><span class="glyphicon glyphicon-share-alt" style="color: rgb(255, 140, 60);">返回</span></a></div>
                <div style="display: inline-block; ">
                &nbsp;(项目名称：${param.projectName}  &nbsp;起止时间： XXX-XXX &nbsp; 官网：XXXXXX)
                </div>
                <div style="display: inline-block;float: right;"class="btn-group">
						<button id="uAddExpertBtnId" type="button" class="btn btn-default">添加专家</button>
						<button type="button" class="btn btn-default" onclick="lookAddedExpert()">查看已添加专家&nbsp;<span style="color: blue;">XX/XX</span></button>
				</div>
            </div>
	        <div class="panel-body" style="padding-bottom:50px;">
	            <form role="form" action="" method="post">
	                <%@ include file="/WEB-INF/views/university/me_table.jsp" %>
	      	    </form>
		    </div>
     </div>
</div>
<div class="modal" id="uCkeckExpertInfoModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">详细信息</h4>
			</div>
			<div class="modal-body">
				<form action="" class="form-horizontal" method="post">
				   <table class="table table-bordered">
				   <thead>
				   </thead>
				   <tbody id="uceiId">
				     <tr>
				       <td>姓名</td>
				       <td name="name"> </td>
				       <td>性别</td>
				       <td  name="sex"> </td>
				       <td>职务</td>
				       <td  name="position"> </td>
				       <td>职称</td>
				       <td  name="jobTitleName"> </td>
				       <td>学历</td>
				       <td  name="educationName"> </td>
				     </tr>
				     <tr>
				       <td>来源</td>
				       <td colspan="9"  name="originName"> </td>
				     </tr>
				     <tr>
				       <td>工作单位</td>
				       <td colspan="4"  name="workUnit"> </td>
				       <td>所在城市</td>
				       <td colspan="4"  name="rigionName"> </td>
				     </tr>
				     <tr>
				       <td>所属行业</td>
				       <td colspan="4"  name="industryName"> </td>
				       <td>研究领域</td>
				       <td colspan="4"  name="researchArea"> </td>
				     </tr>
				     <tr>
				       <td>申报类型</td>
				       <td colspan="9"  name="declareTypeName"> </td>
				     </tr>
				     <tr>
				        <td>个人介绍</td>
				       <td colspan="9"> <textarea id="descriptionId" style="border:0px; resize : none;" class="form-control"   name="description"> </textarea> </td>
				     </tr>
				     <tr>
				       <td>工作业绩</td>
				       <td colspan="9"  name="achievement"> </td>
				     </tr>
				   </tbody>
				 </table>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
			</div>
		</div> 
	</div> 
</div> 
<script type="text/javascript">	
	function uCkeckExpertInfo(idCard,obj){
		var tr = $(obj).parent().parent();
		$("#uceiId td[name='name']").text(tr.find("td[name='name']").text());
		$("#uceiId td[name='sex']").text(tr.find("td[name='sex']").text());
		$("#uceiId td[name='position']").text(tr.find("td[name='position']").text());
		$("#uceiId td[name='jobTitleName']").text(tr.find("input[name='jobTitleName']").val());
		$("#uceiId td[name='educationName']").text(tr.find("input[name='educationName']").val());
		$("#uceiId td[name='originName']").text(tr.find("td[name='originName']").text());
		$("#uceiId td[name='workUnit']").text(tr.find("input[name='workUnit']").val());
		$("#uceiId td[name='rigionName']").text(tr.find("input[name='rigionName']").val());
		$("#uceiId td[name='industryName']").text(tr.find("td[name='industryName']").text());
		$("#uceiId td[name='researchArea']").text(tr.find("td[name='researchArea']").text());
		$("#uceiId td[name='declareTypeName']").text(tr.find("td[name='declareTypeName']").text());
		$("#uceiId textarea[name='description']").text(tr.find("input[name='description']").val());
		$("#uceiId td[name='achievement']").html(tr.find("input[name='achievement']").val());
		$("#uCkeckExpertInfoModal").modal("toggle");
	}
	function universityProjectManage(){
		$('#myFrame', parent.document).prop("src",'<c:url value="/university/universityProjectManage.html"></c:url>') ;
	}
	function lookAddedExpert(){
		var projectId = "${param.projectId}";
		var projectName = "${param.projectName}";
		$.ajax({
			url : "<c:url value='/collegesUser/checkAddedExpert.c'></c:url>",
			type : "post",
			data:{
				projectId:projectId
			},
			async: false
		});
		$('#myFrame', parent.document).prop("src","<c:url value='/university/lookAddedExpert.html?projectId="+projectId+"&projectName="+projectName+"'></c:url>") ;
	} 
	
	$(function(){
		$("#uAddExpertBtnId").click(function(){
			var docEle = $(".checkchild:checked");
			if (docEle.size()==0) {
				alert("请选择您要添加的专家！");
				return;
			}else{
				var projectId = "${param.projectId}";
				var expertIds = new Array();
				for(var i=0;i<docEle.size();i++){
					expertIds.push(docEle[i].value);
				}
				var expertId = expertIds.join("-");
				$.ajax({
					url : "<c:url value='/collegesUser/addProjectExpert.c'></c:url>",
					type : "post",
					data : {
						expertId:expertId,
						projectId:projectId
					},
					success : function(data) {
						if (data==1) {
							alert("添加成功！");
							refurbish(projectId,'${param.projectName}');
						}else {
							alert("添加失败！");
						}
					},
					error : function(data) {
						alert("添加失败！");
					}
				});
			}
		})
		
		var refurbish = function(projectId,projectName){
			$.ajax({
				url : "<c:url value='/collegesUser/checkNotPartakeProjectExpert.c'></c:url>",
				type : "post",
				data:{
					projectId:projectId
				},
				async: false
			});
			$('#myFrame', parent.document).prop("src","<c:url value='/university/manageExpert.html?projectId="+projectId+"&projectName="+projectName+"'></c:url>") ;
		}
	})
	
	
	
</script>
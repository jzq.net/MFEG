<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table id="oumTable" class="display cell-border"  style="width: 100%;">
	<thead>
		<tr>
			<td style="width: 10px;">
				 <input type="checkbox" id="checkboxParent"/>
			</td>
			<td>
				序列 
			</td>
			<td>
				处室名称
			</td>
			<td>
				用户名
			</td>
			<td>
				联系人
			</td>
			<td>
				电话
			</td>
			<td>
				状态
			</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="d" items="${departmentUsers}" >
			<tr>
				<td><input type="checkbox"  class="checkchild" value="${d.id}"/></td>
				<td> </td>
				<td name="departmentName">
					${d.departmentName}
				</td>
				<td name="userName">
					${d.userName}
				</td>
				<td name="linkman">
					${d.linkman}
				</td>
				<td name="phoneNum">
					${d.phoneNum}
				</td>
				<td name="status">
					${d.status}
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<script type="text/javascript">
	$(function() {
		//设置数据源
		$("#oumTable").DataTable({
			"rowCallback": function( row, data, index ) {
			  },
			"aaSorting" : [[0, "asc"]], //默认的排序方式，第1列，升序排列  
			columns: [{
				"data": "null",
				"bSortable":false,
			},{
				"data": "index",
				'sClass': "text-center",
				width : "30px",  
				render : function(data, type, row, meta) {  
                    // 显示行号  
                    var startIndex = meta.settings._iDisplayStart;  
                    return startIndex + meta.row + 1;  
               	}  
			},{
				"data": "officeName"
			}, {
				"data": "linkman"
			}, {
				"data": "tel"
			}, {
				"data": "status"
			}, {
				"data": "userName"
			}],
			 "paging": true,  
			 "sPaginationType": "full_numbers",//用于指定分页器风格
			 "oLanguage": {    // 语言设置  
		            "sLengthMenu": "每页显示 _MENU_ 条记录",  
		            "sZeroRecords": "抱歉， 没有找到",  
		            "sInfo": "从 _START_ 到 _END_  共 _TOTAL_条数据",  
		            "sInfoEmpty": "没有数据",  
		            "sInfoFiltered": "(从 _MAX_ 条数据中检索)",  
		            "sZeroRecords": "没有检索到数据",  
		            "sSearch": "搜索:",  
		            "oPaginate": {  
		              "sFirst": "首页",  
		              "sPrevious": "上一页",  
		              "sNext": "下一页",  
		              "sLast": "尾页"  
		            }  
		          },  
		});
	});
</script> 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="modal" id="altertUserModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">修改用户</h4>
			</div>
			<div class="modal-body">
					<form name="f1" class="form-horizontal" method="post" id="ehAlterExpertUserForm">
					<input type="hidden" name="userRole" value="3">
					<input type="hidden" name="id" >
						<div class="form-group">
							<label class="col-sm-3 control-label">用户名：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="用户名"  name="userName"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">姓名：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="姓名" name="name"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">电话：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="电话" name="mobile"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">状态：</label>
							<div class="col-sm-6">
							    <select class="form-control" name="status"> 
							      <option value="1">正常</option> 
							      <option value="2">禁用</option> 
							    </select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">密码：</label>
							<div class="col-sm-6">
								<input type="password" class="form-control" placeholder="密码" name="password"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">确认密码：</label>
							<div class="col-sm-6">
								<input type="password" class="form-control" placeholder="确认密码" name="password2"/>
							</div>
						</div>
					</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary" id="ehAlterExpertUserBtn">确定</button>
			</div>
		</div> 
	</div> 
</div> 
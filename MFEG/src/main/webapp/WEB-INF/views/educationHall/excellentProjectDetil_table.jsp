<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table id="excellentProjectTable" class="display cell-border" style="width: 100%;">
	<thead>
		<tr>
			<td>
				序列 
			</td>
			<td>
				创建用户
			</td>
			<td>
				项目名称
			</td>
			<td>
				项目日期
			</td>
			<td>
				项目状态
			</td>
			<td>
				联系电话
			</td>
			<td>
				主办单位
			</td>
			<td>
				项目官网
			</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="ep" items="${excellentProjects}" >
			<tr>
				<td></td>
				<td>
					${ep.organiser}
				</td>
				<td>
					${ep.projectname}
				</td>
				<td>
					${ep.starttime}~${ep.endtime}
				</td>
				<td>
					${ep.status}
				</td>
				<td>
					${ep.phonenum}
				</td>
				<td>
					${ep.hostunit}
				</td>
				<td>
					${ep.website}
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<script type="text/javascript">
	$(function() {
		//设置数据源
		$("#excellentProjectTable").DataTable({
			"rowCallback": function( row, data, index ) {
			  },
			"aaSorting" : [[0, "asc"]], //默认的排序方式，第1列，升序排列  
			columns: [{
				"data": "index",
				'sClass': "text-center",
				width : "30px",  
				render : function(data, type, row, meta) {  
                    // 显示行号  
                    var startIndex = meta.settings._iDisplayStart;  
                    return startIndex + meta.row + 1;  
               	}  
			},{
				"data": "officeName"
			}, {
				"data": "linkman"
			}, {
				"data": "tel"
			}, {
				"data": "status"
			}, {
				"data": "useme"
			}, {
				"data": "usName"
			}, {
				"data": "usme"
			}],
			 "paging": true,  
			 "sPaginationType": "full_numbers",//用于指定分页器风格
			 "oLanguage": {    // 语言设置  
		            "sLengthMenu": "每页显示 _MENU_ 条记录",  
		            "sZeroRecords": "抱歉， 没有找到",  
		            "sInfo": "从 _START_ 到 _END_  共 _TOTAL_条数据",  
		            "sInfoEmpty": "没有数据",  
		            "sInfoFiltered": "(从 _MAX_ 条数据中检索)",  
		            "sZeroRecords": "没有检索到数据",  
		            "sSearch": "搜索:",  
		            "oPaginate": {  
		              "sFirst": "首页",  
		              "sPrevious": "上一页",  
		              "sNext": "下一页",  
		              "sLast": "尾页"  
		            }  
		          },  
		});
	});
</script> 
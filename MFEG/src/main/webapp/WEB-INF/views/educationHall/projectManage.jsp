<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript" src='<c:url value="/static/jquery/jquery-1.12.4.js"></c:url>'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/page.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/login/js/main.js"></script>
	<link href="${pageContext.request.contextPath }/static/css/tableSort.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/datatables/css/jquery.dataTables.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src='<c:url value="/static/bootstrap/js/bootstrap.js"></c:url>'></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/zzsc.css'/>">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/metismenu.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/demo.css">
	<script src="${pageContext.request.contextPath }/static/js/metismenu.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/fileinput/css/fileinput.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/locales/zh.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/style.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/list.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/shell.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/plugins/bootstrap/js/bootbox.min.js"></script>
	<script src="${pageContext.request.contextPath }/static/js/student/commonFunc.js"></script>
	<link rel="stylesheet" href='<c:url value="/static/bootstrap/css/bootstrap.css"></c:url>' />
</head>
<body>
<div>
      <div class="panel-default" style="margin-bottom:0; overflow-x:auto; border: 0px;">
	            <div class="panel-heading" style="height: 45px;">
	                <div style="display: inline-block;padding-left: 0px;font-size: 16px;"  class="col-sm-2">项目管理</div>
	                <div style="display: inline-block; float: right;"class="btn-group" >
					  <button type="button" class="btn btn-default" id="euStopProjectBtn">暂停项目</button>
					  <button type="button" class="btn btn-default" id="euExportProjectBtn">导出</button>
					  <form method="post" id="euExportProjectId" style="display: none;" action="<c:url value='/exportProject.c'></c:url>"></form>
					</div> 
	            </div>
		        <div class="panel-body" style="padding-bottom:50px;">
		            <form id="listForm" role="form" action="" method="post">
		                <%@ include file="/WEB-INF/views/educationHall/pm_table.jsp" %>
		      	    </form>
			    </div>
	     </div>
	</div>
</body>
<script type="text/javascript">
function expertInfo(obj){
	$.ajax({
		url : "<c:url value='/educationhallUser/checkProjectInfo.c'></c:url>",
		type : "post",
		data:{
			projectId:obj
		},
		async: false
	});
	$('#myFrame', parent.document).prop("src",'<c:url value="/educationHall/lookExpert.html?projectId='+obj+'"></c:url>');
}

$(function(){
	$("#euStopProjectBtn").click(function(){
		//获取选中的项目
		var docEle = $(".checkchild:checked");
		if (docEle.size()==0) {
			alert("请选择您要修改的项目！");
			return;
		}else{
			var projectIds = new Array();
			for(var i=0;i<docEle.size();i++){
				var status = $(docEle[i]).next("input[name='status']").val()
				if(status=='已结束'){
					alert("已结束的项目不可以暂停！")
					return;
				}
				projectIds.push(docEle[i].value);
			}
			var projectId = projectIds.join("-");
			$.ajax({
				url : "<c:url value='/educationhallUser/stopProject.c'></c:url>",
				type : "post",
				data : {
					projectId:projectId
				},
				success : function(data) {
					if (data==1) {
						alert("暂停成功！");
						refurbish();
					}else {
						alert("暂停失败！");
					}
				},
				error : function(data) {
					alert("暂停失败！");
				}
			});
		} 
	})
	
	$("#euExportProjectBtn").click(function(){
		$("#euExportProjectId").submit();
	})
	
})
function refurbish(){
		//项目管理
		$.ajax({
			url : "<c:url value='/educationhallUser/projectManage.c'></c:url>",
			type : "post",
			async: false
		});
		$('#myFrame',parent.document).prop("src",'<c:url value="/educationHall/projectManage.html"></c:url>') 
	}
</script>
</html>

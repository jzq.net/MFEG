<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="modal" id="batchUserModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">批量添加用户</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" action="" method="post">	 
					<div class="form-group">
						<label class="control-label col-md-3">上传Excel文件： </label>
						<div class="col-md-8">
							<input id="input-4" name="file" type="file" multiple class="file-loading" data-allowed-file-extensions='["xls"]'>
						</div>
					</div>
						<label class="control-label col-md-9" style="color: red;font-size: 13px;font-weight: normal;">(文件以.xls结尾)文档格式参考<a href="" style="color: red;font-weight: normal;font-style: italic ;">模板</a> </label>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary">确定</button>
			</div>
		</div> 
	</div> 
</div> 
<script type="text/javascript">
	$(function() {
		$("#input-4").fileinput({
			showCaption : true,
			language : 'zh',
			maxFileCount : 1
		});
	});
</script> 
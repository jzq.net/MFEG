<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript" src='<c:url value="/static/jquery/jquery-1.12.4.js"></c:url>'></script>
	<script type="text/javascript" src='<c:url value="/static/js/ajaxfileupload.js"></c:url>'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/page.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/login/js/main.js"></script>
	<link href="${pageContext.request.contextPath }/static/css/tableSort.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/datatables/css/jquery.dataTables.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src='<c:url value="/static/bootstrap/js/bootstrap.js"></c:url>'></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/zzsc.css'/>">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/metismenu.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/demo.css">
	<script src="${pageContext.request.contextPath }/static/js/metismenu.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/fileinput/css/fileinput.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/locales/zh.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/style.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/list.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/shell.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/plugins/bootstrap/js/bootbox.min.js"></script>
	<script src="${pageContext.request.contextPath }/static/js/student/commonFunc.js"></script>
	<link rel="stylesheet" href='<c:url value="/static/bootstrap/css/bootstrap.css"></c:url>' />
</head>
<body>
<div>
      <div class="panel-default" style="margin-bottom:0; overflow-x:auto; border: 0px;">
            <div class="panel-heading" style="height: 45px;">
                <div style="display: inline-block; padding-left: 0px;font-size: 16px;"> 高校用户管理 </div>
                <div class="dropdown" style="display: inline-block;float: right">
				  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">操作 <span class="caret"></span></button>
				  <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1">
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:u_addUser()">添加用户</a></li>
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:u_stopUser()">禁用用户</a></li>
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:u_altertUser()">修改用户</a></li>
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:u_batchAddUser()">批量添加用户</a></li>
				    <li role="presentation"><a role="menuitem" tabindex="-1" href='<c:url value="/downloadUserModel.c?userRole=2"></c:url>'>下载模板</a></li>
				  </ul>
				</div> 
            </div>
	        <div class="panel-body" style="padding-bottom:50px;">
	            <form id="listForm" role="form" action="" method="post">
	                <%@ include file="/WEB-INF/views/educationHall/uu_table.jsp" %>
	      	    </form>
		    </div>
     </div>
</div>
<div class="modal" id="u_addUserModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">添加用户</h4>
			</div>
			<div class="modal-body">
					<form name="f1" class="form-horizontal" method="post" id="ehAddCollegeUserForm">
						<input type="hidden"  name="userRole" value="2"/>
						<div class="form-group">
							<label class="col-sm-3 control-label">高校代码：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="高校代码"  name="code"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">高校名称：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="高校名称" name="name"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">用户名称：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="用户名称" name="userName"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">联系人：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="联系人" name="linkman"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">电话：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="电话" name="phoneNum"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">状态：</label>
							<div class="col-sm-6">
							    <select class="form-control" name="status"> 
							      <option value="1">正常</option> 
							      <option value="2">禁用</option> 
							    </select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">密码：</label>
							<div class="col-sm-6">
								<input type="password" class="form-control" placeholder="密码" name="password"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">确认密码：</label>
							<div class="col-sm-6">
								<input type="password" class="form-control" placeholder="确认密码" name="password2"/>
							</div>
						</div>
					</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary" id="ehAddCollegeUserBtn">确定</button>
			</div>
		</div> 
	</div> 
</div> 

<div class="modal" id="u_altertUserModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">修改用户</h4>
			</div>
			<div class="modal-body">
					<form name="f1" class="form-horizontal" method="post" id="ehAlterCollegeUserForm">
						<input type="hidden"  name="userRole" value="2"/>
						<input type="hidden"  name="id"/>
						<div class="form-group">
							<label class="col-sm-3 control-label">高校代码：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="高校代码"  name="code"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">高校名称：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="高校名称" name="name"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">用户名称：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="用户名称" name="userName"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">联系人：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="联系人" name="linkman"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">电话：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="电话" name="phoneNum"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">状态：</label>
							<div class="col-sm-6">
							    <select class="form-control" name="status"> 
							      <option value="1">正常</option> 
							      <option value="2">禁用</option> 
							    </select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">密码：</label>
							<div class="col-sm-6">
								<input type="password" class="form-control" placeholder="密码" name="password"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">确认密码：</label>
							<div class="col-sm-6">
								<input type="password" class="form-control" placeholder="确认密码" name="password2"/>
							</div>
						</div>
					</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary" id="ehAlterCollegeUserBtn">确定</button>
			</div>
		</div> 
	</div> 
</div> 

<div class="modal" id="u_batchUserModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">批量导入用户</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" action="" method="post">	 
					<div class="form-group">
						<label class="control-label col-md-3">上传Excel文件： </label>
						<div class="col-md-8" >
							<input id="input-u" name="file" type="file" multiple class="file-loading" data-allowed-file-extensions='["xls"]'>
						</div>
					</div>
						<label class="control-label col-md-9" style="color: red;font-size: 13px;font-weight: normal;">(文件以.xls结尾)文档格式参考<a href="" style="color: red;font-weight: normal;font-style: italic ;">模板</a> </label>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary" id="ehBatchCollegeUserBtn">确定</button>
			</div>
		</div> 
	</div> 
</div> 
</body>
<script type="text/javascript">
	function u_addUser(){
		$("#u_addUserModal").modal("toggle");
	}
	
	function u_stopUser(){
		var docEle = $(".checkchild:checked");
		if (docEle.size()==0) {
			alert("请选择您要禁用的用户！");
			return;
		}else{
			var ids = new Array();
			for(var i=0;i<docEle.size();i++){
				ids.push(docEle[i].value);
			}
			var id = ids.join("-");
			$.ajax({
				url : "<c:url value='/educationhallUser/stopUser.c'></c:url>",
				type : "post",
				data : {
					id:id,
					userRole:'2'
				},
				success : function(data) {
					if (data==1) {
						alert("禁用成功！");
						refurbish();
					}else {
						alert("禁用失败！");
					}
				},
				error : function(data) {
					alert("禁用失败！");
				}
			});
		}
	}
	
	function u_altertUser(){
		var docEle = $(".checkchild:checked");
		if (docEle.size()==0) {
			alert("请选择您要修改的用户！");
			return;
		}else if (docEle.size()>1) {
			alert("只能选择一个用户！")
			return;
		}
		$("#ehAlterCollegeUserForm input[name='code']").val(docEle.parent().parent().find("input[name='code']").val());
		$("#ehAlterCollegeUserForm input[name='name']").val(docEle.parent().parent().find("input[name='name']").val());
		$("#ehAlterCollegeUserForm input[name='userName']").val(docEle.parent().parent().children("td[name='userName']").text());
		$("#ehAlterCollegeUserForm input[name='linkman']").val(docEle.parent().parent().children("td[name='linkman']").text());
		$("#ehAlterCollegeUserForm input[name='phoneNum']").val(docEle.parent().parent().children("td[name='phoneNum']").text());
		var st = docEle.parent().parent().children("td[name='status']").text();
		$("#ehAlterCollegeUserForm option").each(function(i,n){
            if($(n).text()==st){
                $(n).attr("selected",true);
            }
        })
		$("#u_altertUserModal").modal("toggle");
	}
	function u_batchAddUser(){
		$("#u_batchUserModal").modal("toggle");
	}
	$(function() {
		$("#input-u").fileinput({
			showCaption : true,
			language : 'zh',
			maxFileCount : 1
		});
		
		$("#ehAddCollegeUserBtn").click(function(){
			var code = $("#ehAddCollegeUserForm").find("input[name='code']").val();
			var name = $("#ehAddCollegeUserForm").find("input[name='name']").val();
			var userName = $("#ehAddCollegeUserForm").find("input[name='userName']").val();
			var linkman = $("#ehAddCollegeUserForm").find("input[name='linkman']").val();
			var phoneNum = $("#ehAddCollegeUserForm").find("input[name='phoneNum']").val();
			var password = $("#ehAddCollegeUserForm").find("input[name='password']").val();
			if(code=='undefined'||code==''||code==null){
				alert("请输入高校代码!");
				return;
			}
			if(code.length>5){
				alert("高校代码不能超过五位数!");
				return;
			}
			if(name=='undefined'||name==''||name==null){
				alert("请输入高校名称!");
				return;
			}
			if(userName=='undefined'||userName==''||userName==null){
				alert("请输入用户名称!");
				return;
			}
			if(linkman=='undefined'||linkman==''||linkman==null){
				alert("请输入联系人!");
				return;
			}
			if(phoneNum=='undefined'||phoneNum==''||phoneNum==null){
				alert("请输入电话!");
				return;
			}
			if(!validate(phoneNum)){
				alert("请输入合法的手机号!");
				return;
			}
			if(password=='undefined'||password==''||password==null){
				alert("请输入密码!");
				return;
			}
			if(password!=$("#ehAddCollegeUserForm").find("input[name='password2']").val()){
				alert("两次密码输入不相同!")
				return;
			}
			$.ajax({
				url : "<c:url value='/educationhallUser/addUser.c'></c:url>",
				data:$("#ehAddCollegeUserForm").serialize(),
				success : function(data) {
					if (data==1) {
						alert("添加成功！");
						refurbish();
					}else if(data==3) {
						alert("用户名已存在,添加失败！");
					}else{
						alert("添加失败！");
					}
				},
				error : function(data) {
					alert("添加失败！");
				}
			});
		})
		
		$("#ehAlterCollegeUserBtn").click(function(){
			var docEle = $(".checkchild:checked");
			var id = docEle[0].value;
			$("#ehAlterCollegeUserForm").find("input[name='id']").val(id);
			var code = $("#ehAlterCollegeUserForm").find("input[name='code']").val();
			var name = $("#ehAlterCollegeUserForm").find("input[name='name']").val();
			var userName = $("#ehAlterCollegeUserForm").find("input[name='userName']").val();
			var linkman = $("#ehAlterCollegeUserForm").find("input[name='linkman']").val();
			var phoneNum = $("#ehAlterCollegeUserForm").find("input[name='phoneNum']").val();
			var password = $("#ehAlterCollegeUserForm").find("input[name='password']").val();
			if(code=='undefined'||code==''||code==null){
				alert("请输入高校代码!");
				return;
			}
			if(code.length>5){
				alert("高校代码不能超过五位数!");
				return;
			}
			if(name=='undefined'||name==''||name==null){
				alert("请输入高校名称!");
				return;
			}
			if(userName=='undefined'||userName==''||userName==null){
				alert("请输入用户名称!");
				return;
			}
			if(linkman=='undefined'||linkman==''||linkman==null){
				alert("请输入联系人!");
				return;
			}
			if(phoneNum=='undefined'||phoneNum==''||phoneNum==null){
				alert("请输入电话!");
				return;
			}
			if(!validate(phoneNum)){
				alert("请输入合法的手机号!");
				return;
			}
			if(password=='undefined'||password==''||password==null){
				alert("请输入密码!");
				return;
			}
			if(password!=$("#ehAlterCollegeUserForm").find("input[name='password2']").val()){
				alert("两次密码输入不相同!")
				return;
			}
			$.ajax({
				url : "<c:url value='/educationhallUser/alterUser.c'></c:url>",
				data:$("#ehAlterCollegeUserForm").serialize(),
				success : function(data) {
					if (data==1) {
						alert("修改成功！");
						refurbish();
					}else{
						alert("修改失败！");
					}
				},
				error : function(data) {
					alert("修改失败！");
				}
			});
		})
		
		$("#ehBatchCollegeUserBtn").click(function(){
			var fv = $("#input-u").val();
			if(fv==undefined||fv==null||fv==""){
				alert("请选择文件！")
				return;
			}
			 $.ajaxFileUpload({
                url: "<c:url value='/uploadCollegeUserInfo.c'></c:url>",
                secureuri: false,
                cache: false,
                type: "post",
                dataType : 'text/html',//contentType参数是一定要有的，否则浏览器总是提示将返回的JSON结果另存为文件，不会交给ajaxfileupload处理
                fileElementId: "input-u",
                success: function (data) {
        			$("#u_batchUserModal").modal("hide");
        			alert(data);
                },
                error: function (data) {
                    alert("请求错误!");
                } 
            });
		})
		
		var validate = function(phoneNum){
			var reg = /^1[3|4|5|7|8][0-9]{9}$/; //验证规则
			var flag = reg.test(phoneNum); //true
			return flag;
		}
		
	});
	
	function refurbish(){
		$.ajax({
			url : "<c:url value='/educationhallUser/userManage.c'></c:url>",
			type : "post",
			data:{
				userRole:'2'
			},
			async: false
		});
		$('#myFrame',parent.document).prop("src",'<c:url value="/educationHall/universityUserManage.html"></c:url>') 
	}
</script> 
</html>
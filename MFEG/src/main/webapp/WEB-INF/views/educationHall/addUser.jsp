<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="modal" id="addUserModal">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">添加用户</h4>
				</div>
				<div class="modal-body">
						<form name="f1" class="form-horizontal" method="post">
							<div class="form-group">
								<label class="col-sm-3 control-label">处室名称：</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" placeholder="处室名称"  name="officeName"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">用户名称：</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" placeholder="用户名称" name="userName"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">联系人：</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" placeholder="联系人" name="linkman"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">电话：</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" placeholder="电话" name="tel"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">状态：</label>
								<div class="col-sm-6">
								    <select class="form-control"> 
								      <option>正常</option> 
								      <option>禁用</option> 
								    </select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">密码：</label>
								<div class="col-sm-6">
									<input type="password" class="form-control" placeholder="密码" name="pwd"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">确认密码：</label>
								<div class="col-sm-6">
									<input type="password" class="form-control" placeholder="确认密码" name="pwd2"/>
								</div>
							</div>
						</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
					<button type="button" class="btn btn-primary">确定</button>
				</div>
			</div> 
		</div> 
	</div> 
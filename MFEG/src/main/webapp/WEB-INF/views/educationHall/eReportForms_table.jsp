<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table class="table table-hover table-bordered etable-expandable">
	<thead>
		<tr>
			<th>高校代码/名称</th>
			<th>用户名</th>
			<th>姓名</th>
			<th>电话</th>
			<th>状态</th>
			<th>操作</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="e" items="${expertUsers}" varStatus="ev">
			<tr>
				<td><span class="glyphicon glyphicon-plus" style="font-size: 10px;">&nbsp;</span>${e.collegesCode}/${e.collegesName}</td>
				<td>${e.username}</td>
				<td>${e.name}</td>
				<td>${e.mobile}</td>
				<td>${e.status}</td>
				<td><a href="#" onclick='showEDetilInfo("e_${ev.index}")' style=" text-decoration: none;">[详细信息]</a></td>
			</tr>
			<tr class="hide">
				<td></td>
				<td colspan="5">
					<div>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>创建用户</th>
									<th>项目名称</th>
									<th>项目日期</th>
									<th>项目状态</th>
									<th>主办单位</th>
									<th>项目官网</th>
									<th>联系电话</th>
									<th>需要专家</th>
									<th>已有专家</th>
									<th>优秀人数</th>
									<th>评价</th>
									<th>评语</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="ep" items="${e.projects}" varStatus="epv">
									<tr>
										<td>${ep.organiser}</td>
										<td>${ep.projectname} </td>
										<td>${ep.starttime}~${ep.endtime} </td>
										<td>${ep.status} </td>
										<td>${ep.hostunit} </td>
										<td>${ep.website} </td>
										<td>${ep.phonenum} </td>
										<td>${ep.experttotal} </td>
										<td>XXX </td>
										<td>XXX </td>
										<td>${ep.appraise} </td>
										<td>${ep.comment} </td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</td>
			</tr>
		</c:forEach>	
	</tbody>
</table>
<!-- 模态对话框 -->
<c:forEach var="e" items="${expertUsers}" varStatus="ev">
	<div class="modal" id="showEDetilInfo_modal_e_${ev.index}">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">详细信息</h4>
				</div>
				<div class="modal-body">
					 <form>
					   	<table class="table table-bordered">
						<thead>
							<tr>
								<th>创建用户</th>
								<th>项目名称</th>
								<th>项目日期</th>
								<th>项目状态</th>
								<th>主办单位</th>
								<th>项目官网</th>
								<th>联系电话</th>
								<th>需要专家</th>
								<th>已有专家</th>
								<th>优秀人数</th>
								<th>评价</th>
								<th>评语</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="ep" items="${e.projects}" varStatus="epv">
								<tr>
									<td>${ep.organiser}</td>
									<td>${ep.projectname} </td>
									<td>${ep.starttime}~${ep.endtime} </td>
									<td>${ep.status} </td>
									<td>${ep.hostunit} </td>
									<td>${ep.website} </td>
									<td>${ep.phonenum} </td>
									<td>${ep.experttotal} </td>
									<td>XXX </td>
									<td>XXX </td>
									<td>${ep.appraise} </td>
									<td>${ep.comment} </td>
								</tr>
							</c:forEach>	
						</tbody>
					</table>
					</form> 
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div> 
		</div> 
	</div> 
</c:forEach>	
<script type="text/javascript">
	$(function () {
		$(".etable-expandable tr[class='hide']").attr("display","display");
		$(".etable-expandable tr td:has(span)").click(function(){
			if($(this).parent().next('tr').prop('class')=="hide"){
				$(this).parent().next('tr').removeProp('class');
			}else{
				$(this).parent().next('tr').prop('class','hide');
			}
		})
	});
	function showEDetilInfo(index){
		$("#showEDetilInfo_modal_"+index).modal("toggle");
	}
</script> 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table class="table table-hover table-bordered utable-expandable">
	<thead>
		<tr>
			<th>学校名</th>
			<th>共有项目</th>
			<th>进行中项目</th>
			<th>操作</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="c" items="${infoMap.collegesUsers}" varStatus="cv">
			<tr>
				<td><span class="glyphicon glyphicon-plus" style="font-size: 10px;">&nbsp;</span>${c.name}</td>
				<td>${c.projects.size()}</td>
				<td>XXX</td>
				<td>
					<a href="#" onclick='showOUDetilInfo("c_${cv.index}")' style=" text-decoration: none;">[详细信息]</a>
				</td>
			</tr>
			
			<tr class="hide">
				<td></td>
				<td colspan="3">
					<div>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>创建用户</th>
									<th>项目名称</th>
									<th>项目日期</th>
									<th>项目状态</th>
									<th>主办单位</th>
									<th>项目官网</th>
									<th>联系电话</th>
									<th>需要专家</th>
									<th>已有专家</th>
									<th>优秀专家数</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="cp" items="${c.projects}" varStatus="cpv">
									<tr>
										<td>${cp.organiser}</td>
										<td>${cp.projectname} </td>
										<td>${cp.starttime}~${cp.endtime} </td>
										<td>${cp.status} </td>
										<td>${cp.hostunit} </td>
										<td>${cp.website} </td>
										<td>${cp.phonenum} </td>
										<td>${cp.experttotal} </td>
										<td>XXX </td>
										<td>XXX </td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</td>
			</tr>
		</c:forEach>
		<c:if test="${not empty infoMap.departmentUsers}">
			<tr>
			<th>处室名</th>
			<th>共有项目</th>
			<th>进行中项目</th>
			<th>操作</th>
		</tr>
		</c:if>
		<c:forEach var="d" items="${infoMap.departmentUsers}" varStatus="dv">
			<tr>
				<td><span class="glyphicon glyphicon-plus" style="font-size: 10px;">&nbsp;</span>${d.departmentName}</td>
				<td>${d.projects.size()}</td>
				<td>XXX</td>
				<td>
					<a href="#" onclick='showOUDetilInfo("d_${dv.index}")' style=" text-decoration: none;">[详细信息]</a>
				</td>
			</tr>
			<tr class="hide">
				<td></td>
				<td colspan="3">
					<div>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>创建用户</th>
									<th>项目名称</th>
									<th>项目日期</th>
									<th>项目状态</th>
									<th>主办单位</th>
									<th>项目官网</th>
									<th>联系电话</th>
									<th>需要专家</th>
									<th>已有专家</th>
									<th>优秀专家数</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="dp" items="${d.projects}" varStatus="dpv">
									<tr>
										<td>${dp.organiser}</td>
										<td>${dp.projectname} </td>
										<td>${dp.starttime}~${cp.endtime} </td>
										<td>${dp.status} </td>
										<td>${dp.hostunit} </td>
										<td>${dp.website} </td>
										<td>${dp.phonenum} </td>
										<td>${dp.experttotal} </td>
										<td>XXX </td>
										<td>XXX </td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<!-- 模态对话框 -->
<c:forEach var="c" items="${infoMap.collegesUsers}" varStatus="cv">
	<div class="modal" id="showOUDetilInfo_modal_c_${cv.index}" style="display: none;">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">详细信息</h4>
				</div>
				<div class="modal-body">
					 <form>
					    <table class="table table-bordered">
							<thead>
								<tr>
									<th>创建用户</th>
									<th>项目名称</th>
									<th>项目日期</th>
									<th>项目状态</th>
									<th>主办单位</th>
									<th>项目官网</th>
									<th>联系电话</th>
									<th>需要专家</th>
									<th>已有专家</th>
									<th>优秀专家数</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="cp" items="${c.projects}" varStatus="cpv">
									<tr>
										<td>${cp.organiser}</td>
										<td>${cp.projectname} </td>
										<td>${cp.starttime}~${cp.endtime} </td>
										<td>${cp.status} </td>
										<td>${cp.hostunit} </td>
										<td>${cp.website} </td>
										<td>${cp.phonenum} </td>
										<td>${cp.experttotal} </td>
										<td>XXX </td>
										<td>XXX </td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</form> 
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div> 
		</div> 
	</div> 
</c:forEach>	
<c:forEach var="d" items="${infoMap.departmentUsers}" varStatus="dv">
	<div class="modal" id="showOUDetilInfo_modal_d_${dv.index}" style="display: none;">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">详细信息</h4>
				</div>
				<div class="modal-body">
					 <form>
					    <table class="table table-bordered">
							<thead>
								<tr>
									<th>创建用户</th>
									<th>项目名称</th>
									<th>项目日期</th>
									<th>项目状态</th>
									<th>主办单位</th>
									<th>项目官网</th>
									<th>联系电话</th>
									<th>需要专家</th>
									<th>已有专家</th>
									<th>优秀专家数</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="dp" items="${d.projects}" varStatus="dpv">
									<tr>
										<td>${dp.organiser}</td>
										<td>${dp.projectname} </td>
										<td>${dp.starttime}~${cp.endtime} </td>
										<td>${dp.status} </td>
										<td>${dp.hostunit} </td>
										<td>${dp.website} </td>
										<td>${dp.phonenum} </td>
										<td>${dp.experttotal} </td>
										<td>XXX </td>
										<td>XXX </td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</form> 
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div> 
		</div> 
	</div> 
</c:forEach>		
	
	
<script type="text/javascript">
	$(function () {
		$(".utable-expandable tr[class='hide']").attr("display","display");
		$(".utable-expandable tr td:has(span)").click(function(){
			if($(this).parent().next('tr').prop('class')=="hide"){
				$(this).parent().next('tr').removeProp('class');
			}else{
				$(this).parent().next('tr').prop('class','hide');
			}
		})
	});
	function showOUDetilInfo(index){
		$("#showOUDetilInfo_modal_"+index).modal("toggle");
	}
</script> 
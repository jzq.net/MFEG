<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table id="edTable" class="display cell-border" style="width: 100%;">
	<thead>
		<tr>
			<td>
				序列 
			</td>
			<td>
				高校代码
			</td>
			<td>
				用户名
			</td>
			<td>
				姓名
			</td>
			<td>
				性别
			</td>
			<td>
				职务
			</td>
			<td>
				行业
			</td>
			<td>
				领域
			</td>
			<td>
				来源
			</td>
			<td>
				类型
			</td>
			<td>
				详细信息
			</td>
			<td>
				参与项目
			</td>
			<td>
				优秀项目
			</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="e" items="${expertUsers}" varStatus="v">
			<tr>
				<td></td>
				<td name="collegesCode">
					 ${e.collegesCode}
				</td>
				<td  name="username">
					${e.username}
				</td>
				<td  name="name">
					${e.name}
				</td>
				<td  name="sex">
					${e.sex}
				</td>
				<td  name="position">
					${e.position}
				</td>
				<td  name="industryName">
					${e.industryName}
				</td>
				<td  name="researchArea">
					${e.researchArea}
				</td>
				<td  name="originName">
					${e.originName}
				</td>
				<td  name="declareTypeName">
					${e.declareTypeName}
				</td>
				<td>
				 	<a href="#" onclick="checkExpertDetailInfo(this)" style="text-decoration:none;">查看</a>
				 	<input  name="jobTitleName" type="hidden" value="${e.jobTitleName}"/>
					<input  name="educationName" type="hidden" value="${e.educationName}"/>
					<input  name="workUnit" type="hidden" value="${e.workUnit}"/>
					<input  name="rigionName" type="hidden" value="${e.rigionName}"/>
					<input  name="description" type="hidden" value="${e.description}"/>
					<input  name="achievement" type="hidden" value="${e.achievement}"/>
				</td>
				<td>
					<a href="#" onclick="checkProgectDetil('${e.idCard}')" style="text-decoration: none;">${e.allProjects.size()}</a>
				</td>
				<td>
					<a href="#" onclick="checkExcellentProgectDetil('${e.idCard}')" style="text-decoration: none;">${e.excellentProjects.size()}</a>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<script type="text/javascript">
	$(function() {
		//设置数据源
		$("#edTable").DataTable({
			"rowCallback": function( row, data, index ) {
			  },
			"aaSorting" : [[0, "asc"]], //默认的排序方式，第1列，升序排列  
			columns: [{
				"data": "index",
				'sClass': "text-center",
				width : "30px",  
				render : function(data, type, row, meta) {  
                    // 显示行号  
                    var startIndex = meta.settings._iDisplayStart;  
                    return startIndex + meta.row + 1;  
               	}  
			},{
				"data": "officeName"
			}, {
				"data": "linkman"
			}, {
				"data": "tel"
			}, {
				"data": "status"
			}, {
				"data": "useme"
			}, {
				"data": "usName"
			}, {
				"data": "usme"
			}, {
				"data": "us3"
			}, {
				"data": "us6"
			}, {
				"data": "us7",
				'sClass': "text-center" 
			}, {
				"data": "us8",
				'sClass': "text-center"
			}, {
				"data": "us23",
				'sClass': "text-center"
			}],
			 "paging": true,  
			 "sPaginationType": "full_numbers",//用于指定分页器风格
			 "oLanguage": {    // 语言设置  
		            "sLengthMenu": "每页显示 _MENU_ 条记录",  
		            "sZeroRecords": "抱歉， 没有找到",  
		            "sInfo": "从 _START_ 到 _END_  共 _TOTAL_条数据",  
		            "sInfoEmpty": "没有数据",  
		            "sInfoFiltered": "(从 _MAX_ 条数据中检索)",  
		            "sZeroRecords": "没有检索到数据",  
		            "sSearch": "搜索:",  
		            "oPaginate": {  
		              "sFirst": "首页",  
		              "sPrevious": "上一页",  
		              "sNext": "下一页",  
		              "sLast": "尾页"  
		            }  
		          },  
		});
	});
</script> 
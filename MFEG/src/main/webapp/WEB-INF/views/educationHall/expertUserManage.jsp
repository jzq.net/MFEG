<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript" src='<c:url value="/static/jquery/jquery-1.12.4.js"></c:url>'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/page.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/login/js/main.js"></script>
	<link href="${pageContext.request.contextPath }/static/css/tableSort.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/datatables/css/jquery.dataTables.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src='<c:url value="/static/bootstrap/js/bootstrap.js"></c:url>'></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/zzsc.css'/>">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/metismenu.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/demo.css">
	<script src="${pageContext.request.contextPath }/static/js/metismenu.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/fileinput/css/fileinput.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/locales/zh.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/style.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/list.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/shell.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/plugins/bootstrap/js/bootbox.min.js"></script>
	<script src="${pageContext.request.contextPath }/static/js/student/commonFunc.js"></script>
	<link rel="stylesheet" href='<c:url value="/static/bootstrap/css/bootstrap.css"></c:url>' />
</head>
<body>
<div>
      <div class="panel-default" style="margin-bottom:0; overflow-x:auto; border: 0px;">
            <div class="panel-heading" style="height: 45px;">
                <div style="display: inline-block;padding-left: 0px;font-size: 16px;"> 专家用户管理 </div>
                <div class="dropdown" style="display: inline-block; float: right;">
				  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">操作 <span class="caret"></span></button>
				  <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1">
				    <!-- <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:e_addUser()">添加用户</a></li> -->
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:eh_stopExpertUser()">禁用用户</a></li>
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:e_altertUser()">修改用户</a></li>
				    <!-- <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:e_batchAddUser()">批量添加用户</a></li>
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="">下载模板</a></li> -->
				  </ul>
				</div> 
            </div>
	        <div class="panel-body" style="padding-bottom:50px;">
	            <form id="listForm" role="form" action="" method="post">
	                <%@ include file="/WEB-INF/views/educationHall/eu_table.jsp" %>
	      	    </form>
		    </div>
     </div>
</div>
 <%@ include file="/WEB-INF/views/educationHall/addUser.jsp" %>
 <%@ include file="/WEB-INF/views/educationHall/alterUser.jsp" %>
 <%@ include file="/WEB-INF/views/educationHall/batchAddUser.jsp" %>
 </body>
<script type="text/javascript">
	function e_addUser(){
		$("#addUserModal").modal("toggle");
	}
	function eh_stopExpertUser(){
		var docEle = $(".checkchild:checked");
		if (docEle.size()==0) {
			alert("请选择您要禁用的用户！");
			return;
		}else{
			var ids = new Array();
			for(var i=0;i<docEle.size();i++){
				ids.push(docEle[i].value);
			}
			var id = ids.join("-");
			$.ajax({
				url : "<c:url value='/educationhallUser/stopUser.c'></c:url>",
				type : "post",
				data : {
					id:id,
					userRole:'3'
				},
				success : function(data) {
					if (data==1) {
						alert("禁用成功！");
						refurbish();
					}else {
						alert("禁用失败！");
					}
				},
				error : function(data) {
					alert("禁用失败！");
				}
			});
		}
	}
	function e_altertUser(){
		var docEle = $(".checkchild:checked");
		if (docEle.size()==0) {
			alert("请选择您要修改的用户！");
			return;
		}else if (docEle.size()>1) {
			alert("只能选择一个用户！")
			return;
		}
		$("#ehAlterExpertUserForm input[name='userName']").val(docEle.parent().parent().children("td[name='userName']").text());
		$("#ehAlterExpertUserForm input[name='name']").val(docEle.parent().parent().children("td[name='name']").text());
		$("#ehAlterExpertUserForm input[name='mobile']").val(docEle.parent().parent().children("td[name='mobile']").text());
		var st = docEle.parent().parent().children("td[name='status']").text();
		$("#ehAlterExpertUserForm option").each(function(i,n){
            if($(n).text()==st){
                $(n).attr("selected",true);
            }
        })
		$("#altertUserModal").modal("toggle");
	}
	
	function e_batchAddUser(){
		$("#batchUserModal").modal("toggle");
	}
	
	$(function(){
		$("#ehAlterExpertUserBtn").click(function(){
			var docEle = $(".checkchild:checked");
			var id = docEle[0].value;
			$("#ehAlterExpertUserForm").find("input[name='id']").val(id);
			var userName = $("#ehAlterExpertUserForm").find("input[name='userName']").val();
			var name = $("#ehAlterExpertUserForm").find("input[name='name']").val();
			var mobile = $("#ehAlterExpertUserForm").find("input[name='mobile']").val();
			var password = $("#ehAlterExpertUserForm").find("input[name='password']").val();
			if(userName=='undefined'||userName==''||userName==null){
				alert("请输入用户名!");
				return;
			}
			if(name=='undefined'||name==''||name==null){
				alert("请输入姓名!");
				return;
			}
			if(mobile=='undefined'||mobile==''||mobile==null){
				alert("请输入电话!");
				return;
			}
			if(!validate(mobile)){
				alert("请输入合法的手机号!");
				return;
			}
			if(password=='undefined'||password==''||password==null){
				alert("请输入密码!");
				return;
			}
			if(password!=$("#ehAlterExpertUserForm").find("input[name='password2']").val()){
				alert("两次密码输入不相同!")
				return;
			}
			$.ajax({
				url : "<c:url value='/educationhallUser/alterUser.c'></c:url>",
				data:$("#ehAlterExpertUserForm").serialize(),
				success : function(data) {
					if (data==1) {
						alert("修改成功！");
						refurbish();
					}else{
						alert("修改失败！");
					}
				},
				error : function(data) {
					alert("修改失败！");
				}
			});
		})
		
		var validate = function(phoneNum){
			var reg = /^1[3|4|5|7|8][0-9]{9}$/; //验证规则
			var flag = reg.test(phoneNum); //true
			return flag;
		}
	})
	
	function refurbish(){
		$.ajax({
			url : "<c:url value='/educationhallUser/userManage.c'></c:url>",
			type : "post",
			data:{
				userRole:'3'
			},
			async: false
		});
		$('#myFrame',parent.document).prop("src",'<c:url value="/educationHall/expertUserManage.html"></c:url>') 
	}
</script> 
</html>
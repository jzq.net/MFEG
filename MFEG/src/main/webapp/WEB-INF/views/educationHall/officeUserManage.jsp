<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript" src='<c:url value="/static/jquery/jquery-1.12.4.js"></c:url>'></script>
	<script type="text/javascript" src='<c:url value="/static/js/ajaxfileupload.js"></c:url>'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/page.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/login/js/main.js"></script>
	<link href="${pageContext.request.contextPath }/static/css/tableSort.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/datatables/css/jquery.dataTables.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src='<c:url value="/static/bootstrap/js/bootstrap.js"></c:url>'></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/zzsc.css'/>">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/metismenu.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/demo.css">
	<script src="${pageContext.request.contextPath }/static/js/metismenu.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/fileinput/css/fileinput.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/locales/zh.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/style.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/list.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/shell.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/plugins/bootstrap/js/bootbox.min.js"></script>
	<script src="${pageContext.request.contextPath }/static/js/student/commonFunc.js"></script>
	<link rel="stylesheet" href='<c:url value="/static/bootstrap/css/bootstrap.css"></c:url>' />
</head>
<body>
<div>
      <div class="panel-default" style="margin-bottom:0; overflow-x:auto; border: 0px;">
            <div class="panel-heading"  style="height: 45px;">
                <div style="display: inline-block;padding-left: 0px;font-size: 16px;"  class="col-sm-2">处室用户管理</div>
                <div class="btn-group" style="display: inline-block;float: right;">
				  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">操作 <span class="caret"></span></button>
				  <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1">
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:o_addUser()">添加用户</a></li>
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:o_stopUser()">禁用用户</a></li>
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:o_altertUser()">修改用户</a></li>
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:o_batchAddUser()">批量添加用户</a></li>
				    <li role="presentation"><a role="menuitem" tabindex="-1" href='<c:url value="/downloadUserModel.c?userRole=4"></c:url>'>下载模板</a></li>
				  </ul>
				</div> 
            </div>
	        <div class="panel-body" style="padding-bottom:50px;">
	            <form id="listForm" role="form" action="" method="post">
	                <%@ include file="/WEB-INF/views/educationHall/oum_table.jsp" %>
	      	    </form>
		    </div>
     </div>
</div>
<div class="modal" id="o_addUserModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">添加处室用户</h4>
			</div>
			<div class="modal-body">
					<form id="euAddOficeUserForm" name="f1" class="form-horizontal" method="post">
						<input type="hidden"  name="userRole" value="4"/>
						<div class="form-group">
							<label class="col-sm-3 control-label">处室名称：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="处室名称"  name="departmentName"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">用户名称：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="用户名称" name="userName"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">联系人：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="联系人" name="linkman"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">电话：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="电话" name="phoneNum"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">状态：</label>
							<div class="col-sm-6">
							    <select class="form-control" name="status"> 
							      <option value="1" selected="selected">正常</option> 
							      <option value="2">禁用</option> 
							    </select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">密码：</label>
							<div class="col-sm-6">
								<input type="password" class="form-control" placeholder="密码" name="password"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">确认密码：</label>
							<div class="col-sm-6">
								<input type="password" class="form-control" placeholder="确认密码" name="password2"/>
							</div>
						</div>
					</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary" id="euAddOficeUserBtn">确定</button>
			</div>
		</div> 
	</div> 
</div> 

<div class="modal" id="o_altertUserModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">修改处室用户</h4>
			</div>
			<div class="modal-body">
					<form id="euAlterUserForm" name="f1" class="form-horizontal" method="post">
						<input type="hidden"  name="userRole" value="4"/>
						<input type="hidden"  name="id" />
						<div class="form-group">
							<label class="col-sm-3 control-label">处室名称：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="处室名称"  name="departmentName"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">用户名称：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="用户名称" name="userName"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">联系人：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="联系人" name="linkman"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">电话：</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" placeholder="电话" name="phoneNum"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">状态：</label>
							<div class="col-sm-6">
							    <select class="form-control" name="status"> 
							      <option value="1">正常</option> 
							      <option value="2">禁用</option> 
							    </select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">密码：</label>
							<div class="col-sm-6">
								<input type="password" class="form-control" placeholder="密码" name="password"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">确认密码：</label>
							<div class="col-sm-6">
								<input type="password" class="form-control" placeholder="确认密码" name="password2"/>
							</div>
						</div>
					</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary" id="euAlterUserBtn">确定</button>
			</div>
		</div> 
	</div> 
</div> 

<div class="modal" id="o_batchUserModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">批量导入处室用户</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" action="" method="post">	 
					<div class="form-group">
						<label class="control-label col-md-3">上传Excel文件： </label>
						<div class="col-md-8">
							<input id="input-o" name="file" type="file" multiple class="file-loading" data-allowed-file-extensions='["xls"]'>
						</div>
					</div>
						<label class="control-label col-md-9" style="color: red;font-size: 13px;font-weight: normal;">(文件以.xls结尾)文档格式参考<a href="" style="color: red;font-weight: normal;font-style: italic ;">模板</a> </label>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary" id="euoBatchUserBtnId">确定</button>
			</div>
		</div> 
	</div> 
</div> 
</body>
<script type="text/javascript">
	function o_addUser(){
		$("#o_addUserModal").modal("toggle");
	}
	function o_stopUser(){
		var docEle = $(".checkchild:checked");
		if (docEle.size()==0) {
			alert("请选择您要禁用的用户！");
			return;
		}else{
			var ids = new Array();
			for(var i=0;i<docEle.size();i++){
				ids.push(docEle[i].value);
			}
			var id = ids.join("-");
			$.ajax({
				url : "<c:url value='/educationhallUser/stopUser.c'></c:url>",
				type : "post",
				data : {
					id:id,
					userRole:'4'
				},
				success : function(data) {
					if (data==1) {
						alert("禁用成功！");
						refurbish();
					}else {
						alert("禁用失败！");
					}
				},
				error : function(data) {
					alert("禁用失败！");
				}
			});
		}
	}
	function o_altertUser(){
		var docEle = $(".checkchild:checked");
		if (docEle.size()==0) {
			alert("请选择您要修改的用户！");
			return;
		}else if (docEle.size()>1) {
			alert("只能选择一个用户！")
			return;
		}
		$("#euAlterUserForm input[name='departmentName']").val(docEle.parent().parent().children("td[name='departmentName']").text());
		$("#euAlterUserForm input[name='userName']").val(docEle.parent().parent().children("td[name='userName']").text());
		$("#euAlterUserForm input[name='linkman']").val(docEle.parent().parent().children("td[name='linkman']").text());
		$("#euAlterUserForm input[name='phoneNum']").val(docEle.parent().parent().children("td[name='phoneNum']").text());
		var st = docEle.parent().parent().children("td[name='status']").text();
		$("#euAlterUserForm option").each(function(i,n){
            if($(n).text()==st){
                $(n).attr("selected",true);
            }
        })
		$("#o_altertUserModal").modal("toggle");
	}
	function o_batchAddUser(){
		$("#o_batchUserModal").modal("toggle");
	}
	$(function() {
		$("#input-o").fileinput({
			showCaption : true,
			language : 'zh',
			maxFileCount : 1
		});
		
		$("#euAddOficeUserBtn").click(function(){
			var departmentName = $("#euAddOficeUserForm").find("input[name='departmentName']").val();
			var userName = $("#euAddOficeUserForm").find("input[name='userName']").val();
			var linkman = $("#euAddOficeUserForm").find("input[name='linkman']").val();
			var phoneNum = $("#euAddOficeUserForm").find("input[name='phoneNum']").val();
			var password = $("#euAddOficeUserForm").find("input[name='password']").val();
			if(departmentName=='undefined'||departmentName==''||departmentName==null){
				alert("请输入处室名称!");
				return;
			}
			if(userName=='undefined'||userName==''||userName==null){
				alert("请输入用户名称!");
				return;
			}
			if(linkman=='undefined'||linkman==''||linkman==null){
				alert("请输入联系人!");
				return;
			}
			if(phoneNum=='undefined'||phoneNum==''||phoneNum==null){
				alert("请输入电话!");
				return;
			}
			if(!validate(phoneNum)){
				alert("请输入合法的手机号!");
				return;
			}
			if(password=='undefined'||password==''||password==null){
				alert("请输入密码!");
				return;
			}
			if(password!=$("#euAddOficeUserForm").find("input[name='password2']").val()){
				alert("两次密码输入不相同!")
				return;
			}
			$.ajax({
				url : "<c:url value='/educationhallUser/addUser.c'></c:url>",
				data:$("#euAddOficeUserForm").serialize(),
				success : function(data) {
					if (data==1) {
						alert("添加成功！");
						refurbish();
					}else if(data==3) {
						alert("用户名已存在,添加失败！");
					}else{
						alert("添加失败！");
					}
				},
				error : function(data) {
					alert("添加失败！");
				}
			});
		})
		$("#euAlterUserBtn").click(function(){
			var docEle = $(".checkchild:checked");
			var id = docEle[0].value;
			$("#euAlterUserForm").find("input[name='id']").val(id);
			var departmentName = $("#euAlterUserForm").find("input[name='departmentName']").val();
			var userName = $("#euAlterUserForm").find("input[name='userName']").val();
			var linkman = $("#euAlterUserForm").find("input[name='linkman']").val();
			var phoneNum = $("#euAlterUserForm").find("input[name='phoneNum']").val();
			var password = $("#euAlterUserForm").find("input[name='password']").val();
			if(departmentName=='undefined'||departmentName==''||departmentName==null){
				alert("请输入处室名称!");
				return;
			}
			if(userName=='undefined'||userName==''||userName==null){
				alert("请输入用户名称!");
				return;
			}
			if(linkman=='undefined'||linkman==''||linkman==null){
				alert("请输入联系人!");
				return;
			}
			if(phoneNum=='undefined'||phoneNum==''||phoneNum==null){
				alert("请输入电话!");
				return;
			}
			if(!validate(phoneNum)){
				alert("请输入合法的手机号!");
				return;
			}
			if(password=='undefined'||password==''||password==null){
				alert("请输入密码!");
				return;
			}
			if(password!=$("#euAlterUserForm").find("input[name='password2']").val()){
				alert("两次密码输入不相同!")
				return;
			}
			$.ajax({
				url : "<c:url value='/educationhallUser/alterUser.c'></c:url>",
				data:$("#euAlterUserForm").serialize(),
				success : function(data) {
					if (data==1) {
						alert("修改成功！");
						refurbish();
					}else{
						alert("修改失败！");
					}
				},
				error : function(data) {
					alert("修改失败！");
				}
			});
		})
		
		$("#euoBatchUserBtnId").click(function(){
			var fv = $("#input-o").val();
			if(fv==undefined||fv==null||fv==""){
				alert("请选择文件！")
				return;
			}
			 $.ajaxFileUpload({
                url: "<c:url value='/uploadDepartmentUserInfo.c'></c:url>",
                secureuri: false,
                cache: false,
                type: "post",
                dataType : 'text/html',//contentType参数是一定要有的，否则浏览器总是提示将返回的JSON结果另存为文件，不会交给ajaxfileupload处理
                fileElementId: "input-o",
                success: function (data) {
        			$("#o_batchUserModal").modal("hide");
        			alert(data);
                },
                error: function (data) {
                    alert("请求错误!");
                } 
            });
		})
		
		var validate = function(phoneNum){
			var reg = /^1[3|4|5|7|8][0-9]{9}$/; //验证规则
			var flag = reg.test(phoneNum); //true
			return flag;
		}
		
	});
	
	function refurbish(){
		$.ajax({
			url : "<c:url value='/educationhallUser/userManage.c'></c:url>",
			type : "post",
			data:{
				userRole:'4'
			},
			async: false
		});
		$('#myFrame',parent.document).prop("src",'<c:url value="/educationHall/officeUserManage.html"></c:url>') 
	}
</script> 
</html>
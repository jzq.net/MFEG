<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript" src='<c:url value="/static/jquery/jquery-1.12.4.js"></c:url>'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/page.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/login/js/main.js"></script>
	<link href="${pageContext.request.contextPath }/static/css/tableSort.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/datatables/css/jquery.dataTables.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/datatables/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src='<c:url value="/static/bootstrap/js/bootstrap.js"></c:url>'></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/zzsc.css'/>">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/metismenu.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/static/css/demo.css">
	<script src="${pageContext.request.contextPath }/static/js/metismenu.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/fileinput/css/fileinput.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/fileinput/js/locales/zh.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/style.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/list.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/shell.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/plugins/bootstrap/js/bootbox.min.js"></script>
	<script src="${pageContext.request.contextPath }/static/js/student/commonFunc.js"></script>
	<link rel="stylesheet" href='<c:url value="/static/bootstrap/css/bootstrap.css"></c:url>' />
</head>
<body>
<div>
      <div class="panel-default" style="margin-bottom:0; overflow-x:auto; border: 0px;">
            <div class="panel-heading" style="height: 45px;">
                <div style="display: inline-block;padding-left: 0px;font-size: 16px;"><a href="javascript:projectManage()"><span class="glyphicon glyphicon-share-alt" style="color: rgb(255, 140, 60);">返回</span></a></div>
                <div style="display: inline-block; float: right;">
				  <button type="button" class="btn btn-default" id="euExportExpertBtnId">导出</button>
				  <form method="post" id="euExportExpertid" style="display: none;" action="<c:url value='/exportExpert.c'></c:url>">
                   	<input name="projectId" value="${param.projectId}">
                  </form>
				</div> 
            </div>
	        <div class="panel-body" style="padding-bottom:50px;">
	            <form role="form" action="" method="post">
	                <%@ include file="/WEB-INF/views/educationHall/eInfo_table.jsp" %>
	      	    </form>
		    </div>
     </div>
</div>
<!-- 模态对话框 -->
<div class="modal" id="checkExpertInfo_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">专家信息</h4>
			</div>
			<div class="modal-body">
				 <form>
				   <table class="table table-bordered">
					   <thead></thead>
					   <tbody id="ehid">
					     <tr>
					       <td>姓名</td>
					       <td name="name"> </td>
					       <td>性别</td>
					       <td  name="sex"> </td>
					       <td>职务</td>
					       <td  name="position"> </td>
					       <td>职称</td>
					       <td  name="jobTitleName"> </td>
					       <td>学历</td>
					       <td  name="educationName"> </td>
					     </tr>
					     <tr>
					       <td>专家来源</td>
					       <td colspan="9"  name="originName"> </td>
					     </tr>
					     <tr>
					       <td>工作单位</td>
					       <td colspan="4"  name="workUnit"> </td>
					       <td>所在城市</td>
					       <td colspan="4"  name="rigionName"> </td>
					     </tr>
						     <tr>
					       <td>所属行业</td>
					       <td colspan="4"  name="industryName"> </td>
					       <td>研究领域</td>
					       <td colspan="4"  name="researchArea"> </td>
					     </tr>
					     <tr>
					       <td>办公电话</td>
					       <td colspan="4"  name="telephone"> </td>
					       <td>电子邮箱</td>
					       <td colspan="4"  name="email"> </td>
					     </tr>
					     <tr>
					       <td>手机号码</td>
					       <td colspan="4"  name="mobile"> </td>
					       <td>身份证号</td>
					       <td colspan="4" name="idCard"> </td>
					     </tr>
					     <tr>
					       <td>申报类型</td>
					       <td colspan="9"  name="declareTypeName"> </td>
					     </tr>
					     <tr>
					        <td>个人介绍</td>
					       <td colspan="9"> <textarea readonly="readonly" id="descriptionId" style="border:0px; resize : none;" class="form-control"   name="description"> </textarea> </td>
					     </tr>
					     <tr>
					       <td>工作业绩</td>
					       <td colspan="9"  name="achievement"> </td>
					     </tr>
					   </tbody>
					 </table>
				</form> 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
			</div>
		</div> 
	</div> 
</div> 
</body>
<script type="text/javascript">
	function ehcheckExpertInfo(idCard,obj){
		var tr = $(obj).parent().parent();
		$("#ehid td[name='name']").text(tr.find("td[name='name']").text());
		$("#ehid td[name='sex']").text(tr.find("td[name='sex']").text());
		$("#ehid td[name='position']").text(tr.find("td[name='position']").text());
		$("#ehid td[name='jobTitleName']").text(tr.find("input[name='jobTitleName']").val());
		$("#ehid td[name='educationName']").text(tr.find("input[name='educationName']").val());
		$("#ehid td[name='originName']").text(tr.find("td[name='originName']").text());
		$("#ehid td[name='workUnit']").text(tr.find("input[name='workUnit']").val());
		$("#ehid td[name='rigionName']").text(tr.find("input[name='rigionName']").val());
		$("#ehid td[name='industryName']").text(tr.find("td[name='industryName']").text());
		$("#ehid td[name='researchArea']").text(tr.find("td[name='researchArea']").text());
		$("#ehid td[name='declareTypeName']").text(tr.find("td[name='declareTypeName']").text());
		$("#ehid textarea[name='description']").text(tr.find("input[name='description']").val());
		$("#ehid td[name='achievement']").html(tr.find("input[name='achievement']").val());
		$("#ehid td[name='telephone']").html(tr.find("input[name='telephone']").val());
		$("#ehid td[name='email']").html(tr.find("input[name='email']").val());
		$("#ehid td[name='mobile']").html(tr.find("input[name='mobile']").val());
		$("#ehid td[name='idCard']").html(tr.find("input[name='idCard']").val());
		$("#checkExpertInfo_modal").modal("toggle");
	}
	function projectManage(){
		$('#myFrame', parent.document).prop("src",'<c:url value="/educationHall/projectManage.html"></c:url>');
	}
	
	$("#euExportExpertBtnId").click(function(){
		$("#euExportExpertid").submit();
	})
</script> 
</html>
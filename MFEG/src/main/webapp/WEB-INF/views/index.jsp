<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>导师库管理网</title>
	<link href="${pageContext.request.contextPath }/static/login/css/login.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/jquery/jquery-1.12.4.js"></script>
	<script src="${pageContext.request.contextPath }/static/login/js/cloud.js" type="text/javascript"></script>

	<script type="text/javascript">
		if(window.parent != window)
			window.parent.location.reload(true);
	</script>
	
	<style type="text/css">
    .loginUserPhone{width:343px; height:48px; background:url(<c:url value='/static/login/images/loginuser.png'/>) no-repeat; border:none; line-height:48px; padding-left:44px; font-size:14px; font-weight:bold;}
	.loginPhoneNum{width:343px; height:48px; background:url(<c:url value='/static/login/images/loginpassword.png'/>) no-repeat; border:none;line-height:48px; padding-left:44px; font-size:14px; color:#90a2bc;}
    </style>
</head>
<body style="background-color:#1c77ac; background-image:url(${pageContext.request.contextPath }/static/login/images/light.png); background-repeat:no-repeat; background-position:center top; overflow:auto;">
    <div id="mainBody">
      <div id="cloud1" class="cloud"></div>
      <div id="cloud2" class="cloud"></div>
    </div>  
    <div class="loginbody">
	    <span class="systemlogo"></span> 
	    <div class="loginbox">
		    <form action='<c:url value="/index.c"></c:url>' method="post" id="pwdLogin" style="display: block;">
		    <ul>
			    <li><input name="userName" type="text" class="loginuser" placeholder="请输入用户名" onclick="JavaScript:this.value=''"/></li>
			    <li><input name="userPwd" type="password" class="loginpwd" placeholder="请输入密码" id="loginPwd" onclick="JavaScript:this.value=''"/></li>
			    <li class="loginli"style="margin-left: 0px;padding-left: 0px;">
			    	<input name="loginType" type="hidden" value="1"><!-- 密码登录 -->
			    	<input name="userRole" type="radio" value="1" id="user_1" checked="checked">教育厅&nbsp;&nbsp;
			    	<input name="userRole" type="radio" value="2" id="user_2">高    校&nbsp;&nbsp;
			    	<input name="userRole" type="radio" value="4" id="user_4">处    室&nbsp;&nbsp;
			    	<input name="userRole" type="radio" value="3" id="user_3">专    家&nbsp;&nbsp;&nbsp;&nbsp;
			    	<input type="button" id="loginbtn" class="loginbtn1" style="margin-right: 0px;" value="登录">
			    	<input type="button" id="firstloginbtn" style="color: red;background-color: white;cursor:pointer;" value="首次登陆">
					<br> <br>
					<c:if test="${not empty loginError }">
				    	<font color="red">${loginError}</font>
				    </c:if>
			    </li>
		    </ul>
		    </form>
		    <form action='<c:url value="/index.c"></c:url>' method="post" id="phoneLogin" style="display: none;">
		    <ul>
			    <li><input name="userIdCard" type="text" class="loginUserPhone" placeholder="请输入身份证号" onclick="JavaScript:this.value=''" style=""/></li>
			    <li><input name="userPhoneNum" type="text" class="loginPhoneNum" placeholder="请输入预留手机号" id="loginPhoneNum" onclick="JavaScript:this.value=''"/></li>
			    <li class="loginli" >
			    	<input name="loginType" type="hidden" value="2"><!-- 首次登录 -->
			    	<input name="userRole" type="radio" value="3" checked="checked">专家&nbsp;&nbsp;&nbsp;&nbsp;
			    	<input type="button" id="loginbtnPhone" class="loginbtn1" value="登录" style="margin-right: 0px;">
			    	<input type="button" id="loginbtn2Phone" value="返回" style="color: red;background-color: white;cursor:pointer;">
					<br> <br>
					<c:if test="${not empty firstLoginError }">
				    	<font color="red">${firstLoginError}</font>
				    </c:if>
			    </li>
		    </ul>
		   </form>
	    </div>
    </div>
</body>

<script>
$(function(){
    $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
	$(window).resize(function(){  
    	$('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
    })  
    
    var ur = "${userRole}";
    if(ur!=null&&ur!="nudefined"&&ur!=""){
    	$("#user_"+ur).attr("checked","checked");
    }
	var firstLoginError = "${empty firstLoginError}";
	if(firstLoginError=="false"){
		$("#phoneLogin").css("display","block");
   		$("#pwdLogin").css("display","none");
	}
	
    $("#loginbtn").click(function(){
    	$("#pwdLogin").submit();
    })
    
    $("#loginbtnPhone").click(function(){
    	$("#phoneLogin").submit();
    })
    $("#firstloginbtn").click(function(){
    	$("#phoneLogin").css("display","block");
   		$("#pwdLogin").css("display","none");
    })
    $("#loginbtn2Phone").click(function(){
   		$("#phoneLogin").css("display","none");
   		$("#pwdLogin").css("display","block");
    })
    
});  
</script>
<!--@网站公共页脚-->
</html>
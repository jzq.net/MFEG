<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE>
<html>
<style>
	*			{ margin:0px; margin:0px; padding:0px; font-size:14px; list-style-type:none; }
	html,body	{ height:100%; overflow:hidden; }
	body		{ margin:0 ; height:100%;overflow:hidden; background-color:#181D33; }
	#timeoutImg	{ width:419px; height:206px; position:absolute; top:50%; left:50%;margin-top:-164px; margin-left:-208px; background:url(<c:url value="/static/images/timeout.png"></c:url>) no-repeat; overflow:hidden; }
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>页面过期</title>
</head>

<body style="overflow:hidden;-moz-user-select:none;-webkit-user-select:none;">
	<div id="timeoutImg">
		<div style="font-size:22px; margin-top:80px; margin-left:125px; text-align:center; color:#f00; font-weight:bold;">
			页面发生错误！
		</div>
		<div style="font-size:16px; margin-top:30px; margin-left:125px; text-align:center;">
			 &nbsp;<a href="" onclick="window.history.back(-1);" style="text-decoration: none;">返回</a>
		</div>
	</div>
</body>
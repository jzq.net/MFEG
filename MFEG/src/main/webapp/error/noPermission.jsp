<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragrma", "no-cache");
	response.setDateHeader("Expires",0);
%>
<!DOCTYPE>
<html>
<style>
	*			{ margin:0px; margin:0px; padding:0px; font-size:14px; list-style-type:none; }
	html,body	{ height:100%; overflow:hidden; }
	body		{ margin:0 ; height:100%;overflow:hidden; background-color:#181D33; }
	#timeoutImg	{ width:419px; height:206px; position:absolute; top:50%; left:50%;margin-top:-164px; margin-left:-208px; background:url(<c:url value="/static/images/timeout.png"></c:url>) no-repeat; overflow:hidden; }
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>页面过期</title>
</head>

<body style="overflow:hidden;-moz-user-select:none;-webkit-user-select:none;">
	<div id="timeoutImg">
		<div style="font-size:22px; margin-top:60px; margin-left:125px; text-align:center; color:#f00; font-weight:bold;">
			您没有访问权限！
		</div>
		<div style="font-size:16px; margin-top:30px; margin-left:125px; text-align:center;">
			<span id="senc" style="font-size:18px;">9</span>&nbsp;秒后返回登录页面
			<br/><br/>
			或&nbsp;<a href="#" onclick="reLogin();">点击此处</a>&nbsp;重新登录
		</div>
	</div>
</body>

<script type="text/javascript">
	function reLogin(){
		window.parent.parent.parent.parent.parent.location='<c:url value="/"></c:url>';
	}
	function closethisWin(){
		window.top.closeMyWin();
	}
		
	var tim = 9;
	setInterval(function(){
		if(tim > 0){
			tim = tim - 1;
			document.getElementById("senc").innerHTML = tim;
		}else{
			reLogin();
		}
	}, 1000);

</script>
</html>